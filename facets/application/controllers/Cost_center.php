<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_center extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("cost_center_model");
	}

	public function index()
	{
		$cost_center = $this->cost_center_model->getCost_center();
		// Loads HTML page
		$this->template->loadContent("cost_center/index.php", array(
			"cost_center" => $cost_center
			)
		);
	}

	public function addCost_center() 
	{

		$this->cost_center_model->addCost_center(array("cost_center" =>$cost_center));
		//$this->session->set_flashdata("globalmsg", lang("success_30"));
		redirect(base_url().'cost_center');
	}

	public function updateCost_center($id) 
	{

		$cost_center = $this->cost_center_model->getCost_centerById($id);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		// Loads HTML page
		$this->template->loadContent("cost_center/edit_costcenter.php", array(
			"cost_center" => $cost_center->row()
			)
		);
	}

	public function updateCost_centerPro($id) 
	{

		$id = intval($id);

		$cost_center = $this->common->nohtml($this->input->post("cost_center"));

		$this->cost_center_model->updateCost_center($id, 
			array(
				"cost_center" =>$cost_center
				)
		);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		redirect(site_url("cost_center"));
	}

	public function deleteCost_center($id) 
	{

		$this->cost_center_model->deleteCost_center($id);
		//$this->session->set_flashdata("globalmsg", lang("success_32"));
		redirect(site_url('cost_center'));
	}
}

?>