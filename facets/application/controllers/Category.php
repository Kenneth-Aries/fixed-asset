<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("category_model");
		$this->load->model("user_model");
	}

	public function index()
	{
		$category = $this->category_model->getCategory();
		// Loads HTML page
		$this->template->loadContent("category/index.php", array(
			"category" => $category
			)
		);
	}

	public function addCategory() 
	{

		$this->category_model->addCategory(array("category_name" =>$category_name));
		//$this->session->set_flashdata("globalmsg", lang("success_30"));
		redirect(base_url().'category');
	}

	public function updateCategory($id) 
	{

		$category = $this->category_model->getCategoryById($id);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		// Loads HTML page
		$this->template->loadContent("category/edit_category.php", array(
			"category" => $category->row()
			)
		);
	}

	public function updateCategoryPro($id) 
	{

		$id = intval($id);

		$category_name = $this->common->nohtml($this->input->post("category_name"));

		$this->category_model->updateCategory($id, 
			array(
				"category_name" =>$category_name
				)
		);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		redirect(site_url("category"));
	}

	public function deleteCategory($id) 
	{

		$this->category_model->deleteCategory($id);
		//$this->session->set_flashdata("globalmsg", lang("success_32"));
		redirect(site_url('category'));
	}
}

?>