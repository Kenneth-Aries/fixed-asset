<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assets extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("assets_model");
		$this->load->model("entity_model");
		$this->load->model("location_model");
	}

	public function index()
	{
		//, $identity=""
		$location_id = intval($location_id);
		$barcode = intval($barcode);
		//$id = intval($identity);
		$asset_id = intval($asset_id);

		//$jqueryAddition = ""
		$assetLocation = $this->assets_model->getAssetByLocationId($location_id)->row();
		$assetBarcode = $this->assets_model->getAssetByBarcode($barcode)->row();
		$assetHistory = $this->assets_model->getAssetHistory($asset_id)->row();
		$assetImages = $this->assets_model->getAssetImages($asset_id)->row();
//		$location = $this->location_model->getLocations($entity_id);
		//$changeLocation = $this->location_model->getLocations($identity)->row();
		$entity = $this->entity_model->getEntity();

		// Loads HTML page
		$this->template->loadContent("assets/index.php", array(
			"assetLocation" => $assetLocation,
			"assetBarcode" => $assetBarcode,
			"assetHistory" => $assetHistory,
			"assetImages" => $assetImages,
//			"location" => $location,
			"entity" => $entity
			)
		);
	}
	
	public function Locations($id) 
	{
		$id = $this->input->post('entity_list');
		$query = $this->db->query('SELECT location_id, location_name FROM location WHERE ID=' . $id);
		$changeLocation = $query->result_array();
		$this->template->loadContent("assets/index.php", array(
				"changeLocation" => $changeLocation,
			)
		);

		echo $entity_id;
	}

}

?>