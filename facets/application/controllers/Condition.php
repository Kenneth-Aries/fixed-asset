<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Condition extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("condition_model");
	}

	public function index()
	{
		$condition = $this->condition_model->getCondition();
		// Loads HTML page
		$this->template->loadContent("condition/index.php", array(
			"condition" => $condition
			)
		);
	}

	public function addCondition() 
	{

		$this->condition_model->addCondition(array("condition_name" =>$condition_name));
		//$this->session->set_flashdata("globalmsg", lang("success_30"));
		redirect(base_url().'condition');
	}

	public function updateCondition($id) 
	{

		$condition = $this->condition_model->getConditionById($id);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		// Loads HTML page
		$this->template->loadContent("condition/edit_condition.php", array(
			"condition" => $condition->row()
			)
		);
	}

	public function updateConditionPro($id) 
	{

		$id = intval($id);

		$condition_name = $this->common->nohtml($this->input->post("condition_name"));

		$this->condition_model->updateCondition($id, 
			array(
				"condition_name" =>$condition_name
				)
		);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		redirect(site_url("condition"));
	}

	public function deleteCondition($id) 
	{

		$this->condition_model->deleteCondition($id);
		//$this->session->set_flashdata("globalmsg", lang("success_32"));
		redirect(site_url('condition'));
	}
}

?>