<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("admin_model");
		$this->load->model("user_model");

		//if (!$this->user->loggedin) $this->template->error(lang("error_1"));
		//if (!isset($this->user->info->user_role_id) && 
			//(!$this->user->info->admin && !$this->user->info->admin_settings 
				//&& !$this->user->info->admin_members &&
				//!$this->user->info->admin_payment)
			//) {
			//$this->template->error(lang("error_2"));
		//}
	}


	public function index() 
	{	
		$this->template->loadData("nav-active", 
			array("admin" => array("general" => 1)));
		$this->template->loadContent("admin/index.php", array(
			)
		);

	}

	public function premium_users($page=0) 
	{
		$this->template->loadData("activeLink", 
			array("admin" => array("premium_users" => 1)));


		$page = intval($page);
		$users = $this->admin_model->get_premium_users($page);

		$this->load->library('pagination');
		$config['base_url'] = site_url("admin/premium_users");
		$config['total_rows'] = $this->admin_model
			->get_total_premium_users_count();
		$config['per_page'] = 20;
		$config['uri_segment'] = 2;

		include (APPPATH . "/config/page_config.php");

		$this->pagination->initialize($config); 

		$this->template->loadContent("admin/premium_users.php", array(
			"members" => $users
			)
		);
	}

	public function user_roles() 
	{
		//if(!$this->user->info->admin) $this->template->error(lang("error_2"));
		$this->template->loadData("activeLink", 
			array("admin" => array("user_roles" => 1)));
		$roles = $this->admin_model->get_user_roles();
		$this->template->loadContent("admin/user_roles.php", array(
			"roles" => $roles
			)
		);
	}

	public function add_user_role_pro() 
	{
		//if(!$this->user->info->admin) $this->template->error(lang("error_2"));

		$name = $this->common->nohtml($this->input->post("name"));
		if (empty($name)) $this->template->error(lang("error_64"));

		$admin = intval($this->input->post("admin"));
		$admin_settings = intval($this->input->post("admin_settings"));
		$admin_members = intval($this->input->post("admin_members"));
		$admin_payment = intval($this->input->post("admin_payment"));

		$this->admin_model->add_user_role(
			array(
				"name" =>$name,
				"admin" => $admin,
				"admin_settings" => $admin_settings,
				"admin_members" => $admin_members,
				"admin_payment" => $admin_payment
				)
			);
		$this->session->set_flashdata("globalmsg", lang("success_30"));
		redirect(site_url("admin/user_roles"));
	}

	public function edit_user_role($id) 
	{
		//if(!$this->user->info->admin) $this->template->error(lang("error_2"));
		$id = intval($id);
		$role = $this->admin_model->get_user_role($id);
		if ($role->num_rows() == 0) $this->template->error(lang("error_65"));

		$this->template->loadData("activeLink", 
			array("admin" => array("user_roles" => 1)));

		$this->template->loadContent("admin/edit_user_role.php", array(
			"role" => $role->row()
			)
		);
	}

	public function edit_user_role_pro($id) 
	{
		//if(!$this->user->info->admin) $this->template->error(lang("error_2"));
		$id = intval($id);
		$role = $this->admin_model->get_user_role($id);
		if ($role->num_rows() == 0) $this->template->error(lang("error_65"));

		$name = $this->common->nohtml($this->input->post("name"));
		if (empty($name)) $this->template->error(lang("error_64"));

		$admin = intval($this->input->post("admin"));
		$admin_settings = intval($this->input->post("admin_settings"));
		$admin_members = intval($this->input->post("admin_members"));
		$admin_payment = intval($this->input->post("admin_payment"));

		$this->admin_model->update_user_role($id, 
			array(
				"name" =>$name,
				"admin" => $admin,
				"admin_settings" => $admin_settings,
				"admin_members" => $admin_members,
				"admin_payment" => $admin_payment
				)
		);
		$this->session->set_flashdata("globalmsg", lang("success_31"));
		redirect(site_url("admin/user_roles"));
	}

	public function delete_user_role($id, $hash) 
	{
		if(!$this->user->info->admin) $this->template->error(lang("error_2"));
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$group = $this->admin_model->get_user_role($id);
		if ($group->num_rows() == 0) $this->template->error(lang("error_65"));

		$this->admin_model->delete_user_role($id);
		// Delete all user groups from member

		$this->session->set_flashdata("globalmsg", lang("success_32"));
		redirect(site_url("admin/user_roles"));
	}

	public function payment_logs($page = 0) 
	{
		if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			$this->template->error(lang("error_2"));
		}

		$page = intval($page);
		$this->template->loadData("activeLink", 
			array("admin" => array("payment_logs" => 1)));

		$logs = $this->admin_model->get_payment_logs($page);

		$this->load->library('pagination');
		$config['base_url'] = site_url("admin/payment_logs");
		$config['total_rows'] = $this->admin_model
			->get_total_payment_logs_count();
		$config['per_page'] = 20;
		$config['uri_segment'] = 2;

		include (APPPATH . "/config/page_config.php");

		$this->pagination->initialize($config); 

		$this->template->loadContent("admin/payment_logs.php", array(
			"logs" => $logs
			)
		);
	}

	public function payment_plans() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("payment_plans" => 1)));
		$plans = $this->admin_model->get_payment_plans();

		$this->template->loadContent("admin/payment_plans.php", array(
			"plans" => $plans
			)
		);
	}

	public function add_payment_plan() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			//$this->template->error(lang("error_2"));
		//}
		$name = $this->common->nohtml($this->input->post("name"));
		$desc = $this->common->nohtml($this->input->post("description"));
		$cost = abs($this->input->post("cost"));
		$color = $this->common->nohtml($this->input->post("color"));
		$fontcolor = $this->common->nohtml($this->input->post("fontcolor"));
		$days = intval($this->input->post("days"));

		$this->admin_model->add_payment_plan(array(
			"name" => $name,
			"cost" => $cost,
			"hexcolor" => $color,
			"days" => $days,
			"description" => $desc,
			"fontcolor" => $fontcolor
			)
		);

		$this->session->set_flashdata("globalmsg", lang("success_25"));
		redirect(site_url("admin/payment_plans"));
	}

	public function edit_payment_plan($id) 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("payment_plans" => 1)));
		$id = intval($id);
		$plan = $this->admin_model->get_payment_plan($id);
		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));

		$this->template->loadContent("admin/edit_payment_plan.php", array(
			"plan" => $plan->row()
			)
		);
	}

	public function edit_payment_plan_pro($id) 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			//$this->template->error(lang("error_2"));
		//}
		$id = intval($id);
		$plan = $this->admin_model->get_payment_plan($id);
		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));

		$name = $this->common->nohtml($this->input->post("name"));
		$desc = $this->common->nohtml($this->input->post("description"));
		$cost = abs($this->input->post("cost"));
		$color = $this->common->nohtml($this->input->post("color"));
		$fontcolor = $this->common->nohtml($this->input->post("fontcolor"));
		$days = intval($this->input->post("days"));

		$this->admin_model->update_payment_plan($id, array(
			"name" => $name,
			"cost" => $cost,
			"hexcolor" => $color,
			"days" => $days,
			"description" => $desc,
			"fontcolor" => $fontcolor
			)
		);

		$this->session->set_flashdata("globalmsg", lang("success_26"));
		redirect(site_url("admin/payment_plans"));
	}

	public function delete_payment_plan($id, $hash) 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			//$this->template->error(lang("error_2"));
		//}
		//if($hash != $this->security->get_csrf_hash()) {
			//$this->template->error(lang("error_6"));
		//}

		$id = intval($id);
		$plan = $this->admin_model->get_payment_plan($id);
		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));

		$this->admin_model->delete_payment_plan($id);
		$this->session->set_flashdata("globalmsg", lang("success_27"));
		redirect(site_url("admin/payment_plans"));
	}

	public function payment_settings() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_payment) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("payment_settings" => 1)));
		$this->template->loadContent("admin/payment_settings.php", array(
			)
		);
	}

	public function email_members() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_members) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("email_members" => 1)));
		$groups = $this->admin_model->get_user_groups();
		$this->template->loadContent("admin/email_members.php", array(
			"groups" => $groups
			)
		);
	}

	public function email_members_pro() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_members) {
			//$this->template->error(lang("error_2"));
		//}
		$usernames = $this->common->nohtml($this->input->post("usernames"));
		$groupid = intval($this->input->post("groupid"));
		$title = $this->common->nohtml($this->input->post("title"));
		$message = $this->lib_filter->go($this->input->post("message"));

		if ($groupid == -1) {
			// All members
			$users = array();
			$usersc = $this->admin_model->get_all_users();
			foreach ($usersc->result() as $r) {
				$users[] = $r;
			}
		} else {
			$usernames = explode(",", $usernames);

			$users = array();
			foreach ($usernames as $username) {
				if (empty($username)) continue;
				$user = $this->user_model->get_user_by_username($username);
				if ($user->num_rows() == 0) {
					$this->template->error(lang("error_3") . $username);
				}
				$users[] = $user->row();
			}

			if ($groupid > 0) {
				$group = $this->admin_model->get_user_group($groupid);
				if ($group->num_rows() == 0) {
					$this->template->error(lang("error_4"));
				}

				$users_g = $this->admin_model->get_all_group_users($groupid);
				$cursers = $users;

				foreach ($users_g->result() as $r) {
					// Check for duplicates
					$skip = false;
					foreach ($cusers as $a) {
						if($a->userid == $r->userid) $skip = true;
					}
					if (!$skip) {
						$users[] = $r;
					}
				}
			}

		}

		foreach ($users as $r) {
			$this->common->send_email($title, $message, $r->email);
		}

		$this->session->set_flashdata("globalmsg", lang("success_1"));
		redirect(site_url("admin/email_members"));
	}

	public function email_templates() 
	{
		//if(!$this->user->info->admin) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("email_templates" => 1)));
		$email_templates = $this->admin_model->get_email_templates();
		$this->template->loadContent("admin/email_templates.php", array(
			"email_templates" => $email_templates
			)
		);
	}

	public function edit_email_template($id) 
	{
		//if(!$this->user->info->admin) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("email_templates" => 1)));
		$id = intval($id);
		$email_template = $this->admin_model->get_email_template($id);
		if ($email_template->num_rows() == 0) {
			$this->template->error(lang("error_8"));
		}

		$this->template->loadContent("admin/edit_email_template.php", array(
			"email_template" => $email_template->row()
			)
		);
	}

	public function edit_email_template_pro($id) 
	{
		//if(!$this->user->info->admin) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("email_templates" => 1)));
		$id = intval($id);
		$email_template = $this->admin_model->get_email_template($id);
		if ($email_template->num_rows() == 0) {
			$this->template->error(lang("error_8"));
		}

		$title = $this->common->nohtml($this->input->post("title"));
		$message = $this->lib_filter->go($this->input->post("message"));

		if (empty($title) || empty($message)) {
			$this->template->error(lang("error_9"));
		}

		$this->admin_model->update_email_template($id, $title, $message);
		$this->session->set_flashdata("globalmsg", lang("success_7"));
		redirect(site_url("admin/email_templates"));
	}

	public function ipblock() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_members) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
			//array("admin" => array("ipblock" => 1)));

		$ipblock = $this->admin_model->get_ip_blocks();

		$this->template->loadContent("admin/ipblock.php", array(
			"ipblock" => $ipblock
			)
		);
	}

	public function add_ipblock() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_members) {
			//$this->template->error(lang("error_2"));
		//}
		$ip = $this->common->nohtml($this->input->post("ip"));
		$reason = $this->common->nohtml($this->input->post("reason"));

		if (empty($ip)) $this->template->error(lang("error_10"));

		$this->admin_model->add_ipblock($ip, $reason);
		$this->session->set_flashdata("globalmsg", lang("success_8"));
		redirect(site_url("admin/ipblock"));
	}

	public function delete_ipblock($id) 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_members) {
			//$this->template->error(lang("error_2"));
		//}
		$id = intval($id);
		$ipblock = $this->admin_model->get_ip_block($id);
		if ($ipblock->num_rows() == 0) $this->template->error(lang("error_11"));

		$this->admin_model->delete_ipblock($id);
		$this->session->set_flashdata("globalmsg", lang("success_9"));
		redirect(site_url("admin/ipblock"));
	}

	public function settings() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_settings) {
			//$this->template->error(lang("error_2"));
		//}
		//$this->template->loadData("activeLink", 
		//$attributes = array('class'=>'form-horizontal', 'form-bordered' );
		$this->template->loadContent("admin/settings.php", array(
			//"attributes" => $attributes
			)
		);
	}

	public function settings_pro() 
	{
		//if(!$this->user->info->admin && !$this->user->info->admin_settings) {
			//$this->template->error(lang("error_2"));
		//}
		$site_name = $this->common->nohtml($this->input->post("site_name"));
		$site_desc = $this->common->nohtml($this->input->post("site_desc"));
		$site_email = $this->common->nohtml($this->input->post("site_email"));
		$upload_path = $this->common->nohtml($this->input->post("upload_path"));
		$file_types = $this->common
			->nohtml($this->input->post("file_types"));
		$file_size = intval($this->input->post("file_size"));
		$upload_path_rel = 
			$this->common->nohtml($this->input->post("upload_path_relative"));
		$register = intval($this->input->post("register"));
		$avatar_upload = intval($this->input->post("avatar_upload"));
		$disable_captcha = intval($this->input->post("disable_captcha"));
		$date_format = $this->common->nohtml($this->input->post("date_format"));
		$login_protect = intval($this->input->post("login_protect"));
		$activate_account = intval($this->input->post("activate_account"));

		// Validate
		if (empty($site_name) || empty($site_email)) {
			$this->template->error(lang("error_23"));
		}
		$this->load->library("upload");

		if ($_FILES['userfile']['size'] > 0) {
			$this->upload->initialize(array( 
		       "upload_path" => $this->settings->info->upload_path,
		       "overwrite" => FALSE,
		       "max_filename" => 300,
		       "encrypt_name" => TRUE,
		       "remove_spaces" => TRUE,
		       "allowed_types" => $this->settings->info->file_types,
		       "max_size" => 2000,
		       "xss_clean" => TRUE
		    ));

		    if (!$this->upload->do_upload()) {
		    	$this->template->error(lang("error_21") 
		    	.$this->upload->display_errors());
		    }

		    $data = $this->upload->data();

		    $image = $data['file_name'];
		} else {
			$image= $this->settings->info->site_logo;
		}

		$this->admin_model->updateSettings(
			array(
				"site_name" => $site_name,
				"site_desc" => $site_desc,
				"upload_path" => $upload_path,
				"upload_path_relative" => $upload_path_rel, 
				"site_logo"=> $image,  
				"site_email" => $site_email,
				"register" => $register,
				"avatar_upload" => $avatar_upload,
				"file_types" => $file_types,
				"disable_captcha" => $disable_captcha,
				"date_format" => $date_format,
				"file_size" => $file_size,
				"login_protect" => $login_protect,
				"activate_account" => $activate_account
			)
		);
		$this->session->set_flashdata("globalmsg", lang("success_13"));
		redirect(site_url("admin/settings"));
	}

}

?>