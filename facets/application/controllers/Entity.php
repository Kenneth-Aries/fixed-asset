<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entity extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("entity_model");
	}

	public function index()
	{
		$entity = $this->entity_model->getEntity();
		// Loads HTML page
		$this->template->loadContent("entity/index.php", array(
			"entity" => $entity
			)
		);
	}

	public function addEntity() 
	{

		$this->entity_model->addEntity(array("entity_name" =>$entity_name));
		//$this->session->set_flashdata("globalmsg", lang("success_30"));
		redirect(base_url().'entity');
	}

	public function updateEntity($id) 
	{

		$entity = $this->entity_model->getEntityById($id);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		// Loads HTML page
		$this->template->loadContent("entity/edit_entity.php", array(
			"entity" => $entity->row()
			)
		);
	}

	public function updateEntityPro($id) 
	{

		$id = intval($id);

		$entity_name = $this->common->nohtml($this->input->post("entity_name"));

		$this->entity_model->updateEntity($id, 
			array(
				"entity_name" =>$entity_name
				)
		);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		redirect(site_url("entity"));
	}

	public function deleteEntity($id) 
	{

		$this->entity_model->deleteEntity($id);
		//$this->session->set_flashdata("globalmsg", lang("success_32"));
		redirect(site_url('entity'));
	}
}

?>