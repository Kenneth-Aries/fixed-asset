<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("status_model");
	}

	public function index()
	{
		$status = $this->status_model->getStatus();
		// Loads HTML page
		$this->template->loadContent("status/index.php", array(
			"status" => $status
			)
		);
	}

	public function addStatus() 
	{

		$this->status_model->addStatus(array("status_name" =>$status_name));
		//$this->session->set_flashdata("globalmsg", lang("success_30"));
		redirect(base_url().'status');
	}

	public function updateStatus($id) 
	{

		$status = $this->status_model->getStatusById($id);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		// Loads HTML page
		$this->template->loadContent("status/edit_status.php", array(
			"status" => $status->row()
			)
		);
	}

	public function updateStatusPro($id) 
	{

		$id = intval($id);

		$status_name = $this->common->nohtml($this->input->post("status_name"));

		$this->status_model->updateStatus($id, 
			array(
				"status_name" =>$status_name
				)
		);
		//$this->session->set_flashdata("globalmsg", lang("success_31"));
		redirect(site_url("status"));
	}

	public function deleteStatus($id) 
	{

		$this->status_model->deleteStatus($id);
		//$this->session->set_flashdata("globalmsg", lang("success_32"));
		redirect(site_url('status'));
	}
}

?>