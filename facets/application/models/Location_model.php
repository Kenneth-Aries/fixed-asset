<?php
	
class Location_model extends CI_Model
{

	public function getLocations($entity_id) 
	{
		return $this->db->where("entity_id", $entity_id)->get("location");
	}
	
}