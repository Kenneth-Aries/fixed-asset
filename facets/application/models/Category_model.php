<?php

class Category_model extends CI_Model 
{

	public function getCategory() 
	{
		return $this->db->get("category");
	}

	public function getCategoryByID($id) 
	{
		return $this->db->where("ID", $id)
			->get("category");
	}

	public function addCategory() 
	{
		$data['category_name']  =  $this->input->post('category_name');
		$this->db->insert('category' , $data);
	}

	public function updateCategory($id, $data)
	{
		$this->db->where("ID", $id)->update("category", $data);
	}

	public function deleteCategory($id) 
	{
		$this->db->where("ID", $id)->delete("category");
	}
}

?>