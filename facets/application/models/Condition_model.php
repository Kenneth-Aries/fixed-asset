<?php

class Condition_model extends CI_Model 
{

	public function getCondition() 
	{
		return $this->db->get("condition");
	}

	public function getConditionByID($id) 
	{
		return $this->db->where("ID", $id)
			->get("condition");
	}

	public function addCondition() 
	{
		$data['condition_name']  =  $this->input->post('condition_name');
		$this->db->insert('condition' , $data);
	}

	public function updateCondition($id, $data)
	{
		$this->db->where("ID", $id)->update("condition", $data);
	}

	public function deleteCondition($id) 
	{
		$this->db->where("ID", $id)->delete("condition");
	}
}

?>