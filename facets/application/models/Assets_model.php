<?php

class Assets_model extends CI_Model 
{

	public function getAssetByLocationId($location_id) 
	{
		return $this->db->select("asset.asset_id, asset.asset_barcode, asset.asset_number, asset.asset_description,asset.serial_number, asset.longitude, asset.latitude,
			location.location_name as location_name")
			->join("location", "asset.location_id = location.ID")
			->get("asset");
	}

	public function getAssetByBarcode($barcode) 
	{
		return $this->db->select("asset.asset_id, asset.asset_barcode, asset.asset_number, asset.asset_description,asset.serial_number, asset.longitude, asset.latitude,
			location.location_name as location_name")
			->join("location", "asset.location_id = location.ID")
			->where("asset_barcode", $barcode)
			->get("asset");
	}

	public function getAssetHistory($asset_id)
	{
		return $this->db->select("asset_tracker.ID, asset_tracker.asset_id, asset_tracker.date_time, asset_tracker.asset_number, asset_tracker.category_name,
			asset_tracker.asset_description, asset_tracker.make, asset_tracker.model,
			asset_tracker.serial_number, asset_tracker.user_id, asset_tracker.costcentre_id,
			asset_tracker.condition_id, asset_tracker.status_id, asset_tracker.longitude, 
			asset_tracker.latitude, cost_centre.cost_centre as cost_centre, 
			condition.condition_name as condition_name, status.status_name as status_name, 
			location.location_name as location_name, user.user_name as user_name")
			->join("cost_centre", "cost_centre.ID = asset_tracker.costcentre_id")
			->join("condition", "condition.ID = asset_tracker.condition_id")
			->join("status", "status.ID = asset_tracker.status_id")
			->join("location", "location.location_id = asset_tracker.location_id")
			->join("user", "user.IDr = asset_tracker.user_id")
			->where("asset_tracker.asset_id", $asset_id)
			->get("asset_tracker");
	}

	public function getAssetImages($idasset) 
	{
		return $this->db->where("asset_id", $asset_id)->get("asset_image");
	}
}

?>