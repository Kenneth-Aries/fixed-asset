<?php

class Status_model extends CI_Model 
{

	public function getStatus() 
	{
		return $this->db->get("status");
	}

	public function getStatusByID($id) 
	{
		return $this->db->where("ID", $id)
			->get("status");
	}

	public function addStatus() 
	{
		$data['status_name']  =  $this->input->post('status_name');
		$this->db->insert('status' , $data);
	}

	public function updateStatus($id, $data)
	{
		$this->db->where("ID", $id)->update("status", $data);
	}

	public function deleteStatus($id) 
	{
		$this->db->where("ID", $id)->delete("status");
	}
}

?>