<?php

class Entity_model extends CI_Model 
{

	public function getEntity() 
	{
		return $this->db->get("entity");
	}

	public function getEntityByID($id) 
	{
		return $this->db->where("ID", $id)
			->get("entity");
	}

	public function addEntity() 
	{
		$data['entity_name']  =  $this->input->post('entity_name');
		$this->db->insert('entity' , $data);
	}

	public function updateEntity($id, $data)
	{
		$this->db->where("ID", $id)->update("entity", $data);
	}

	public function deleteEntity($id) 
	{
		$this->db->where("ID", $id)->delete("entity");
	}
}

?>