<?php

class Cost_center_model extends CI_Model 
{

	public function getCost_center() 
	{
		return $this->db->get("cost_center");
	}

	public function getCost_centerByID($id) 
	{
		return $this->db->where("ID", $id)
			->get("cost_center");
	}

	public function addCost_center() 
	{
		$data['cost_center']  =  $this->input->post('cost_center');
		$this->db->insert('cost_center' , $data);
	}

	public function updateCost_center($id, $data)
	{
		$this->db->where("ID", $id)->update("cost_center", $data);
	}

	public function deleteCost_center($id) 
	{
		$this->db->where("ID", $id)->delete("cost_center");
	}
}

?>