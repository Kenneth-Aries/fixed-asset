<header class="page-header">
	<h2>Cost Center</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo base_url();?>">
					<i class="fa fa-home"></i> &nbsp;Home 
				</a>
			</li>
			<li><span>Cost Center</span></li>
			<li><span>Edit Cost Center</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-calendar"></i></a>
	</div>
</header>

<section role="main" class="content-body">
<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
	
					<h2 class="panel-title">Edit Cost Center</h2>
				</header>
				<div class="panel-body">
					<?php echo form_open(site_url("cost_center/updatecost_centerpro/" . $cost_center->ID)) ?>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">Cost Center Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="inputDefault" name="cost_center" value="<?php echo $cost_center->cost_center ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">&nbsp;</label>
							<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Update Cost Center</button>
						</div
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>
</section>