<header class="page-header">
	<h2>Status</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo base_url();?>">
					<i class="fa fa-home"></i> &nbsp;Home 
				</a>
			</li>
			<li><span>Status</span></li>
			<!--<li><span>Builder</span></li>-->
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-calendar"></i></a>
	</div>
</header>

<!-- start: page -->
<section class="content-with-menu" data-theme-gmap-builder>
<div class="content-with-menu-container">

	<div class="inner-menu-toggle">
		<a href="#" class="inner-menu-expand" data-open="inner-menu">
			Show Forms <i class="fa fa-chevron-right"></i>
		</a>
	</div>

	<menu id="content-menu" class="inner-menu" role="menu">
		<div class="nano">
			<div class="nano-content">

				<div class="inner-menu-toggle-inside">
					<a href="#" class="inner-menu-collapse">
						<i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> Hide Forms
					</a>
					<a href="#" class="inner-menu-expand" data-open="inner-menu">
						Show Forms <i class="fa fa-chevron-down"></i>
					</a>
				</div>

				<div class="inner-menu-content">

					<h4 class="title">Status Form</h4>
                    
                    <hr class="separator" />

                    <?php echo form_open(site_url("status/addStatus")) ?>

					<div class="form-group">
						<div class="row">
							<label class="col-xs-12 control-label" for="status">Status</label>
							<div class="col-xs-12">
								<input id="status" name="status_name" class="form-control" type="text" placeholder="Enter the name..." required>
							</div>
						</div>
					</div>

					<hr class="separator" />

					<div class="form-group">

						<div class="row">
							<div class="col-xs-12">
								<input type="submit" class="btn btn-default btn-block mb-lg" id="submit" value="Add Status">
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
                    
				</div>
			</div>
		</div>
	</menu>
	<div class="inner-body">
    <!-- start: page -->
		<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
				</div>
		
				<h2 class="panel-title">Status</h2>
			</header>
			<div class="panel-body">
				<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?php echo base_url();?>assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
					<thead>
						<tr>
							<th width="20%">ID</th>
							<th width="60%">Name</th>
							<th width="20%">Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($status->result() as $r) : ?>
							<tr class="gradeX">
								<td><?php echo $r->ID ?></td>
								<td><?php echo $r->status_name ?></td>
								<td class="center hidden-phone">
									<a href="<?php echo base_url("status/updatestatus/" . $r->ID) ?>" title="edit"><span><i class="fa fa-pencil"></i></span></a> &nbsp;
									<a href="<?php echo base_url("status/deletestatus/" . $r->ID) ?>" onclick="if (confirm('Are you sure you mant to delete the status')) commentDelete(1); return false" title="delete"><span><i class="fa fa-trash"></i></span></a>
								</td>
							</tr>
					    <?php endforeach; ?>
						
					</tbody>
				</table>
			</div>
		</section>

	<!-- end: page -->
	</div>
</section>

<!-- end: page -->