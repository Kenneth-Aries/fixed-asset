<!doctype html>
<html class="fixed sidebar-left-collapsed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo lang("ctn_1") ?></title>
		<meta name="keywords" content="Facets" />
		<meta name="description" content="Facets Admin">
		<meta name="author" content="stephenzivanayi.co.za">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/pnotify/pnotify.custom.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dropzone/css/basic.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/dropzone/css/dropzone.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/summernote/summernote.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/summernote/summernote-bs3.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/codemirror/lib/codemirror.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/codemirror/theme/monokai.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.js"></script>

		<!-- SCRIPTS -->
		<script type="text/javascript">
        var global_base_url = "<?php echo site_url('/') ?>";
        </script>

		<!-- CODE INCLUDES -->
        <?php echo $cssincludes ?>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="<?php echo base_url();?>" class="logo">
						<img src="<?php echo base_url();?>assets/images/facetsLogo.png" height="45" alt="Facets" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo base_url();?>assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<span class="name">Trevon</span>
								<span class="role">administrator</span>
							</div>
							<i class="fa custom-caretd"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> <?php echo lang("ctn_4") ?></a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo site_url("login/logout/" . $this->security->get_csrf_hash()) ?>"><i class="fa fa-power-off"></i> <?php echo lang("ctn_3") ?></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							<?php echo lang("ctn_5") ?>
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-parent nav-active">
										<a href="#">
											<i class="fa fa-empire" aria-hidden="true"></i>
											<span>Administration</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo site_url("admin/settings") ?>">
													 Global Setting
												</a>
											</li>
											<li>
												<a href="<?php echo site_url("admin/user_roles") ?>">
													 User Roles
												</a>
											</li>
											<li>
												<a href="<?php echo site_url("admin/ipblock") ?>">
													 IP Blocking
												</a>
											</li>
											<li>
												<a href="<?php echo site_url("admin/email_templates") ?>">
													 Email Templates
												</a>
											</li>
											<li>
												<a href="<?php echo site_url("admin/email_members") ?>">
													 Email Members
												</a>
											</li>
											<li>
												<a href="<?php echo site_url("admin/payment_plans") ?>">
													 Payment Plans
												</a>
											</li>
										</ul>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("assets");?>">
											<i class="fa fa-university" aria-hidden="true"></i>
											<span><?php echo lang("ctn_6") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("locations");?>">
											<i class="fa fa-map-marker" aria-hidden="true"></i>
											<span><?php echo lang("ctn_7") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("entity");?>">
											<i class="fa fa-briefcase" aria-hidden="true"></i>
											<span><?php echo lang("ctn_8") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("category");?>">
											<i class="fa fa-star" aria-hidden="true"></i>
											<span><?php echo lang("ctn_9") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("status");?>">
											<i class="fa fa-question-circle" aria-hidden="true"></i>
											<span><?php echo lang("ctn_10") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("cost_center");?>">
											<i class="fa fa-credit-card" aria-hidden="true"></i>
											<span><?php echo lang("ctn_11") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("condition");?>">
											<i class="fa fa-plus-square" aria-hidden="true"></i>
											<span><?php echo lang("ctn_12") ?></span>
										</a>
									</li>
									<li class="nav-active">
										<a href="<?php echo base_url("users");?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span><?php echo lang("ctn_13") ?></span>
										</a>
									</li>
								</ul>
							</nav>
				
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					
					<?php echo $content ?>

				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Calender</h6>
								<hr class="separator" />
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
								<hr class="separator" />
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="<?php echo base_url();?>assets/vendor/select2/select2.js"></script>
		<script src="<?php echo base_url();?>http://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="<?php echo base_url();?>assets/javascripts/maps/snazzy.themes.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-autosize/jquery.autosize.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/pnotify/pnotify.custom.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/select2/select2.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/fuelux/js/spinner.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/dropzone/dropzone.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-markdown/js/markdown.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/lib/codemirror.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/addon/selection/active-line.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/addon/edit/matchbrackets.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/mode/javascript/javascript.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/mode/xml/xml.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/codemirror/mode/css/css.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/summernote/summernote.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
		<script src="<?php echo base_url();?>assets/vendor/ios7-switch/ios7-switch.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="<?php echo base_url();?>assets/javascripts/maps/examples.map.builder.js"></script>
		<script src="<?php echo base_url();?>assets/javascripts/tables/examples.datatables.tabletools.js"></script>
		<script src="<?php echo base_url();?>assets/javascripts/ui-elements/examples.modals.js"></script>
	</body>
</html>