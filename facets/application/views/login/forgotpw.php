<div class="center-sign">
	<a href="<?php echo base_url();?>" class="logo pull-left">
		<img src="<?php echo base_url();?>assets/images/facetsLogo.png" height="45" alt="Facets" />
	</a>

	<div class="panel panel-sign">
		<div class="panel-title-sign mt-xl text-right">
			<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-magic"></i>&nbsp; Forgot</h2>
		</div>
		<div class="panel-body">
			<?php echo form_open(site_url("login/forgotpw_pro/")) ?>
				<div class="form-group mb-lg">
					<label>Enter Your Email Address</label>
					<div class="input-group input-group-icon">
						<input name="email" type="text" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-envelope"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-8">
						<div class="input-group">
							<a href="<?php echo base_url();?>">
								<i class="fa fa-reply"></i>
								<label for="none">Cancel and go back</label>
							</a>
						</div>
					</div>
					<div class="col-sm-4 text-right">
						<button type="submit" class="btn btn-primary hidden-xs">Submit</button>
						<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Submit</button>
					</div>
				</div>
			<?php echo form_close() ?>
		</div>
	</div>

	<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
</div>