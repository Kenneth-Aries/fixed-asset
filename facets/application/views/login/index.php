<div class="center-sign">
	<a href="<?php echo base_url();?>" class="logo pull-left">
		<img src="<?php echo base_url();?>assets/images/facetsLogo.png" height="45" alt="Facets" />
	</a>

	<div class="panel panel-sign">
		<div class="panel-title-sign mt-xl text-right">
			<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-lock"></i>&nbsp; Login</h2>
		</div>
		<div class="panel-body">
			<?php echo form_open(site_url("login/pro")) ?>
				<div class="form-group mb-lg">
					<label>Username or Email</label>
					<div class="input-group input-group-icon">
						<input name="email" type="text" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-user"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="form-group mb-lg">
					<div class="clearfix">
						<label class="pull-left">Password</label>
						<a href="<?php echo base_url("login/forgotpw") ?>" class="pull-right">Lost Password?</a>
					</div>
					<div class="input-group input-group-icon">
						<input name="pass" type="password" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-lock"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-8">
						<div class="checkbox-custom checkbox-default">
							<input id="remeber" name="remember" type="checkbox"/>
							<label for="remeber">Remember Me</label>
						</div>
					</div>
					<div class="col-sm-4 text-right">
						<button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
						<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
					</div>
				</div>

			<?php echo form_close() ?>
		</div>
	</div>

	<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
</div>