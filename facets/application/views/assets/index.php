<header class="page-header">
	<h2>Assets</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo base_url();?>">
					<i class="fa fa-home"></i> &nbsp;Home 
				</a>
			</li>
			<li><span>Assets</span></li>
			<!--<li><span>Builder</span></li>-->
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-calendar"></i></a>
	</div>
</header>

<!-- start: page -->
<section class="content-with-menu" data-theme-gmap-builder>
<div class="content-with-menu-container">

	<div class="inner-menu-toggle">
		<a href="#" class="inner-menu-expand" data-open="inner-menu">
			Show Forms <i class="fa fa-chevron-right"></i>
		</a>
	</div>

	<menu id="content-menu" class="inner-menu" role="menu">
		<div class="nano">
			<div class="nano-content">

				<div class="inner-menu-toggle-inside">
					<a href="#" class="inner-menu-collapse">
						<i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> Hide Forms
					</a>
					<a href="#" class="inner-menu-expand" data-open="inner-menu">
						Show Forms <i class="fa fa-chevron-down"></i>
					</a>
				</div>

				<div class="inner-menu-content">

					<h4 class="title">Assets Form</h4>
                    
                    <hr class="separator" />

                    <?php echo form_open(site_url("category/addCategory")) ?>

					<div class="form-group">
						<div class="row">
							<label class="col-xs-12 control-label" for="category">Entity Name</label>
							<div class="col-xs-12">
								<select class="form-control select_entity" id="entity_list" name="entity_list"></select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<label class="col-xs-12 control-label" for="category">Location</label>
							<div class="col-xs-12">
								<select class="form-group select_entity" id="location_list" name="location_list"></select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<label class="col-xs-12 control-label" for="category">Asset Barcode</label>
							<div class="col-xs-12">
								<input id="category" name="category_name" class="form-control" type="text" placeholder="Enter the name..." required>
							</div>
						</div>
					</div>

					<hr class="separator" />

					<div class="form-group">

						<div class="row">
							<div class="col-xs-12">
								<input type="submit" class="btn btn-default btn-block mb-lg" id="submit" value="Add Asset">
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
                    
				</div>
			</div>
		</div>
	</menu>
	<div class="inner-body">
    <!-- start: page -->
		<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
				</div>
		
				<h2 class="panel-title">Categories</h2>
			</header>
			<div class="panel-body">
				<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?php echo site_url();?>assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
					<thead>
						<tr>
							<th width="20%">ID</th>
							<th width="60%">Name</th>
							<th width="20%">Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($category->result() as $r) : ?>
							<tr class="gradeX">
								<td><?php echo $r->ID ?></td>
								<td><?php echo $r->category_name ?></td>
								<td class="center hidden-phone">
									<a href="<?php echo base_url("category/updatecategory/" . $r->ID) ?>" title="edit"><span><i class="fa fa-pencil"></i></span></a> &nbsp;
									<a href="<?php echo base_url("category/deletecategory/" . $r->ID) ?>" onclick="if (confirm('Are you sure you mant to delete the category')) commentDelete(1); return false" title="delete"><span><i class="fa fa-trash"></i></span></a>
								</td>
							</tr>
					    <?php endforeach; ?>
						
					</tbody>
				</table>
			</div>
		</section>

	<!-- end: page -->
	</div>
</section>

<!-- end: page -->
<script>   
  
     (function($) {
         
         
     jQuery.noConflict();
        jQuery(document).ready(function(){               
            jQuery("#get_assets").click(function() {

                 if(document.getElementById('asset_barcode').value == ""){
                    alert("Please enter an asset barcode.");
                 }else{
                    jQuery(this).pop_asset_by_barcode(); 
                 }

                   

            });
    });

          
      jQuery.fn.pop_assets_by_location = function(){
        //alert("HERE 5");
          
        //var entity_list = document.getElementById("entity_list");
        //var identity = entity_list.options[entity_list.selectedIndex].value;
        //alert("HERE "+identity);  
        $('#asset_table').empty();
          
        $('#asset_table').append('<th class ="user_th">Location Name</th><th class ="user_th">Barcode</th><th class ="user_th">Asset Number</th><th class ="user_th">Description</th><th class ="user_th">Serial Number</th><th class ="user_th">Last Location</th>');
        jQuery(document).ready(function(){
            
        var location_list = document.getElementById("location_list");
        var idlocation = location_list.options[location_list.selectedIndex].value;
            
        //alert("HERE 46 " + location_list.selectedIndex);
        //alert("HERE 47 " + idlocation);
        $.ajax ({
                type: "POST",
                url: "asset_api.php",
                data: {populate_assets_by_location: 'populate_assets_by_location', idlocation:idlocation} , //optional
                success: function( data ) {
                    
                    //alert("pop_assets_by_location - " + data );
                    
                      var opts = $.parseJSON(data);

                        $.each(opts, function(i, d) {
                            //alert("HERE 1" + d.iduser);
                            //$('#entity_table tr').attr(d.identity);
                            $('#asset_table').append('<tr id="' + d.idasset + '" data-idasset="'
                                                    + d.idasset + '"><td class ="user_td">'
                                                    + d.location_name + '</td><td class ="user_td">' 
                                                    + d.asset_barcode + '</td><td class ="user_td">' 
                                                    + d.asset_number + '</td><td class ="user_td">' 
                                                    + d.asset_description + '</td><td class ="user_td">'
                                                    + d.serial_number + '</td><td class ="user_td">'   
                                                    + d.longitude + ' / ' + d.latitude + '</td></tr>');
                            
                        });
                    jQuery(this).addRowHandlers();
                    }
                });
                
                //var emp = $('#4');
                //var entity_name = $('tr[data-entity_id="4"]').data("entity_name");
                //alert(entity_name + ' ' + emp.data("entity_name"));
                //alert("Nothing");
                //jQuery(this).addRowHandlers();
            
            });
     };   
         
         
    jQuery.fn.pop_asset_by_barcode = function(){
        
        
        //alert("HERE 5");
          
        //var entity_list = document.getElementById("entity_list");
        //var identity = entity_list.options[entity_list.selectedIndex].value;
        //alert("HERE "+identity);  
        $('#asset_table').empty();
          
        $('#asset_table').append('<th class ="user_th">Location Name</th><th class ="user_th">Barcode</th><th class ="user_th">Asset Number</th><th class ="user_th">Description</th><th class ="user_th">Serial Number</th><th class ="user_th">Last Location</th>');
        jQuery(document).ready(function(){
            
        var barcode = document.getElementById('asset_barcode').value;
        
        //alert("HERE 46 " + barcode);
        //alert("HERE 47 " + idlocation);
        $.ajax ({
                type: "POST",
                url: "asset_api.php",
                data: {populate_asset_by_barcode: 'populate_asset_by_barcode', barcode:barcode} , //optional
                success: function( data ) {
                    
                    //alert("pop_assets_by_location - " + data );
                    
                      var opts = $.parseJSON(data);

                        $.each(opts, function(i, d) {
                            //alert("HERE 1" + d.iduser);
                            //$('#entity_table tr').attr(d.identity);
                            $('#asset_table').append('<tr id="' + d.idasset + '" data-idasset="'
                                                    + d.idasset + '"><td class ="user_td">'
                                                    + d.location_name + '</td><td class ="user_td">' 
                                                    + d.asset_barcode + '</td><td class ="user_td">' 
                                                    + d.asset_number + '</td><td class ="user_td">' 
                                                    + d.asset_description + '</td><td class ="user_td">'
                                                    + d.serial_number + '</td><td class ="user_td">'
                                                    + d.longitude + ' / ' + d.latitude + '</td></tr>');
                            
                        });
                    jQuery(this).addRowHandlers();
                    }
                });
                
                //var emp = $('#4');
                //var entity_name = $('tr[data-entity_id="4"]').data("entity_name");
                //alert(entity_name + ' ' + emp.data("entity_name"));
                //alert("Nothing");
                //jQuery(this).addRowHandlers();
            
            });
     };        
         
    jQuery.fn.addRowHandlers = function(){
    //function addRowHandlers() {
    var table = document.getElementById("asset_table");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) 
            {
                
                return function() { 
                    
                                    var idasset = row.getAttributeNode("id").value;
                                    
                                    //alert("idasset " + idasset);
                                    $('#asset_table').empty();

                                    $('#asset_table').append('<th class ="user_th">Asset Number</th><th class ="user_th">Location Name</th><th class ="user_th">Category Name</th><th class ="user_th">Description</th><th class ="user_th">Make</th><th class ="user_th">Model</th><th class ="user_th">Serial Number</th><th class ="user_th">Cost Center</th><th class ="user_th">Condition</th><th class ="user_th">Status</th><th class ="user_th">Longitude/Latitude</th><th class ="user_th">User Name</th><th class ="user_th">Date</th>');

                                    
                                    //var user_title_list = document.getElementById("user_title_list");
                                    //var iduser_title = user_title_list.options[user_title_list.selectedIndex].value;
                                    //sessionStorage.setItem("selected_title", iduser_title);
                                    //alert("iduser title . ".concat( iduser_title ));
                                    $.ajax ({
                                        type: "POST",
                                        url: "asset_api.php",
                                        data: {populate_asset_history : 'populate_asset_history', idasset: idasset } , //optional
                                        success: function( data ) {
                                            //alert("popuser . ".concat( data ));
                                              var opts = $.parseJSON(data);

                                                $.each(opts, function(i, d) {
                                                    $('#asset_table').append('<tr id="' + d.idasset + '" data-idasset="'
                                                    + d.idasset + '"><td class ="user_td">'
                                                    + d.asset_number + '</td><td class ="user_td">' 
                                                    + d.location_name + '</td><td class ="user_td">' 
                                                    + d.category_name + '</td><td class ="user_td">' 
                                                    + d.asset_description + '</td><td class ="user_td">'
                                                    + d.make + '</td><td class ="user_td">'
                                                    + d.model + '</td><td class ="user_td">'
                                                    + d.serial_number + '</td><td class ="user_td">'
                                                    + d.cost_centre + '</td><td class ="user_td">'
                                                    + d.condition_name + '</td><td class ="user_td">'
                                                    + d.status_name + '</td><td class ="user_td">'
                                                    + d.longitude + ' / ' + d.latitude + '</td><td class ="user_td">'
                                                    + d.user_name + '</td><td class ="user_td">'
                                                    + d.date_time + '</td></tr>');
                            
                                                });
                                            jQuery(this).addRowHandlers();
                                            }
                                        });
                    
                    
                                        jQuery(this).popImages(idasset);
                    
//                                        jQuery('#add_location_state').show();
//                                        jQuery('#add_location').hide();
//                                        jQuery('#update_location').show();
//                    

                    /*
                                        //var id = cell.innerHTML;
                                        //alert("Name:" + row.getAttributeNode("data-entity_name").value);
                                        jQuery('#entity_name').val(row.getAttributeNode("data-user_name").value);
                                        jQuery('#entity_name').attr("entity_id",row.getAttributeNode("id").value);
                                        jQuery('#add_entity').hide();
                                        jQuery('#update_user').show();
                                        jQuery('#add_entity_state').show();  
                      */                                                                                    
                                 };
                
    
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}
         

    jQuery.fn.pop_location_details = function(){
        //alert("HERE 5");
          
        //var entity_list = document.getElementById("entity_list");
        //var identity = entity_list.options[entity_list.selectedIndex].value;
        //alert("HERE "+identity);  

        jQuery('#location_name').val("");
        jQuery('#building').val("");
        jQuery('#region').val("");
        jQuery('#country').val("");
        jQuery('#barcode').val("");
        jQuery('#longitude').val("");
        jQuery('#latitude').val("");
        
        
        jQuery(document).ready(function(){
            
        var location_list = document.getElementById("location_list");
        var idlocation = location_list.options[location_list.selectedIndex].value;
            
        //alert("HERE 45 " + idlocation);
        $.ajax ({
                type: "POST",
                url: "location_api.php",
                data: {populate_location_details : 'populate_location_details', idlocation: idlocation } , //optional
                success: function( data ) {
                    //alert("popuser . ".concat( data ));
                      var opts = $.parseJSON(data);

                        $.each(opts, function(i, d) {
                            //alert("HERE 1" + d.identity);
                            jQuery('#location_name').attr("idlocation",d.idlocation);
                            sessionStorage.setItem("selected_user", d.iduser);
                            jQuery('#location_name').val(d.location_name);
                            jQuery('#building').val(d.building);
                            jQuery('#region').val(d.region);
                            jQuery('#country').val(d.country);
                            jQuery('#barcode').val(d.barcode);
                            jQuery('#longitude').val(d.longitude);
                            jQuery('#latitude').val(d.latitude)
                            //sessionStorage.setItem("selected_title", $('#user_title_list').val());
                            //jQuery(this).pop_user_name(d.identity);
                            //jQuery('#user_name').val(d.user_name);


                        });
                    //jQuery(this).addRowHandlers();
                    }
                });

                //var emp = $('#4');
                //var entity_name = $('tr[data-entity_id="4"]').data("entity_name");
                //alert(entity_name + ' ' + emp.data("entity_name"));
                //alert("Nothing");
                //jQuery(this).addRowHandlers();
            
            });
     };  
    
    
    
        jQuery(document).ready(function(){
            
        $( "#location_list" ).change(function() {
            $('#image_list').empty();
            //alert("in country change");
//            var entity_list = document.getElementById("location_list");
//            var identity = entity_list.options[entity_list.selectedIndex].value;
            //alert(identity);
            //sessionStorage.setItem("selected_entity", identity);
            //jQuery(this).set_to_default_state();
            jQuery(this).pop_location_details();
            jQuery('#asset_barcode').val("");
            jQuery(this).pop_assets_by_location();
            
            //jQuery(this).pop_user_name(identity);
        
        });    
        /*
        $( "#user_title_list" ).change(function() {
            
            //alert("in country change");
            //sessionStorage.setItem("selected_title", $('#user_title_list').val());
            
        });
        */
        
     });  
    
    
         
    jQuery(document).ready(function(){
            
        $( "#entity_list" ).change(function() {
            $('#image_list').empty();
            //alert("in country change");
            var entity_list = document.getElementById("entity_list");
            var identity = entity_list.options[entity_list.selectedIndex].value;
            //alert(identity);
            sessionStorage.setItem("selected_entity", identity);
            //jQuery(this).set_to_default_state();
            jQuery(this).popLocations();
            //jQuery(this).pop_user_name(identity);
        
        });    
        /*
        $( "#user_title_list" ).change(function() {
            
            //alert("in country change");
            //sessionStorage.setItem("selected_title", $('#user_title_list').val());
            
        });
        */
        
     });   
         
      jQuery.noConflict();
        jQuery.fn.popEntities = function(){
        //alert("HERE 5");
        $('#entity_list').empty();
        $('#image_list').empty();
        var selected_entity = sessionStorage.getItem('selected_entity'); 
        //alert("HERE 4");
        $.ajax ({
                type: "POST",
                url: "entity_api.php",
                data: {populate_entities : 'populate_entities'} , //optional
                success: function( data ) {
                    //alert("countryChange . ".concat( data ));
                  var opts = $.parseJSON(data);

                        $('#entity_list').append('<option value="none">Select an Entity</option>');
                
                        $.each(opts, function(i, d) {
                            //alert("HERE " + selectedcountryid + " " + d.idcountry);  
                            if(d.identity === selected_entity){
                               // alert("BOOOM!! " + selectedcountryid + " " + d.idcountry);  
                                $('#entity_list').append('<option selected="selected" value="' + d.identity + '">' + d.entity_name + '</option>');
                            }else{
                                $('#entity_list').append('<option value="' + d.identity + '">' + d.entity_name + '</option>');
                            }
                        });
                    }
            
            });
        };
    
         
        jQuery.noConflict();
        jQuery.fn.popLocations = function(){
        //alert("HERE 5");
        $('#location_list').empty();
        $('#image_list').empty();
        //var selected_entity = sessionStorage.getItem('selected_entity'); 
        //alert("HERE 4");
        $.ajax ({
                type: "POST",
                url: "location_api.php",
                data: {populate_locations: 'populate_locations', identity: sessionStorage.getItem('selected_entity')} , //optional
                success: function( data ) {                    //alert("countryChange . ".concat( data ));
                  var opts = $.parseJSON(data);

                        $('#location_list').append('<option value="none">Select a Location</option>');
                
                        $.each(opts, function(i, d) {
                            //alert("HERE " + selectedcountryid + " " + d.idcountry);  
//                            if(d.idlocation === selected_entity){
//                                //alert("BOOOM!! " + selectedcountryid + " " + d.idcountry);  
//                                $('#location_list').append('<option selected="selected" value="' + d.idlocation + '">' + d.location_name + '</option>');
//                            }else{
                                $('#location_list').append('<option value="' + d.idlocation + '">' + d.location_name + '</option>');
//                            }
                        });
                    }
            
            });
        };
         
         
        jQuery.noConflict();
        jQuery.fn.popImages = function(idasset){
        //alert("HERE 5");
        $('#image_list').empty();
        //var selected_entity = sessionStorage.getItem('selected_entity'); 
        //alert("HERE 4");
        $.ajax ({
                type: "POST",
                url: "asset_api.php",
                data: {populate_asset_images: 'populate_asset_images', idasset:idasset} , //optional
                success: function( data ) {                    //alert("countryChange . ".concat( data ));
                  var opts = $.parseJSON(data);

//                        $('#image_list').append('<option value="none">Select a Location</option>');
                
                        $.each(opts, function(i, d) {
                            $('#image_list').append('<div style="float: left;height: auto; width: 150;"><img src="' + d.asset_url + '"></div>');
                        });
                    }
            
            });
        }; 
         
         
         
//        jQuery.noConflict();
//        jQuery.fn.popLocations = function(){
//        //alert("HERE 5");
//        $('#location_list').empty();
//        //var selected_entity = sessionStorage.getItem('selected_entity'); 
//        //alert("HERE 4");
//        $.ajax ({
//                type: "POST",
//                url: "location_api.php",
//                data: {populate_locations: 'populate_locations', identity: sessionStorage.getItem('selected_entity')} , //optional
//                success: function( data ) {                    //alert("countryChange . ".concat( data ));
//                  var opts = $.parseJSON(data);
//
//                        $('#location_list').append('<option value="none">Select a Location</option>');
//                
//                        $.each(opts, function(i, d) {
//                            //alert("HERE " + selectedcountryid + " " + d.idcountry);  
////                            if(d.idlocation === selected_entity){
////                                //alert("BOOOM!! " + selectedcountryid + " " + d.idcountry);  
////                                $('#location_list').append('<option selected="selected" value="' + d.idlocation + '">' + d.location_name + '</option>');
////                            }else{
//                                $('#location_list').append('<option value="' + d.idlocation + '">' + d.location_name + '</option>');
////                            }
//                        });
//                    }
//            
//            });
//        }; 
//         
//         
         
//        jQuery.fn.pop_user_name = function(identity){
//        //alert("identity");
//            jQuery(document).ready(function(){
//                $('#user_list').empty();
//                var selected_user = sessionStorage.getItem('selected_user'); 
//
//                //var entity_list = document.getElementById("entity_list");
//                //var identity = entity_list.options[entity_list.selectedIndex].value;
//
//                //alert("HERE 4");
//                $.ajax ({
//                        type: "POST",
//                        url: "user_api.php",
//                        data: {populate_users : 'populate_users',identity:identity } , //optional
//                        success: function( data ) {
//                            var opts = $.parseJSON(data);
//                            //alert(data);
//                            $('#user_list').append('<option value="none">Select a user</option>');
//
//                                $.each(opts, function(i, d) {
//                                    //alert("HERE " + selectedcountryid + " " + d.idcountry);
//                                    //alert("BOOOM!! " + d.iduser_title + " " + sessionStorage.getItem('selected_title'));  
//                                    if(d.iduser === sessionStorage.getItem('selected_user')){
//
//                                        $('#user_list').append('<option selected="selected" value="' + d.iduser + '">' + d.user_name + '</option>');
//                                    }else{
//                                        $('#user_list').append('<option value="' + d.iduser + '">' + d.user_name + '</option>');
//                                    }
//                                });
//
//                            }
//                        });
//
//                        //var emp = $('#4');
//                        //var user_title_name = $('tr[data-user_title_id="4"]').data("user_title_name");
//                        //alert(user_title_name + ' ' + emp.data("user_title_name"));
//                        //alert("Nothing");
//                        //jQuery(this).addRowHandlers();
//
//            });
//     };
         
         
         jQuery.fn.set_to_default_state = function(){
            jQuery(document).ready(function(){ 
                
                //alert("set_to_default_state");
                sessionStorage.setItem("selected_entity", "none"); 
                sessionStorage.setItem("selected_user", "none");
                jQuery('#location_name').val("");
                jQuery('#building').val("");
                jQuery('#region').val("");
                jQuery('#country').val("");
                jQuery('#barcode').val("");
                jQuery('#longitude').val("");
                jQuery('#latitude').val("");
                jQuery('#asset_barcode').val("");
                
//                jQuery('#add_location').show();
//                jQuery('#update_location').hide();
                jQuery(this).popEntities();
                //$('#user_list').empty();
                //jQuery(this).pop_user_name();
                
             });
     };
         
         
    })(jQuery);
     
     
      window.onload = function() {
         //alert("onload");
        
        (function($) {
            jQuery(document).ready(function(){
                
                jQuery(this).set_to_default_state();
                
            });
        })(jQuery);
    };
     
</script>