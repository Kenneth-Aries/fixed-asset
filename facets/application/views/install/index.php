<div class="center-sign">
	<a href="<?php echo base_url();?>" class="logo pull-left">
		<img src="<?php echo base_url();?>assets/images/facetsLogo.png" height="45" alt="Facets" />
	</a>

	<div class="panel panel-sign">
		<div class="panel-title-sign mt-xl text-right">
			<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-lock"></i>&nbsp; Install</h2>
		</div>
		<div class="panel-body">
			<?php echo form_open(site_url("install/install_pro")) ?>
				<div class="form-group mb-lg">
					<label>Admin Username</label>
					<div class="input-group input-group-icon">
						<input id="username-in" name="username" type="text" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-user"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="form-group mb-lg">
					<label>Admin Email</label>
					<div class="input-group input-group-icon">
						<input id="username-in" name="email" type="email" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-envelope"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="form-group mb-lg">
					<label>Password</label>
					<div class="input-group input-group-icon">
						<input type="password" id="password-in" name="password" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-lock"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="form-group mb-lg">
					<label>Repeat Password</label>
					<div class="input-group input-group-icon">
						<input type="password" id="password2-in" name="password2" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-lock"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="form-group mb-lg">
					<label>Site Name</label>
					<div class="input-group input-group-icon">
						<input id="username-in" name="site_name" type="text" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-globe"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="form-group mb-lg">
					<label>Site Description</label>
					<div class="input-group input-group-icon">
						<input id="username-in" name="site_desc" type="text" class="form-control input-lg" />
						<span class="input-group-addon">
							<span class="icon icon-lg">
								<i class="fa fa-info"></i>
							</span>
						</span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-8">
						<div class="checkbox-custom checkbox-default">
							&nbsp;
						</div>
					</div>
					<div class="col-sm-4 text-right">
						<button type="submit" class="btn btn-primary hidden-xs">Install</button>
						<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
					</div>
				</div>
			<?php echo form_close() ?>
		</div>
	</div>

	<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
</div>