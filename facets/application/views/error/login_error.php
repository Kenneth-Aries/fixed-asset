<div class="center-sign">
	<a href="<?php echo base_url();?>" class="logo pull-left">
		<img src="<?php echo base_url();?>assets/images/facetsLogo.png" height="45" alt="Facets" />
	</a>

	<div class="panel panel-sign">
		<div class="panel-title-sign mt-xl text-right">
			<h2 class="title text-uppercase text-bold m-none">Oops<i class="fa fa-exclamation-triangle"></i> </h2>
		</div>
		<div class="panel-body">
			
			<div class="alert alert-danger">
				<strong><i class="fa fa-exclamation-triangle"></i> Error Notice:</strong> <?php echo $message ?></p>
				<p><button class="btn btn-info mt-xs mb-xs" type="button" onclick="window.history.back()">Go Back</button></p>
			</div>

		</div>
	</div>

	<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
</div>