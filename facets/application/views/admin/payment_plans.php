<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Payment Plans</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a class="modal-with-form" href="#modalForm"><button class="btn btn-primary">Add Plan</button></a>
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">Payment Plans</h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered mb-none">
							<thead>
								<tr>
									<td>Plan Name</td>
									<td>Cost</td>
									<td>Days Given</td>
									<td>Sales</td>
									<td>Options</td>
								</tr>
							</thead>
							<tbody>
							<?php foreach($plans->result() as $r) : ?>
								<tr>
									<td><?php echo $r->name ?></td>
									<td><?php echo number_format($r->cost, 2) ?></td>
									<td><?php echo $r->days ?></td>
									<td><?php echo $r->sales ?></td>
									<td><a href="<?php echo site_url("admin/edit_payment_plan/" . $r->ID) ?>" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;<a href="<?php echo site_url("admin/delete_payment_plan/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" title="Delete"><i class="fa fa-trash-o"></i></a></td>
								<tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Modal Form -->
				<div id="modalForm" class="modal-block modal-block-primary mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<h2 class="panel-title">Add Payment Plan</h2>
						</header>
						<div class="panel-body">
						<?php echo form_open(site_url("admin/add_payment_plan")) ?>
								<div class="form-group mt-lg">
									<label class="col-sm-3 control-label">Plan Name</label>
									<div class="col-sm-9">
										<input type="text" id="email-in" name="name" class="form-control" placeholder="Enter The Name..." required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Description</label>
									<div class="col-sm-9">
										<input type="text" id="username" name="description" class="form-control" placeholder="Enter The Description..." required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Credit Costs</label>
									<div class="col-sm-9">
										<input type="text" id="username" name="cost" class="form-control" placeholder="Enter The Cost..." required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Background Color</label>
									<div class="col-sm-9">
										<input type="text" id="username" name="color" class="form-control" placeholder="Enter The Background Color..." required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Font Color</label>
									<div class="col-sm-9">
										<input type="text" id="username" name="fontcolor" class="form-control" placeholder="Enter The Font Color..." required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Days Given</label>
									<div class="col-sm-9">
										<input type="text" id="username" name="days" class="form-control" placeholder="Enter The Reason..." required/>
									</div>
								</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<input type="submit" class="btn btn-primary" value="Add Plan" />
									<button class="btn btn-default modal-dismiss">Cancel</button>
						<?php echo form_close() ?>
								</div>
							</div>
						</footer>						
					</section>
				</div>
			</section>
		</div>
	</div>