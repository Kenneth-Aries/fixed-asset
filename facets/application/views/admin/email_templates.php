<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Edit Email Template</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">Edit Email Template</h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered mb-none">
							<thead>
								<tr>
									<td>Title</td>
									<td>Options</td>
								</tr>
							</thead>
							<tbody>
							<?php foreach($email_templates->result() as $r) : ?>
								<tr>
									<td><?php echo $r->title ?></td>
									<td><a href="<?php echo site_url("admin/edit_email_template/" . $r->ID) ?>" title="Edit"><i class="fa fa-pencil-square"></i></a></td>
								<tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>
	</div>