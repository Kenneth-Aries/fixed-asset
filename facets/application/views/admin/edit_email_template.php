<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Edit Email Template</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">Edit Email Template</h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered mb-none">
							<tbody>
								<tr>
									<td>[NAME]</td>
									<td>User's name.</td>
								</tr>
								<tr>
									<td>[SITE_URL]</td>
									<td>The URL to your site.</td>
								</tr>
								<tr>
									<td>[SITE_NAME]</td>
									<td>The name of your site.</td>									
								</tr>
								<tr>
									<td>[EMAIL_LINK]</td>
									<td>The email link specified for this particular email.</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="panel-body">				
					<?php echo form_open(site_url("admin/edit_email_template_pro/" . $email_template->ID)) ?>
						<div class="form-group">
							<label class="col-md-3 control-label">Title</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="title" value="<?php echo $email_template->title ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Message</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<textarea name="message" rows="8" class="form-control" id="textareaDefault"><?php echo $email_template->message ?></textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-6">
								<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Update Template</button>
							</div>
						</div>
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>
								