<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>IP Block</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a class="modal-with-form" href="#modalForm"><button class="btn btn-primary">Add IP Block</button></a>
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">IP Block</h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered mb-none">
							<thead>
								<tr>
									<td>IP</td>
									<td>Reason</td>
									<td>Timestamp</td>
									<td>Options</td>
								</tr>
							</thead>
							<tbody>
							<?php foreach($ipblock->result() as $r) : ?>
								<tr>
									<td><?php echo $r->IP ?></td>
									<td><?php echo $r->reason ?></td>
									<td><?php echo date($this->settings->info->date_format, $r->timestamp) ?></td>
									<td><a href="<?php echo site_url("admin/delete_ipblock/" . $r->ID) ?>" title="Delete"><i class="fa fa-trash-o"></i></a></td>
								<tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Modal Form -->
				<div id="modalForm" class="modal-block modal-block-primary mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<h2 class="panel-title">Add IP Block</h2>
						</header>
						<div class="panel-body">
						<?php echo form_open(site_url("admin/add_ipblock")) ?>
								<div class="form-group mt-lg">
									<label class="col-sm-3 control-label">IP Address</label>
									<div class="col-sm-9">
										<input type="text" id="email-in" name="ip" class="form-control" placeholder="Enter IP Address..." required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Reason</label>
									<div class="col-sm-9">
										<input type="text" id="username" name="reason" class="form-control" placeholder="Enter The Reason..." required/>
									</div>
								</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<input type="submit" class="btn btn-primary" value="Submit" />
									<button class="btn btn-default modal-dismiss">Cancel</button>
						<?php echo form_close() ?>
								</div>
							</div>
						</footer>						
					</section>
				</div>
			</section>
		</div>
	</div>