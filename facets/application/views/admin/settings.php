<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Settings</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">Global Settings</h2>
				</header>
				<div class="panel-body">				
					<?php echo form_open_multipart(site_url("admin/settings_pro"))  ?>
						<div class="form-group">
							<label class="col-md-3 control-label">Site Name</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="site_name" placeholder="" value="<?php echo $this->settings->info->site_name ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Site Description</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="site_desc" placeholder="" value="<?php echo $this->settings->info->site_desc ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-info"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Site Email</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="site_email" placeholder="" value="<?php echo $this->settings->info->site_email ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-envelope"></i></span>
									</span>
								</div>
							</div>
						</div>
	
						<div class="form-group">
							<label class="col-md-3 control-label">Site Logo</label>
							<div class="col-md-6">
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input">
											<?php if(!empty($this->settings->info->site_logo)) : ?>
									            <p><img src="<?php echo base_url();?>assets/images/facetsLogo.png" height="45" alt="Facets" /></p>
									        <?php endif; ?>
										</div>
										<span class="btn btn-default btn-file">
											<span class="fileupload-exists">Change</span>
											<span class="fileupload-new">Select file</span>
											<input type="file" name="userfile" size="20" />
										</span>
										<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Upload Path</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="upload_path" placeholder="" value="<?php echo $this->settings->info->upload_path ?>" >
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-upload"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Relative Upload Path</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="upload_path_relative" placeholder="" value="<?php echo $this->settings->info->upload_path_relative ?>" >
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-upload"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Date Formart</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="date_format" placeholder="" value="<?php echo $this->settings->info->date_format ?>" >
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-calendar"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Allowed File Types</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="file_types" placeholder="" value="<?php echo $this->settings->info->file_types ?>" >
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-file"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Upload File Size</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="file_size" placeholder="" value="<?php echo $this->settings->info->file_size ?>" >
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-upload"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" name="register" value="1" <?php if($this->settings->info->register) echo "checked" ?> /> Disable Registration
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="disable_captcha" value="1" <?php if($this->settings->info->disable_captcha) echo "checked" ?> /> Disable Captcha
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" value="1" <?php if($this->settings->info->avatar_upload) echo "checked" ?> /> Allow Avatar Upload
								</label>
							</div>
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" name="login_protect" value="1" <?php if($this->settings->info->login_protect) echo "checked" ?> /> Brutal force login protection
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="activate_account" value="1" <?php if($this->settings->info->activate_account) echo "checked" ?> /> Email Account Activation
								</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-6">
								<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Update Settings</button>
							</div>
						</div>
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>
								