<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Email Members</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">Email Members</h2>
				</header>
				<div class="panel-body">				
					<?php echo form_open(site_url("admin/email_members_pro/")) ?>
						<div class="form-group">
							<label class="col-md-3 control-label">Enter Usernames To Email</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="usernames" value="">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-user"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Select User Group To Email</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<select name="groupid" class="form-control mb-md">
										<option value="0">None</option>
										<option value="-1">All Users</option>
										<?php foreach($groups->result() as $r) : ?>
											<option value="<?php echo $r->ID ?>"><?php echo $r->name ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Title</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="title" value="">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Message</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<textarea name="message" rows="8" class="form-control" id="textareaDefault"></textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-6">
								<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Send Email</button>
							</div>
						</div>
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>