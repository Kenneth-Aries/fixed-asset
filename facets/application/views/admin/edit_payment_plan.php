<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Edit Payment Plan</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">Update Payment Plan</h2>
				</header>
				<div class="panel-body">				
					<?php echo form_open(site_url("admin/edit_payment_plan_pro/" . $plan->ID)) ?>
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Name</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="name" value="<?php echo $plan->name ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Discription</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="description" value="<?php echo $plan->description ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Cost</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="cost" value="<?php echo $plan->cost ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Background Color</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="color" value="<?php echo $plan->hexcolor ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Font Color</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="fontcolor" value="<?php echo $plan->fontcolor ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Days Given</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" name="days" value="<?php echo $plan->days ?>">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-globe"></i></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-6">
								<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Update Plans</button>
							</div>
						</div>
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>