<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>User Roles</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary">Add User Role</button>
					<a href="#" class="fa fa-caret-down"></a>
				</div>

				<h2 class="panel-title">All User Roles</h2>
			</header>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover mb-none">
						<thead>
							<tr>
								<th>#</th>
								<th>Role Name</th>
								<th>Permissions</th>
								<th>Actions</th>
							</tr>
						</thead>
					<?php foreach($roles->result() as $r) : ?>
						<tbody>
							<tr>
								<td><?php echo $r->ID ?></td>
								<td><?php echo $r->name ?></td>
								<td>
									<?php if($r->admin) : ?><span class="highlight">Super Admin</span><?php endif; ?>
									<?php if($r->admin_settings) : ?><span class="highlight">Admin Settings</span><?php endif; ?>
									<?php if($r->admin_members) : ?><span class="highlight">Admin Member</span><?php endif; ?>
									<?php if($r->admin_payment) : ?><span class="highlight">Admin Payment</span><?php endif; ?>
								</td>
								<td class="actions-hover actions-fade">
									<a href="<?php echo site_url("admin/edit_user_role/" . $r->ID) ?>"><i class="fa fa-pencil"></i></a>
									<a href="<?php echo site_url("admin/delete_user_role/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('Are You Sure You Want To Delete This Role')"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						</tbody>
					<?php endforeach; ?>
					</table>
				</div>
			</div>
		</section>
	</div>
</div>