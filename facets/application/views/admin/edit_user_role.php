<header class="page-header">
	<h2>Adminstration</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo site_url("home") ?>">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Administration</span></li>
			<li><span>Edit User Role</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
					</div>
	
					<h2 class="panel-title">User Role <?php echo $role->name ?></h2>
				</header>
				<div class="panel-body">				
					<?php echo form_open(site_url("admin/edit_user_role_pro/" . $role->ID), array("class" => "form-horizontal", "form-bordered")) ?>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" name="admin" value="1" <?php if($role->admin) echo "checked" ?> /> Admin
								</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" name="admin_settings" value="1" <?php if($role->admin_settings) echo "checked" ?> /> Admin Settings
								</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" name="admin_members" value="1" <?php if($role->admin_members) echo "checked" ?> /> Admin Members
								</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" name="admin_payment" value="1" <?php if($role->admin_payment) echo "checked" ?> /> Admin Payment
								</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-md-3 control-label">&nbsp;</label>
							<div class="col-md-6">
								<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Update Role</button>
							</div>
						</div>
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>