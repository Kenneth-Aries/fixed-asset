<header class="page-header">
	<h2>Category</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo base_url();?>">
					<i class="fa fa-home"></i> &nbsp;Home 
				</a>
			</li>
			<li><span>Category</span></li>
			<li><span>Edit Category</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-calendar"></i></a>
	</div>
</header>

<section role="main" class="content-body">
<!-- start: page -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>
	
					<h2 class="panel-title">Edit Category</h2>
				</header>
				<div class="panel-body">
					<?php echo form_open(site_url("category/updatecategorypro/" . $category->ID)) ?>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">Category Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="inputDefault" name="category_name" value="<?php echo $category->category_name ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputDefault">&nbsp;</label>
							<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Update Category</button>
						</div
					<?php echo form_close() ?>
				</div>
			</section>
		</div>
	</div>
</section>