<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class User 
{

	var $info=array();
	var $loggedin=false;
	var $u=null;
	var $p=null;

	public function __construct() 
	{
		$CI =& get_instance();
		$config = $CI->config->item("cookieprefix");
		$this->u = $CI->input->cookie($config . "un", TRUE);
		$this->p = $CI->input->cookie($config . "tkn", TRUE);
 		
 		$user = null; 

 		$select = "user.`ID`, user.`username`, user.`email`, user.entity_id, 
 				user.entity_name, user.points, user.`online_timestamp`, user.avatar,  
				user.email_notification, user.premium_time, user.active, user.activate_code,				
				user.user_role, user_roles.name as ur_name, user_roles.admin,
				user_roles.admin_settings, user_roles.admin_members,
				user_roles.admin_payment, user_roles.ID as user_role_id";
				
		if($user !== null) {
			if ($user->num_rows() == 0) {
				$this->loggedin=false;
			} else {
				$this->loggedin=true;
				$this->info = $user->row();

				if( (empty($this->info->username) || empty($this->info->email)) && ($CI->router->fetch_class() != "register")) {
					redirect(site_url("login"));
				}

				if($this->info->online_timestamp < time() - 60*5) {
					$this->update_online_timestamp($this->info->ID);
				}

				if ($this->info->user_role == -1) {
					$CI->load->helper("cookie");
					$this->loggedin = false;
					$CI->session->set_flashdata("globalmsg", 
						"This account has been deactivated and can no longer be used.");
					delete_cookie($config . "un");
					delete_cookie($config . "tkn");
					redirect(site_url("login/banned"));
				}
			}
		}
	}

	public function getPassword() 
	{
		$CI =& get_instance();
		$user = $CI->db->select("user.`password`")
		->where("ID", $this->info->ID)->get("user");
		$user = $user->row();
		return $user->password;
	}

	public function update_online_timestamp($userid) 
	{
		$CI =& get_instance();
		$CI->db->where("ID", $userid)->update("user", array(
			"online_timestamp" => time()
			)
		);
	}

}

?>
