-- phpMyAdmin SQL Dump
-- version 4.4.13.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 19, 2016 at 04:21 AM
-- Server version: 5.6.26
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fixed_asset`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE IF NOT EXISTS `asset` (
  `ID` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `asset_barcode` varchar(50) DEFAULT NULL,
  `asset_number` varchar(50) DEFAULT NULL,
  `category_name` varchar(50) NOT NULL,
  `asset_description` varchar(250) DEFAULT NULL,
  `make` varchar(50) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `serial_number` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `costcenter_id` int(11) NOT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`ID`, `location_id`, `asset_barcode`, `asset_number`, `category_name`, `asset_description`, `make`, `model`, `serial_number`, `user_id`, `costcenter_id`, `condition_id`, `status_id`, `longitude`, `latitude`) VALUES
(52, 23, '2222', '000011', 'Vehicle', 'Alfa Romeo Key', '', '', '0000011', 1, 0, 0, 0, '27.7108143', '-26.7457484'),
(53, 27, '3333e', '00011', 'Furniture', 'Apple Mouse', '', '', '', 1, 0, 0, 0, '27.7107339', '-26.7456762'),
(54, 23, '6001060566820', 'Nnn', 'Furniture', 'Orro', '33', 'Iii', 'Ooo', 1, 3, 1, 3, '', ''),
(55, 23, '011', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 2, 3, 2, '28.030863', '-26.0219398'),
(65, 23, '990', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308562', '-26.0219405'),
(66, 23, '990', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(67, 23, '990', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(68, 23, '990', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(69, 23, '990', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(70, 27, '2212', '00011', 'Computer Equipment', 'sunglasses', '', '', '', 1, 3, 3, 3, '28.0308545', '-26.0219286'),
(71, 27, '000011', '0001', 'Vehicle', 'USB cable', '', '', '', 1, 2, 2, 1, '28.0009689', '-26.1866324'),
(72, 29, '111222', 'Ray Ban', 'Computer Equipment', 'sunglasses', 'Ray Ban', '', '', 1, 2, 1, 3, '28.030873', '-26.0219416'),
(73, 27, '2016', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(74, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(75, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(76, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(77, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(78, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(79, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(80, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(81, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(82, 27, '88787', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(83, 24, 'yyuuhh', '00000', 'Computer Equipment', 'desc', '', '', '', 1, 3, 3, 2, '28.00118', '-26.1866999'),
(84, 27, '0000099', '009900', 'Computer Equipment', 'desc', '', '', '', 1, 1, 1, 2, '28.0008002', '-26.1863514'),
(85, 28, 'tools_111', '0000', 'Furniture', 'desc', 'mac', 'pro', '000', 1, 4, 1, 1, '28.0008443', '-26.1865456'),
(86, 30, '22202', '3', 'Computer Equipment', 'desc rasberry pi', '', '', '', 1, 4, 1, 3, '28.0009793', '-26.1861445'),
(87, 28, '5449000123268', 'Valpre123', 'Computer Equipment', 'Bottle of water', '', '', '', 1, 1, 1, 3, '28.0685071', '-26.133848'),
(88, 23, '4444', '4444', 'Furniture', '4444', '', '', 'Serial 4444', 1, 1, 1, 3, '28.0685971', '-26.1339609'),
(89, 23, 'Aaaa', 'Nnnnnn', 'Computer Equipment', 'Nnn', '', '', '', 1, 4, 1, 3, '18.5908825', '-33.9670111'),
(90, 28, 'Aaaa1', '10001', 'Computer Equipment', 'pc', '', '', '', 1, 2, 2, 3, '28.1079784', '-25.6714779');

--
-- Triggers `asset`
--
DELIMITER $$
CREATE TRIGGER `after_insert_asset` AFTER INSERT ON `asset`
 FOR EACH ROW BEGIN
    INSERT INTO `Fixed_Asset`.`asset_tracker`
		(`idasset`,
		`date_time`,
		`asset_number`,
		`category_name`,
		`asset_description`,
		`make`,
		`model`,
		`serial_number`,
		`iduser`,
		`idcost_centre`,
		`idcondition`,
		`idstatus`,
        `longitude`,
        `latitude`,
        `idlocation`)
		VALUES
		(NEW.idasset,
		NOW(),
		NEW.asset_number,
		NEW.category_name,
		NEW.asset_description,
		NEW.make,
		NEW.model,
		NEW.serial_number,
		NEW.iduser,

		NEW.idcost_centre,
		NEW.idcondition,
		NEW.idstatus,
        NEW.longitude,
        NEW.latitude,
        NEW.idlocation);
  END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_asset` AFTER UPDATE ON `asset`
 FOR EACH ROW BEGIN
    INSERT INTO `Fixed_Asset`.`asset_tracker`
		(`idasset`,
		`date_time`,
		`asset_number`,
		`category_name`,
		`asset_description`,
		`make`,
		`model`,
		`serial_number`,
		`iduser`,
		`idcost_centre`,
		`idcondition`,
		`idstatus`,
        `longitude`,
        `latitude`,
        `idlocation`)
		VALUES
		(NEW.idasset,
		NOW(),
		NEW.asset_number,
		NEW.category_name,
		NEW.asset_description,
		NEW.make,
		NEW.model,
		NEW.serial_number,
		NEW.iduser,
		NEW.idcost_centre,
		NEW.idcondition,
		NEW.idstatus,
        NEW.longitude,
        NEW.latitude,
        NEW.idlocation);
  END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `asset_image`
--

CREATE TABLE IF NOT EXISTS `asset_image` (
  `ID` int(11) NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `asset_url` varchar(45) DEFAULT NULL,
  `image_priority` int(11) DEFAULT NULL,
  `image_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_image`
--

INSERT INTO `asset_image` (`ID`, `asset_id`, `asset_url`, `image_priority`, `image_date`) VALUES
(39, 52, '/asset_images/Facets_2016_56_03_8_56_28.jpg', 1, '2016-08-03'),
(40, 52, '/asset_images/Facets_2016_00_03_9_00_05.jpg', 1, '2016-08-03'),
(41, 53, '/asset_images/Facets_2016_03_03_9_03_49.jpg', 1, '2016-08-03'),
(42, 54, '/asset_images/Facets_2016_42_11_12_42_20.jpg', 1, '2016-08-11'),
(43, 55, '/asset_images/Facets_2016_46_21_11_46_40.jpg', 1, '2016-08-21'),
(44, 56, '/asset_images/Facets_2016_46_21_11_46_40.jpg', 1, '2016-08-21'),
(45, 57, '/asset_images/Facets_2016_46_21_11_46_40.jpg', 1, '2016-08-21'),
(46, 58, '/asset_images/Facets_2016_46_21_11_46_40.jpg', 1, '2016-08-21'),
(47, 65, '/asset_images/Facets_2016_12_22_8_12_16.jpg', 1, '2016-08-22'),
(48, 71, '/asset_images/Facets_2016_00_03_9_00_05.jpg', 1, '2016-08-22'),
(49, 71, '/asset_images/Facets_2016_03_03_9_03_49.jpg', 0, '2016-08-22'),
(50, 71, '/asset_images/Facets_2016_44_20_5_44_29.jpg', 1, '2016-08-22'),
(51, 71, '/asset_images/Facets_2016_00_03_9_00_05.jpg', 1, '2016-08-22'),
(52, 71, '/asset_images/Facets_2016_57_22_10_57_40.jpg', 0, '2016-08-22'),
(53, 71, '/asset_images/Facets_2016_57_22_10_57_40.jpg', 1, '2016-08-22'),
(54, 71, '/asset_images/Facets_2016_35_22_11_35_06.jpg', 1, '2016-08-22'),
(55, 71, '/asset_images/Facets_2016_31_03_5_31_39.jpg', 1, '2016-08-23'),
(56, 71, '/asset_images/Facets_2016_35_22_11_35_06.jpg', 1, '2016-08-23'),
(57, 1, '/asset_images/Facets_2016_34_23_12_34_04.jpg', 1, '2016-08-23'),
(58, 73, '/asset_images/Facets_2016_38_23_12_38_37.jpg', 1, '2016-08-23'),
(59, 0, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(60, 0, '/asset_images/Facets_2016_38_23_12_38_37.jpg', 0, '2016-08-23'),
(61, 83, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(62, 84, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(63, 1, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(64, 1, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(65, 1, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(66, 1, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(67, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(68, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(69, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(70, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(71, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(72, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(73, 73, '/asset_images/Facets_2016_55_23_6_55_07.jpg', 1, '2016-08-23'),
(74, 85, '/asset_images/Facets_2016_38_23_12_38_37.jpg', 1, '2016-08-23'),
(75, 84, '/asset_images/Facets_2016_41_23_7_41_37.jpg', 1, '2016-08-23'),
(76, 84, '/asset_images/Facets_2016_35_22_11_35_06.jpg', 1, '2016-08-23'),
(77, 84, '/asset_images/Facets_2016_41_23_7_41_37.jpg', 1, '2016-08-23'),
(78, 86, '/asset_images/Facets_2016_49_23_7_49_20.jpg', 1, '2016-08-23'),
(79, 85, '/asset_images/Facets_2016_52_23_7_52_02.jpg', 1, '2016-08-23'),
(80, 85, '/asset_images/Facets_2016_12_22_8_12_16.jpg', 1, '2016-08-23'),
(81, 87, '/asset_images/Facets_2016_02_23_7_02_24.jpg', 1, '2016-08-23'),
(82, 88, '/asset_images/Facets_2016_06_23_7_06_18.jpg', 1, '2016-08-23'),
(83, 88, '/asset_images/IMG-20160823-WA0002.jpg', 1, '2016-08-23'),
(84, 88, '/asset_images/IMG-20160823-WA0004.jpg', 0, '2016-08-23'),
(85, 88, '/asset_images/IMG-20160823-WA0003.jpg', 0, '2016-08-23'),
(86, 88, '/asset_images/Facets_2016_07_23_7_07_00.jpg', 0, '2016-08-23'),
(87, 88, '/asset_images/20160823_091401.jpg', 1, '2016-08-23'),
(88, 89, '/asset_images/Facets_2016_11_29_3_11_13.jpg', 1, '2016-08-29'),
(89, 90, '/asset_images/Facets_2016_01_19_8_01_34.jpg', 1, '2016-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `asset_tracker`
--

CREATE TABLE IF NOT EXISTS `asset_tracker` (
  `ID` int(11) NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `date_time` varchar(45) DEFAULT NULL,
  `asset_number` varchar(45) DEFAULT NULL,
  `category_name` varchar(45) DEFAULT NULL,
  `asset_description` varchar(45) DEFAULT NULL,
  `make` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `costcenter_id` int(11) DEFAULT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_tracker`
--

INSERT INTO `asset_tracker` (`ID`, `asset_id`, `location_id`, `date_time`, `asset_number`, `category_name`, `asset_description`, `make`, `model`, `serial_number`, `user_id`, `costcenter_id`, `condition_id`, `status_id`, `longitude`, `latitude`) VALUES
(6, 52, 27, '2016-08-03 20:55:40', '000011', 'Furniture', 'Alfa Key', '', '', '0000011', 1, 1, 1, 2, '28.031118', '-26.0221'),
(7, 52, 27, '2016-08-03 20:58:53', '000011', 'Vehicle', 'Alfa Romel Key', '', '', '0000011', 1, 1, 1, 2, '28.0310925', '-26.0221199'),
(8, 52, 27, '2016-08-03 20:59:18', '000011', 'Vehicle', 'Alfa Romel Key', '', '', '0000011', 1, 1, 1, 2, '28.0310925', '-26.0221199'),
(9, 52, 27, '2016-08-03 21:01:19', '000011', 'Vehicle', 'Alfa Romeo Key', '', '', '0000011', 1, 1, 1, 2, '28.0311118', '-26.0220804'),
(10, 53, 23, '2016-08-03 21:02:57', '00011', 'Furniture', 'Apple Mouse', '', '', '', 1, 1, 1, 2, '28.0310826', '-26.0220644'),
(11, 54, 23, '2016-08-11 12:43:18', 'Nnn', 'Furniture', 'Orro', '33', 'Iii', 'Ooo', 1, 3, 1, 3, '', ''),
(12, 55, 23, '2016-08-21 11:48:55', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 3, 3, 3, '28.0308469', '-26.0218664'),
(13, 56, 23, '2016-08-21 11:49:51', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 3, 3, 3, '28.0308469', '-26.0218664'),
(14, 57, 23, '2016-08-21 11:50:22', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 3, 3, 3, '28.0308469', '-26.0218664'),
(15, 58, 23, '2016-08-21 11:50:28', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 3, 3, 3, '28.0308469', '-26.0218664'),
(16, 53, 0, '2016-08-22 14:55:50', '00011', 'Furniture', 'Apple Mouse', '', '', '', 1, 0, 0, 0, '27.7107339', '-26.7456762'),
(17, 53, 27, '2016-08-22 15:00:50', '00011', 'Furniture', 'Apple Mouse', '', '', '', 1, 0, 0, 0, '27.7107339', '-26.7456762'),
(18, 52, 0, '2016-08-22 15:02:21', '000011', 'Vehicle', 'Alfa Romeo Key', '', '', '0000011', 1, 0, 0, 0, '27.7108143', '-26.7457484'),
(19, 52, 0, '2016-08-22 15:10:01', '000011', 'Vehicle', 'Alfa Romeo Key', '', '', '0000011', 1, 0, 0, 0, '27.7108143', '-26.7457484'),
(20, 52, 23, '2016-08-22 15:21:49', '000011', 'Vehicle', 'Alfa Romeo Key', '', '', '0000011', 1, 0, 0, 0, '27.7108143', '-26.7457484'),
(21, 56, 23, '2016-08-22 18:52:11', '00001', 'Vehicle', 'iphone', '', '', '', 1, 4, 1, 1, '28.0308698', '28.0308698'),
(22, 55, 23, '2016-08-22 18:55:46', '00001', 'Vehicle', 'iphone', '', '', '', 1, 2, 2, 1, '28.0308618', '-26.0219391'),
(23, 55, 23, '2016-08-22 19:05:55', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308332', '-26.0219136'),
(24, 55, 23, '2016-08-22 19:13:53', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308717', '-26.0219391'),
(25, 55, 23, '2016-08-22 19:13:56', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308717', '-26.0219391'),
(26, 55, 23, '2016-08-22 19:14:43', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308717', '-26.0219391'),
(27, 55, 23, '2016-08-22 19:18:14', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308743', '-26.0219352'),
(28, 55, 23, '2016-08-22 19:20:16', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308743', '-26.0219352'),
(29, 55, 23, '2016-08-22 19:20:23', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 1, 3, 2, '28.0308743', '-26.0219352'),
(30, 55, 23, '2016-08-22 19:22:34', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 2, 3, 2, '28.0308351', '-26.0219093'),
(31, 59, 24, '2016-08-22 19:31:27', '000011', 'Vehicle', 'sunglasses', '', '', '', 1, 4, 1, 2, '28.0308475', '-26.0219302'),
(32, 60, 24, '2016-08-22 19:31:33', '000011', 'Vehicle', 'sunglasses', '', '', '', 1, 4, 1, 2, '28.0308475', '-26.0219302'),
(33, 61, 24, '2016-08-22 19:31:35', '000011', 'Vehicle', 'sunglasses', '', '', '', 1, 4, 1, 2, '28.0308475', '-26.0219302'),
(34, 62, 24, '2016-08-22 19:31:38', '000011', 'Vehicle', 'sunglasses', '', '', '', 1, 4, 1, 2, '28.0308475', '-26.0219302'),
(35, 63, 24, '2016-08-22 19:31:43', '000011', 'Vehicle', 'sunglasses', '', '', '', 1, 4, 1, 2, '28.0308475', '-26.0219302'),
(36, 64, 24, '2016-08-22 19:34:37', '000011', 'Vehicle', 'sunglasses', '', '', '', 1, 4, 1, 2, '28.0308475', '-26.0219302'),
(37, 65, 23, '2016-08-22 19:37:49', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(38, 66, 23, '2016-08-22 19:40:06', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(39, 67, 23, '2016-08-22 19:40:32', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(40, 68, 23, '2016-08-22 19:40:33', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(41, 69, 23, '2016-08-22 19:41:07', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308594', '-26.0219365'),
(42, 65, 23, '2016-08-22 20:12:28', '011', 'Computer Equipment', 'glasses', '', '', '', 1, 2, 1, 3, '28.0308562', '-26.0219405'),
(43, 55, 23, '2016-08-22 20:25:59', '00001', 'Computer Equipment', 'iphone', '', '', '', 1, 2, 3, 2, '28.030863', '-26.0219398'),
(44, 70, 27, '2016-08-22 20:26:56', '00011', 'Computer Equipment', 'sunglasses', '', '', '', 1, 3, 3, 3, '28.0308545', '-26.0219286'),
(45, 71, 23, '2016-08-22 20:30:39', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 1, 1, 2, '28.0308598', '-26.0219375'),
(46, 71, 23, '2016-08-22 21:35:51', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 1, 1, 2, '28.0308881', '-26.0219469'),
(47, 71, 23, '2016-08-22 21:38:00', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 1, 1, 2, '28.0308711', '-26.0219385'),
(48, 71, 23, '2016-08-22 22:58:20', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308598', '-26.0219317'),
(49, 71, 23, '2016-08-22 23:17:34', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308661', '-26.0219311'),
(50, 71, 23, '2016-08-22 23:19:40', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308601', '-26.0219248'),
(51, 71, 23, '2016-08-22 23:24:03', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308923', '-26.0219515'),
(52, 71, 23, '2016-08-22 23:25:09', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308583', '-26.0219197'),
(53, 71, 23, '2016-08-22 23:30:20', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308589', '-26.0219172'),
(54, 71, 23, '2016-08-22 23:33:00', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308596', '-26.0219174'),
(55, 71, 23, '2016-08-22 23:34:12', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308578', '-26.0219241'),
(56, 71, 23, '2016-08-22 23:34:37', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308706', '-26.0219317'),
(57, 71, 23, '2016-08-22 23:35:26', '0001', 'Computer Equipment', 'USB cable', '', '', '', 1, 2, 3, 3, '28.0308716', '-26.0219281'),
(58, 71, 27, '2016-08-23 00:21:29', '0001', 'Vehicle', 'USB cable', '', '', '', 1, 2, 2, 1, '28.0308622', '-26.0219195'),
(59, 71, 27, '2016-08-23 00:30:38', '0001', 'Vehicle', 'USB cable', '', '', '', 1, 2, 2, 1, '28.0308616', '-26.0219158'),
(60, 72, 29, '2016-08-23 00:34:21', 'Ray Ban', 'Computer Equipment', 'sunglasses', 'Ray Ban', '', '', 1, 2, 1, 3, '28.030873', '-26.0219416'),
(61, 73, 29, '2016-08-23 00:38:57', '15', 'Vehicle', 'spanner', '', '', '', 1, 1, 1, 3, '28.0308809', '-26.0219567'),
(62, 73, 24, '2016-08-23 06:55:28', '15', 'Computer Equipment', 'spanner', '', '', '', 1, 4, 3, 2, '28.0011196', '-26.1865395'),
(63, 74, 27, '2016-08-23 06:58:58', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(64, 75, 27, '2016-08-23 07:00:39', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(65, 76, 27, '2016-08-23 07:01:52', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(66, 77, 27, '2016-08-23 07:02:24', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(67, 78, 27, '2016-08-23 07:02:31', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(68, 79, 27, '2016-08-23 07:02:31', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(69, 80, 27, '2016-08-23 07:05:28', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(70, 81, 27, '2016-08-23 07:05:36', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(71, 82, 27, '2016-08-23 07:05:38', '5556t', 'Computer Equipment', 'desc', '', '', '', 1, 2, 1, 2, '28.0009021', '-26.1866052'),
(72, 83, 24, '2016-08-23 07:08:55', '00000', 'Computer Equipment', 'desc', '', '', '', 1, 3, 3, 2, '28.00118', '-26.1866999'),
(73, 84, 27, '2016-08-23 07:12:01', '009900', 'Computer Equipment', 'desc', '', '', '', 1, 1, 1, 2, '28.000898', '-26.1865338'),
(74, 73, 27, '2016-08-23 07:12:33', '15', 'Computer Equipment', 'spanner', '', '', '', 1, 4, 3, 2, '27.9997611', '-26.1857172'),
(75, 73, 27, '2016-08-23 07:12:53', '15', 'Computer Equipment', 'spanner', '', '', '', 1, 4, 3, 2, '27.9997611', '-26.1857172'),
(76, 73, 27, '2016-08-23 07:13:04', '15', 'Computer Equipment', 'spanner', '', '', '', 1, 4, 3, 2, '27.9997611', '-26.1857172'),
(77, 73, 27, '2016-08-23 07:14:31', '15', 'Vehicle', 'spanner', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(78, 73, 27, '2016-08-23 07:15:55', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(79, 73, 27, '2016-08-23 07:19:23', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(80, 73, 27, '2016-08-23 07:20:55', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(81, 73, 27, '2016-08-23 07:22:21', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(82, 73, 27, '2016-08-23 07:23:16', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(83, 73, 27, '2016-08-23 07:23:30', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(84, 73, 27, '2016-08-23 07:26:07', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(85, 73, 27, '2016-08-23 07:26:56', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(86, 73, 27, '2016-08-23 07:27:23', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(87, 73, 27, '2016-08-23 07:27:37', '156', 'Vehicle', 'spanner 1', '', '', '', 1, 2, 1, 1, '27.9997611', '-26.1857172'),
(88, 85, 28, '2016-08-23 07:29:04', '0000', 'Furniture', 'desc', '', '', '', 1, 4, 1, 1, '28.0009941', '-26.1866422'),
(89, 71, 27, '2016-08-23 07:41:24', '0001', 'Vehicle', 'USB cable', '', '', '', 1, 2, 2, 1, '28.0009689', '-26.1866324'),
(90, 84, 27, '2016-08-23 07:42:02', '009900', 'Computer Equipment', 'desc', '', '', '', 1, 1, 1, 2, '28.0008002', '-26.1863514'),
(91, 84, 27, '2016-08-23 07:42:24', '009900', 'Computer Equipment', 'desc', '', '', '', 1, 1, 1, 2, '28.000882', '-26.1865826'),
(92, 84, 27, '2016-08-23 07:42:29', '009900', 'Computer Equipment', 'desc', '', '', '', 1, 1, 1, 2, '28.0008002', '-26.1863514'),
(93, 86, 30, '2016-08-23 07:49:31', '3', 'Computer Equipment', 'desc rasberry pi', '', '', '', 1, 4, 1, 3, '28.0009793', '-26.1861445'),
(94, 85, 28, '2016-08-23 07:52:10', '0000', 'Furniture', 'desc', '', '', '', 1, 4, 1, 1, '28.0008053', '-26.1865889'),
(95, 85, 28, '2016-08-23 07:53:34', '0000', 'Furniture', 'desc', 'mac', 'pro', '000', 1, 4, 1, 1, '28.0008443', '-26.1865456'),
(96, 87, 27, '2016-08-23 19:02:35', 'Valpre123', 'Computer Equipment', 'Bottle of water', '', '', '', 1, 1, 1, 3, '28.0687396', '-26.1343673'),
(97, 87, 28, '2016-08-23 19:03:18', 'Valpre123', 'Computer Equipment', 'Bottle of water', '', '', '', 1, 1, 1, 3, '28.0685071', '-26.133848'),
(98, 88, 23, '2016-08-23 19:06:30', '4444', 'Furniture', '4444', '', '', 'Serial 4444', 1, 1, 1, 3, '28.068667', '-26.1341159'),
(99, 88, 23, '2016-08-23 19:07:11', '4444', 'Furniture', '4444', '', '', 'Serial 4444', 1, 1, 1, 3, '28.0686547', '-26.1339863'),
(100, 88, 23, '2016-08-23 19:07:38', '4444', 'Furniture', '4444', '', '', 'Serial 4444', 1, 1, 1, 3, '28.0685971', '-26.1339609'),
(101, 89, 23, '2016-08-29 15:10:23', 'Nnnnnn', 'Computer Equipment', 'Nnn', '', '', '', 1, 4, 1, 3, '18.5908825', '-33.9670111'),
(102, 90, 28, '2016-09-19 20:01:53', '10001', 'Computer Equipment', 'pc', '', '', '', 1, 2, 2, 3, '28.1079784', '-25.6714779');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_name` varchar(50) NOT NULL DEFAULT 'default',
  `ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_name`, `ID`) VALUES
('Furniture', 1),
('Vehicle', 2),
('Computer Equipment', 3),
('Trucks', 4),
('Stationery', 5);

-- --------------------------------------------------------

--
-- Table structure for table `condition`
--

CREATE TABLE IF NOT EXISTS `condition` (
  `ID` int(11) NOT NULL,
  `condition_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `condition`
--

INSERT INTO `condition` (`ID`, `condition_name`) VALUES
(1, 'New'),
(2, 'used'),
(3, 'Damaged'),
(4, 'boxed'),
(5, 'good'),
(6, 'quality'),
(7, 'perfect');

-- --------------------------------------------------------

--
-- Table structure for table `cost_center`
--

CREATE TABLE IF NOT EXISTS `cost_center` (
  `cost_center` varchar(50) NOT NULL DEFAULT 'default',
  `ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cost_center`
--

INSERT INTO `cost_center` (`cost_center`, `ID`) VALUES
('default', 1),
('Stock available', 2),
('100 - Head office', 3),
('200 - Manufacturing', 4);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `ID` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`ID`, `title`, `message`) VALUES
(1, 'Forgot Your Password', 'Dear [NAME],\r\n<br /><br />\r\nSomeone (hopefully you) requested a password reset at [SITE_URL].\r\n<br /><br />\r\nTo reset your password, please follow the following link: [EMAIL_LINK]\r\n<br /><br />\r\nIf you did not reset your password, please kindly ignore  this email.\r\n<br /><br />\r\nYours, <br />\r\n[SITE_NAME]'),
(2, 'Email Activation', 'Dear [NAME],\r\n<br /><br />\r\nSomeone (hopefully you) has registered an account on [SITE_NAME] using this email address.\r\n<br /><br />\r\nPlease activate the account by following this link: [EMAIL_LINK]\r\n<br /><br />\r\nIf you did not register an account, please kindly ignore  this email.\r\n<br /><br />\r\nYours, <br />\r\n[SITE_NAME]');

-- --------------------------------------------------------

--
-- Table structure for table `entity`
--

CREATE TABLE IF NOT EXISTS `entity` (
  `ID` int(11) NOT NULL,
  `entity_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entity`
--

INSERT INTO `entity` (`ID`, `entity_name`) VALUES
(1, 'Cradle offices'),
(2, 'company 1'),
(3, 'company 2'),
(4, 'company4'),
(5, 'themost'),
(6, 'focus'),
(7, 'nexus'),
(8, 'Prudent'),
(9, 'Realm'),
(10, 'Collection'),
(11, 'Intrigue');

-- --------------------------------------------------------

--
-- Table structure for table `ip_block`
--

CREATE TABLE IF NOT EXISTS `ip_block` (
  `ID` int(11) NOT NULL,
  `IP` varchar(500) NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `reason` varchar(1000) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `ID` int(11) NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `building` varchar(50) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`ID`, `barcode`, `location_name`, `building`, `region`, `country`, `longitude`, `latitude`, `user_id`, `entity_id`) VALUES
(23, '11124', 'room4', 'block 4', 'Gauteng', 'RSA', '28.0311109', '-26.022074', 1, 1),
(24, '11126', 'room44', 'block 4', 'Gauteng', 'RSA', '28.0311109', '-26.022074', 1, 1),
(27, '111', 'office 22', 'A Block', 'Gauteng', 'RSA', '28.0310877', '-26.0221002', 2, 1),
(28, '60011223', 'Workshop', 'Plant 1 -25 Rifle Road', 'Gauteng', 'RSA', '', '', 3, 1),
(29, '1112', 'store room', 'workshop 12', 'GAUTENG', 'RSA', '28.0308624', '-26.0219175', 1, 1),
(30, '1118', 'durban branch boardroom', 'durban office', 'natal', 'RSA', '28.0010114', '-26.1866482', 3, 1),
(31, '22202', 'office 16', 'Block A', 'kzn', 'RSA', '28.0009022', '-26.1865447', 1, 1),
(33, 'Aaaa12', 'room2', 'A', 'Quateng', 'rsa', '28.1080157', '-25.6714604', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `ID` int(11) NOT NULL,
  `IP` varchar(500) NOT NULL DEFAULT '',
  `username` varchar(500) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`ID`, `IP`, `username`, `count`, `timestamp`) VALUES
(1, '::1', 'admin@demo.com', 0, 1477128626),
(2, '::1', 'admin', 1, 1477142129);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE IF NOT EXISTS `password_reset` (
  `ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `IP` varchar(500) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_plans`
--

CREATE TABLE IF NOT EXISTS `payment_plans` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `hexcolor` varchar(6) NOT NULL DEFAULT '',
  `fontcolor` varchar(6) NOT NULL DEFAULT '',
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `days` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_plans`
--

INSERT INTO `payment_plans` (`ID`, `name`, `hexcolor`, `fontcolor`, `cost`, `days`, `sales`, `description`) VALUES
(2, 'BASIC', '68aa9b', 'FFFFFF', '5.00', 30, 6, 'This is the basic plan which gives you a introduction to our Premium Plans'),
(3, 'Professional', '416375', 'FFFFFF', '7.99', 90, 3, 'Get all the benefits of basic at a cheaper price and gain content for longer.'),
(4, 'LIFETIME', '578465', 'FFFFFF', '300.00', 0, 1, 'Become a premium member for life and have access to all our premium content.');

-- --------------------------------------------------------

--
-- Table structure for table `reset_log`
--

CREATE TABLE IF NOT EXISTS `reset_log` (
  `ID` int(11) NOT NULL,
  `IP` varchar(500) NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `ID` int(11) NOT NULL,
  `site_name` varchar(500) NOT NULL,
  `site_desc` varchar(500) NOT NULL,
  `upload_path` varchar(500) NOT NULL,
  `upload_path_relative` varchar(500) NOT NULL,
  `site_email` varchar(500) NOT NULL,
  `site_logo` varchar(1000) NOT NULL DEFAULT 'logo.png',
  `register` int(11) NOT NULL,
  `disable_captcha` int(11) NOT NULL,
  `date_format` varchar(25) NOT NULL,
  `avatar_upload` int(11) NOT NULL DEFAULT '1',
  `file_types` varchar(500) NOT NULL,
  `twitter_consumer_key` varchar(255) NOT NULL,
  `twitter_consumer_secret` varchar(255) NOT NULL,
  `disable_social_login` int(11) NOT NULL,
  `facebook_app_id` varchar(255) NOT NULL,
  `facebook_app_secret` varchar(255) NOT NULL,
  `google_client_id` varchar(255) NOT NULL,
  `google_client_secret` varchar(255) NOT NULL,
  `file_size` int(11) NOT NULL,
  `paypal_email` varchar(1000) NOT NULL,
  `paypal_currency` varchar(100) NOT NULL DEFAULT 'USD',
  `payment_enabled` int(11) NOT NULL,
  `payment_symbol` varchar(5) NOT NULL DEFAULT '$',
  `global_premium` int(11) NOT NULL,
  `install` int(11) NOT NULL DEFAULT '1',
  `login_protect` int(11) NOT NULL,
  `activate_account` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`ID`, `site_name`, `site_desc`, `upload_path`, `upload_path_relative`, `site_email`, `site_logo`, `register`, `disable_captcha`, `date_format`, `avatar_upload`, `file_types`, `twitter_consumer_key`, `twitter_consumer_secret`, `disable_social_login`, `facebook_app_id`, `facebook_app_secret`, `google_client_id`, `google_client_secret`, `file_size`, `paypal_email`, `paypal_currency`, `payment_enabled`, `payment_symbol`, `global_premium`, `install`, `login_protect`, `activate_account`) VALUES
(1, 'Facets', 'Physical Asset Management', 'C:\\Bitnami\\wampstack-5.5.30-0\\apache2\\htdocs\\master\\uploads', 'uploads', 'test@test.com', 'logo.png', 0, 0, 'd/m/Y', 0, 'gif|png|jpg|jpeg', '', '', 0, '', '', '', '', 1028, '', 'USD', 1, '$', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `ID` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`ID`, `status_name`) VALUES
(3, 'In use'),
(2, 'Not in use'),
(1, 'Retired'),
(4, 'Service');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `assettrack_id` int(11) NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `last_scanned` datetime DEFAULT NULL,
  `last_moved` datetime DEFAULT NULL,
  `written_off` datetime DEFAULT NULL,
  `retired` datetime DEFAULT NULL,
  `status_name` varchar(50) DEFAULT NULL,
  `condition_name` varchar(50) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `usertitle_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `entity_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `ip` int(11) NOT NULL,
  `online_timestamp` int(11) NOT NULL DEFAULT '0',
  `email_notification` int(11) NOT NULL DEFAULT '1',
  `user_role` int(11) NOT NULL DEFAULT '0',
  `premium_planid` int(11) NOT NULL DEFAULT '0',
  `premium_time` int(11) NOT NULL DEFAULT '0',
  `points` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` int(11) NOT NULL DEFAULT '1',
  `active_code` varchar(255) DEFAULT NULL,
  `joined` int(11) NOT NULL DEFAULT '0',
  `joined_date` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `entity_id`, `usertitle_id`, `username`, `password`, `email`, `contact_number`, `entity_name`, `token`, `ip`, `online_timestamp`, `email_notification`, `user_role`, `premium_planid`, `premium_time`, `points`, `active`, `active_code`, `joined`, `joined_date`) VALUES
(1, 1, 1, 'Brett', 'passpass', NULL, NULL, '', '8ec23002c3000da4921ce64a2e7e4037', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(2, 1, 4, 'Brett Stuart', 'passpass', 'brett@forwardmobile.co.za', '0833900272', '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(3, 1, 2, 'Craig', 'passpass1234', 'craig@cradle.co.za', '083083083', '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(4, 2, 2, 'User 1', 'passpass', 'Brett@brettstuart.com', '0833902926', '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(5, 3, 3, 'Brett 2', 'passpass', 'B@b.com', '0987668899', '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(6, 3, 3, 'User 2', 'pass', 'Bdheh', '67899:', '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(7, NULL, NULL, 'francois', 'Pass4321Pass', 'francois.weideman@gmail.com', NULL, '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, ''),
(8, 2, 2, 'francois1', 'Pass4321Pass', 'francois.weideman@gmail.com', '08200000', '', '', 0, 0, 0, 0, 0, 0, '0.00', 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_events`
--

CREATE TABLE IF NOT EXISTS `user_events` (
  `ID` int(11) NOT NULL,
  `IP` varchar(255) NOT NULL DEFAULT '',
  `event` varchar(255) NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `ID` int(11) NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `default` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`ID`, `name`, `default`) VALUES
(1, 'Default Group', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `admin` int(11) NOT NULL DEFAULT '0',
  `admin_settings` int(11) NOT NULL DEFAULT '0',
  `admin_members` int(11) NOT NULL DEFAULT '0',
  `admin_payment` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`ID`, `name`, `admin`, `admin_settings`, `admin_members`, `admin_payment`) VALUES
(1, 'Admin', 1, 0, 0, 0),
(2, 'Member Manager', 0, 0, 1, 0),
(3, 'Admin Settings', 0, 1, 0, 0),
(4, 'Admin Payments', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_title`
--

CREATE TABLE IF NOT EXISTS `user_title` (
  `ID` int(11) NOT NULL,
  `title_name` varchar(50) NOT NULL DEFAULT 'default'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_title`
--

INSERT INTO `user_title` (`ID`, `title_name`) VALUES
(2, 'admin'),
(3, 'Basic user'),
(4, 'dept. head'),
(5, 'Super User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `asset_image`
--
ALTER TABLE `asset_image`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `asset_tracker`
--
ALTER TABLE `asset_tracker`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`,`category_name`);

--
-- Indexes for table `condition`
--
ALTER TABLE `condition`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cost_center`
--
ALTER TABLE `cost_center`
  ADD PRIMARY KEY (`ID`,`cost_center`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `entity`
--
ALTER TABLE `entity`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ip_block`
--
ALTER TABLE `ip_block`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `barcode_UNIQUE` (`barcode`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `payment_plans`
--
ALTER TABLE `payment_plans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reset_log`
--
ALTER TABLE `reset_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `status_name_i` (`status_name`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`assettrack_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_events`
--
ALTER TABLE `user_events`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_title`
--
ALTER TABLE `user_title`
  ADD PRIMARY KEY (`ID`,`title_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset`
--
ALTER TABLE `asset`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `asset_image`
--
ALTER TABLE `asset_image`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `asset_tracker`
--
ALTER TABLE `asset_tracker`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `condition`
--
ALTER TABLE `condition`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cost_center`
--
ALTER TABLE `cost_center`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `entity`
--
ALTER TABLE `entity`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ip_block`
--
ALTER TABLE `ip_block`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_plans`
--
ALTER TABLE `payment_plans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `reset_log`
--
ALTER TABLE `reset_log`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `assettrack_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_events`
--
ALTER TABLE `user_events`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_title`
--
ALTER TABLE `user_title`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
