<?php
if (!isset($title) || empty($title)) {$title = '';} else {$title = ' - '.$title;}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $GLOBALS['app.title'].$title; ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/metisMenu.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/sb-admin-2.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/jquery.growl.css"/>
        <?php echo $cssTop . $styleTop . $jsTop . $scriptTop;?>
    </head>

    <body style="background-color:<?php echo (!$GLOBALS['app.user.loggedin'] ? '#15147b;' : '#f5f5f5') ?>;">
        <div class="wrapper">
            <?php
            if (funcCore::requireClasses($GLOBALS['app.var.user.classname']) && $GLOBALS['app.user.loggedin']) {
              echo funcUI::getPage('form.php', 'login');
            }
            ?>
            <?php echo $content; ?>      
        </div>
    <div id="dialog" style="display:none" title="<?php echo htmlspecialchars($GLOBALS['app.title']); ?>"></div>
    <div id="indicator" style="display:none">
        <img src="<?php echo $GLOBALS['app.ui.theme.folder']; ?>images/indicator.gif" />
    </div>
        <?php echo $cssBottom . $styleBottom . $jsBottom . $scriptBottom; ?>
        <script src="js/js/jquery.min.js"></script>
        <script src="js/js/bootstrap.min.js"></script>
        <script src="js/js/metisMenu.min.js"></script>
        <script src="js/js/sb-admin-2.js"></script>
        <script src="js/jquery.growl.js"></script>
    </body>
</html>