<?php
if (!isset($title) || empty($title)) {$title = '';} else {$title = ' - '.$title;}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <title><?php echo $GLOBALS['app.title'].$title; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/jquery.jscrollpane.css" /> 
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/unicorn.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/style.css" />
      <?php echo $cssTop . $styleTop . $jsTop . $scriptTop;?>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <![endif]-->
      
  </head> 
  <body data-color="grey" class="flat">
    <?php if(!isset($_GET['ajax']) && $GLOBALS['app.user']->Admin == 'Y'){?>
    <div id="wrapper">
      <div id="header">
        <h1><a href="home.php"><img src="<?php echo $GLOBALS['app.ui.theme.folder']; ?>img/ccs_logo.png"/></a></h1>
        <a id="menu-trigger" href="#"><i class="fa fa-bars"></i></a>  
      </div>

      <?php echo funcUI::getPage('navigation.php', 'index');?>
      <?php
      if (funcCore::requireClasses($GLOBALS['app.var.user.classname']) && $GLOBALS['app.user.loggedin']) {
        echo funcUI::getPage('form.php', 'login');
      }
      ?>
      
      <div id="content">
        <div id="breadcrumb">
          <a href="home.php"><i class="fa fa-home"></i> Home</a>
          <?php foreach($GLOBALS['breadCrumbMap'] as $breadCrumb){
            echo '<a href="' . $breadCrumb["url"] . '">' . $breadCrumb['text'] . '</a>';
          }?>
        </div>
        <div class="container-fluid">
            <div class="widget-content">
              <?php echo $content; ?>             
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div id="footer" class="col-xs-12">
          <table>
            <tr>
              <td height="50" style="background-image:url(<?php echo $GLOBALS['app.ui.theme.folder']; ?>images/footer_url.gif);padding:0 40px">
                    <span style="float:right">
                      <a href="http://www.bottomline.co.za/" target="_blank" class="bli" title="bottomline interactive"></a>
                      <a href="http://www.ccs.co.za/" target="_blank" class="ccs" title="customer care solutions"></a>
                      <a href="http://www.simdirect.co.za/" target="_blank" class="sim" title="sim direct"></a>
                    </span>
                </td>
            </tr> 
          </table>  
          <p><?php echo session_id();?></p> 
        </div>
      </div>
    </div>
    <?php } else{ ?>
    <div class="wrapper">
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <?php echo $content; ?>
          </div>  
        </div>  
      </div>
    </div>  
    <?php };?>

            <script src="js/excanvas.min.js"></script>
            <script src="js/jquery.min.js"></script>
            <script src="js/jquery-ui.custom.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.flot.min.js"></script>
            <script src="js/jquery.flot.resize.min.js"></script>
            <script src="js/jquery.sparkline.min.js"></script>
            <script src="js/fullcalendar.min.js"></script>
            <script src="js/jquery.nicescroll.min.js"></script>
            <script src="js/unicorn.js"></script>
            <script src="js/unicorn.dashboard.js"></script>
            <?php echo $cssBottom . $styleBottom . $jsBottom . $scriptBottom; ?>
  </body>
</html>