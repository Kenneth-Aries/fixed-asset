<?php
if ($GLOBALS['app.user.loggedin']) {
  funcCore::requireClasses('usertitle');
  $userRole = new UserTitle($GLOBALS['app.user']->IduserTitle);
  $content .= '<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: #15147b;">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <img src="images/facets_logo.png" width="835" height="160"/>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span style="display:block; font-size:14px; padding: 11.5px 10px; color:#337ab7; cursor:default">Welcome to Facet Asset Management  ' . $GLOBALS['app.user']->UserName . '. Your role is ' . $userRole->TitleName . '</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user fa-fw"></i>Options <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <a href="home.php?module=user&action=modify&id=' . $GLOBALS['app.user']->Iduser . '"><i class="fa fa-user fa-fw"></i> My Profile</a>
                                <li>
                                    <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>';
}
else {
    funcCore::requireClasses($GLOBALS['app.var.user.classname']);
    funcForm::validation2('frmLogin');
    $content .= funcForm::form('frmLogin', 'post') . 
                funcForm::hidden('request', 'login') . '
                <div class="container">
                <img src="images/facets_logo.png" width="1024" height="196" style="align:center;margin-top: 50px;margin-left: 70px;"/>
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-panel panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Facets Physical Asset Management</h3>
                            </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="control-label" for="username">Email OR Username</label>
                                            ' . funcForm::text($GLOBALS['app.var.user.username'], null, 'form-control') . '
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="password">Password</label>
                                            ' . funcForm::password($GLOBALS['app.var.user.password'], null, 'form-control') . '
                                        </div>' . 
                                        funcForm::submit('btnLogin', 'Login', 'btn btn-success btn-block form-control') . '
                                    </fieldset>
                                </div>
                            </div>
                    </div>';
}
?>