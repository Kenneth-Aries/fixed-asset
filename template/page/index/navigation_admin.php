<?php
$content .= '<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                  <li>
                        <a href="?module=entities&action=index"><i class="fa fa-circle-o fa-fw"></i> Entity</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i> Users <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="home_admin.php?module=user&action=index">User List</a>
                            </li>
                            <li>
                                <a href="home_admin.php?module=user&action=titles">User Titles</a>
                            </li>
                            <li>
                                <a href="home_admin.php?module=user&action=modify&id=' . $GLOBALS['app.user']->Iduser . '">My Profile</a>
                            </li>
                            <li>
                                <a href="logout.php">Logout</a>
                            </li>
                        </ul>
                    </li>   
                  </ul>
                </div>
              </div>
            </nav>';
?>