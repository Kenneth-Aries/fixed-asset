<?php
$content .= '<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">';
                    if(isset($GLOBALS['app.var.user.permissions']['Assets']['view']) && $GLOBALS['app.var.user.permissions']['Assets']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=assets&action=index"><i class="fa fa-chain fa-fw"></i> Assets</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Locations']['view']) && $GLOBALS['app.var.user.permissions']['Locations']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=locations&action=index"><i class="fa fa-thumb-tack fa-fw"></i> Locations</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Entity']['view']) && $GLOBALS['app.var.user.permissions']['Entity']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=entities&action=index"><i class="fa fa-circle-o fa-fw"></i> Entity</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Category']['view']) && $GLOBALS['app.var.user.permissions']['Category']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=category&action=index"><i class="fa fa-th-list fa-fw"></i> Category</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Status']['view']) && $GLOBALS['app.var.user.permissions']['Status']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=status&action=index"><i class="fa fa-list-alt fa-fw"></i> Status</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Cost Centre']['view']) && $GLOBALS['app.var.user.permissions']['Cost Centre']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=cost-centre&action=index"><i class="fa fa-credit-card fa-fw"></i> Cost Centre</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Conditions']['view']) && $GLOBALS['app.var.user.permissions']['Conditions']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=conditions&action=index"><i class="fa fa-bar-chart-o fa-fw"></i> Conditions</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Import Data']['view']) && $GLOBALS['app.var.user.permissions']['Import Data']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="?module=assets&action=import"><i class="fa fa-download fa-fw"></i> Import Data</a>
                      </li>';
                    }
                    if(isset($GLOBALS['app.var.user.permissions']['Users']['view']) && $GLOBALS['app.var.user.permissions']['Users']['view'] == 1) {
                      $content .= '
                      <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i> Users <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">';
                      if ($GLOBALS['app.user']->IduserTitle == 2 || $GLOBALS['app.user']->IduserTitle == 5) {
                        $content .= '
                            <li>
                                <a href="home.php?module=user&action=index">User List</a>
                            </li>';
                      }
                      if ($GLOBALS['app.user']->IduserTitle == 5) {
                        $content .= '
                            <li>
                              <a href="home.php?module=user&action=titles">User Titles</a>
                            </li>';
                      }
                            $content .= '
                            <li>
                                <a href="home.php?module=user&action=modify&id=' . $GLOBALS['app.user']->Iduser . '">My Profile</a>
                            </li>
                            <li>
                                <a href="logout.php">Logout</a>
                            </li>
                        </ul>
                      </li>';
                    }
                    if ($GLOBALS['app.user']->IduserTitle == 5) {
                      /** Only super admin users can access role functionality **/
                      $content .= '
                      <li>
                        <a href="?module=roles&action=index"><i class="fa fa-download fa-fw"></i> Manage Roles</a>
                      </li>';
                    }
                  $content .= '</ul>
                </div>
              </div>
            </nav>';
?>