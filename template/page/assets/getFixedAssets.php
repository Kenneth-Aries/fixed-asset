<?php
funcCore::requireClasses('asset, assettracker, assetimage, location, costcentre, status, condition');
$assetId = funcArray::get($_REQUEST, 'assetId');
$assetId = 89;

$js = <<<JS
jQuery(document).ready(function($) {
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption

  $('#myImg').click(function(e) {
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
  });


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);



if(!empty($assetId)) {
  $fixedAssets = AssetTracker::get(null, "`idasset` = {$assetId}");
  if(!empty($fixedAssets)) {
    $detailsTable = '
    <table class="table table-striped table-bordered table-hover table-responsive">
      <tr><th>Asset Number</th><th>Location Name</th><th>Category Name</th><th>Description</th><th>Make</th><th>Model</th><th>Serial Number</th><th>Cost Center</th><th>Condition</th><th>Status</th><th>Longitude/Latitude</th><th>User Name</th><th>Date</th></tr>';
    foreach ($fixedAssets as $fixedAsset) {
      $detailsTable .= '<tr>
                          <td>' . $fixedAsset->AssetNumber . '</td>
                          <td>' . Location::getName($fixedAsset->Idlocation) . '</td>
                          <td>' . $fixedAsset->CategoryName . '</td>
                          <td>' . $fixedAsset->AssetDescription . '</td>
                          <td>' . $fixedAsset->Make . '</td>
                          <td>' . $fixedAsset->Model . '</td>
                          <td>' . $fixedAsset->SerialNumber . '</td>
                          <td>' . CostCentre::getName($fixedAsset->IdcostCentre) . '</td>
                          <td>' . Condition::getName($fixedAsset->Idcondition) . '</td>
                          <td>' . Status::getName($fixedAsset->Idstatus) . '</td>
                          <td>' . (!empty($fixedAsset->Longitude) || !empty($fixedAsset->Longitude) ? $fixedAsset->Longitude . ' / ' . $fixedAsset->Latitude : null) . '</td>
                          <td>' . $fixedAsset->DateTime . '</td>
                          <td>' . Status::getName($fixedAsset->Iduser) . '</td>
                        </tr>';
    }
    $detailsTable .= '</table>';
//    echo $detailsTable;
  }
}
$detailsTable .= '
<img id="myImg" src="' . substr($assetImage->AssetUrl, 1)  . '" width="300" height="200">

<!-- Trigger the Modal -->
<img id="myImg" src="img_fjords.jpg" alt="Trolltunga, Norway" width="300" height="200">

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close" onclick="document.getElementById(\'myModal\').style.display=\'none\'">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>';
//exit;
//$detailsTable .= '<div style="float: left;height: auto;"><img src="' . substr($assetImage->AssetUrl, 1)  . '"></div>';
//echo funcArray::display();
//exit;
$content .= $detailsTable;
?>

