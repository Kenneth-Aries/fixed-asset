<?php
funcCore::requireClasses('usertitle');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if ($GLOBALS['app.user']->IduserTitle != 5) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}
$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnAdd').click(function(e) {
    $('#txtAddTitle').show();
    $('#btnSave').show();
    
  });
  $('.btnEdit').click(function(e) {
     var Id = this.id.substring(7);
     $("#txtOnly"+Id).hide();
     $("#txtBox"+Id).show();
     $('#btnSave').show();
  });
});
JS;

$titles = Usertitle::get();

funcUI::queueScript('js', 'bottom', 'embed', $js);
$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User Titles</h1>
                    </div>
                </div>
            <table>
                <tr>
                    <td>' . funcForm::button('btnAdd', 'Add title', 'btn btn-primary') . '</td>
                    <td style="padding-left:15px;">' . funcForm::text('txtAddTitle', null, "form-control",null, null, 'style="display:none;" Placeholder=Enter&nbsp;Title') . '</td>
                    <td style="padding-left:15px;">' . funcForm::submit('btnSave', 'Save', 'btn btn-success', null, 'style="display:none;"') . '</td>
                </tr>
            </table>';
if ($titles) {
    $content .= '<table class="table table-striped table-bordered table-hover table-responsive" style="width:70%;">
                    <tr>
                        <th>Action</th>
                        <th>Name</th>
                    </tr>';
    foreach ($titles as $t) {
        $content .= '<tr>
                        <td style="padding-left:15px;">' . funcForm::button('btnEdit' . $t->IduserTitle, 'Edit', "btnEdit form-control btn btn-info", null, 'style="width:30%;"') .
                            '<span style="margin-left:15px;">' . funcForm::button('btnDelete', 'Delete', "form-control btn btn-danger", null, 'style="width:30%;"') . '</span></td>
                        <td>
                            <span id="txtOnly' . $t->IduserTitle . '">' . $t->TitleName . '</span>
                            <span id="txtBox' . $t->IduserTitle . '" style="display:none;">' . funcForm::text('txtUpdate' . $t->IduserTitle , $t->TitleName, "form-control", null, null) . '</span></td>
                    </tr>';    
    }    
}
$content.='</table></div>';
?>