<?php
funcCore::requireClasses('location');

$entityId = funcArray::get($_REQUEST, 'entityId');
$returnTable = funcArray::get($_REQUEST, 'table');
$locationId = funcArray::get($_REQUEST, 'locationId');
//$locations = Location::get(null, "`iduser` = {$GLOBALS['app.user']->Iduser} AND `identity` = {$entityId}");
$locations = Location::get(null, "`identity` = {$entityId}");

if(!$returnTable){

    if (!empty($locations)) {
      $locationOptions = funcArray::classesToSelectOptions($locations, 'Idlocation', 'LocationName');
      echo '<span id="locationSpan" style="margin-left: 10px;">Location ' . funcForm::select('ddLocation', $locationId, 'Select a Location', $locationOptions, "form-control") . '</span>';
    }
    
}else{
    
    $content .= '<tr>
                        <th  style="color: #FFFFFF;background-color: #15147b" width="20%">Action</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Name</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Building</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Region</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Country</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Barcode</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Longitude/Latitude</th>
                    </tr>';
    foreach ($locations as $l) {
        $content .= '<tr>
                        <td style="padding-left:15px;">' . funcForm::button('btnEdit' . $l->Idlocation, 'Edit', "btnEdit form-control btn btn-info", null, 'style="width:35%;"') .
                          '<span style="margin-left:15px;">' . funcForm::submit('btnDelete' . $l->Idlocation, 'Delete', "btnDelete form-control btn btn-danger", null, 'style="width:45%;"') . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyName' . $l->Idlocation . '">' . $l->LocationName. '</span>
                          <span id="txtBoxName' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateName[' . $l->Idlocation . ']' , $l->LocationName, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyBuilding' . $l->Idlocation . '">' . $l->Building. '</span>
                          <span id="txtBoxBuilding' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateBuilding[' . $l->Idlocation . ']' , $l->Building, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyRegion' . $l->Idlocation . '">' . $l->Region. '</span>
                          <span id="txtBoxRegion' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateRegion[' . $l->Idlocation . ']' , $l->Region, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyCountry' . $l->Idlocation . '">' . $l->Country. '</span>
                          <span id="txtBoxCountry' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateCountry[' . $l->Idlocation . ']' , $l->Country, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyBarcode' . $l->Idlocation . '">' . $l->Barcode. '</span>
                          <span id="txtBoxBarcode' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateBarcode[' . $l->Idlocation . ']' , $l->Barcode, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyLongitude' . $l->Idlocation . '">' . $l->Longitude . '/' . $l->Latitude . '</span>
                          <span id="txtBoxLongitude' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateLongitude[' . $l->Idlocation . ']' , $l->Longitude, "form-control", null, null, 'Placeholder=Enter&nbsp;Longitude') . '<br>' . funcForm::text('txtUpdateLatitude[' . $l->Idlocation . ']' , $l->Latitude, "form-control", null, null, 'Placeholder=Enter&nbsp;Latitude') . '</span>
                        </td>
                    </tr>';
    }
    
    echo $content;
}
exit;

?>