<?php
funcCore::requireClasses('location,entity');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');
if (!isset($GLOBALS['app.var.user.permissions']['Locations']['view']) || $GLOBALS['app.var.user.permissions']['Locations']['view'] == 0) {
  funcCore::redirect('home.php?module=index&action=index', 'You do not have permission to view this page.', $GLOBALS['app.alert.Error']);
}
$entityOptions = funcArray::classesToSelectOptions(Entity::get(), 'Identity', 'EntityName');

$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnAdd').click(function(e) {
    $('#tblAddLocation').show();
    $('#btnSave').show();
    
  });
  $('.btnEdit').click(function(e) {
     var Id = this.id.substring(7);
     $("#txtOnlyName"+Id).hide();
     $("#txtOnlyBuilding"+Id).hide();
     $("#txtOnlyRegion"+Id).hide();
     $("#txtOnlyCountry"+Id).hide();
     $("#txtOnlyBarcode"+Id).hide();
     $("#txtOnlyLongitude"+Id).hide();
     
     $("#txtBoxName"+Id).show();
     $("#txtBoxBuilding"+Id).show();
     $("#txtBoxRegion"+Id).show();
     $("#txtBoxCountry"+Id).show();
     $("#txtBoxBarcode"+Id).show();
     $("#txtBoxLongitude"+Id).show();
     $('#btnSave').show();
  });
        
        
  $('#ddEntity').on("change", function(){

        $("#location_table tr").remove(); 
        $('#locationSpan').children("span").remove();
        var entityId = $(this).val();
        $('#idEntity').val(entityId);
        $('ddEntity').prop('selectedIndex', 0);
        $.ajax({
          type: 'GET',
          url:'home.php?ajax&module=locations&action=getByEntity&entityId=' + entityId + '&table=true',
          success: function(result){

            $("#location_table").append(result);  

          }
        });
    });
        
  $('.btnDelete').click(function(e) {
    if (confirm('Are you sure you would like to delete this item?')) {
      var Id = this.id.substring(9);
      $('#deleteId').val(Id);
    }
    else {
      return false;
    }
  });
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);

funcForm::validation2('frmLocation');

$location = null;
if ($GLOBALS['app.user']->IduserTitle == 5) {
  $location = Location::get();
}
elseif (!empty($GLOBALS['app.user']->Identity)) {
  $location = Location::get(null, "`identity` = {$GLOBALS['app.user']->Identity}");
}
//echo funcArray::display($location);
//exit;
$content .= funcForm::form('frmLocation', 'post') . funcForm::hidden('deleteId', null) . funcForm::hidden('idEntity', ($GLOBALS['app.var.site.mode'] != 'super_user' ? $GLOBALS['app.var.site.identity'] : null));

$userOptions = funcArray::classesToSelectOptions(User::get(), 'Iduser', 'UserName');
//$content .= funcForm::form('frmLocation', 'post') . funcForm::hidden('deleteId', null) . funcForm::hidden('idEntity', null);

$content .= '<div id="page-wrapper" style="background-color: #F5F5F5">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Locations</h1>
                    </div>
                </div>
            <table>';

if ($GLOBALS['app.var.site.mode'] === "super_user") {
  //echo "here here";
  $content .= '<tr><td><span>Entity ' . funcForm::select('ddEntity', $entityId, 'Please select', $entityOptions, 'form-control') . '</span></td></tr>';

}
if ($GLOBALS['app.var.user.permissions']['Locations']['add'] == 1) {
  $content .= '<tr>
                    <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::button('btnAdd', 'Add Location', 'btn btn-primary') . '</td>
                    <td style="padding-left:15px;">' . funcForm::submit('btnSave', 'Save', 'btn btn-success', null, 'style="display:none;"') . '</td>
              </tr>';
}
$content .= '</table>
            <table style="display:none; margin: 10px 0 10px 0;" id="tblAddLocation">
              <tr>
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtName', null, "form-control", null, null, 'Placeholder=Enter&nbsp;Name') . '</td>
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtBuilding', null, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Building') . '</td>
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtRegion', null, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Region') . '</td>
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtCountry', null, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Country') . '</td>
              </tr>
              <tr>  
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtBarcode', null, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Barcode') . '</td>
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtLongitude', null, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Longitude') . '</td>
                <td style="padding-left:5px;padding-bottom:5px;">' . funcForm::text('txtLatitude', null, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Latitude') . '</td>
                <td style="padding-left:5px;padding-bottom:5px;"></td>
              </tr>
            </table>';
//<td style="padding-left:5px;padding-bottom:5px;">' . funcForm::select('ddUser', null, 'Select', $userOptions, "validate['%requiredIfVisible'] form-control", null, null, 'Placeholder=Enter&nbsp;Latitude') . '</td>
if (!empty($location)) {
  $showAction = ($GLOBALS['app.var.user.permissions']['Locations']['edit'] == 0 && $GLOBALS['app.var.user.permissions']['Locations']['delete'] == 0 ? false : true);
  $content .= '<table id="location_table" class="table table-striped table-bordered table-hover table-responsive" style="width:100%;margin-top:20px;">
                    <tr>';
  if ($showAction) {
    $content .= '<th style="color: #FFFFFF;background-color: #15147b" width="20%">Action</th>';
  }
  if ($GLOBALS['app.user']->IduserTitle == 5) {
    $content .= '<th style="color: #FFFFFF;background-color: #15147b">Entity</th>';
  }
  $content .= '<th style="color: #FFFFFF;background-color: #15147b">Name</th>
               <th style="color: #FFFFFF;background-color: #15147b">Building</th>
               <th style="color: #FFFFFF;background-color: #15147b">Region</th>
               <th style="color: #FFFFFF;background-color: #15147b">Country</th>
               <th style="color: #FFFFFF;background-color: #15147b">Barcode</th>
               <th style="color: #FFFFFF;background-color: #15147b">Longitude/Latitude</th>
             </tr>';
  foreach ($location as $l) {
    $content .= '<tr>';
    if ($showAction) {
      $content .= '<td style="padding-left:15px;">' . ($GLOBALS['app.var.user.permissions']['Locations']['edit'] == 1 ? funcForm::button('btnEdit' . $l->Idlocation, 'Edit', "btnEdit form-control btn btn-info", null, 'style="width:35%;"') : null) . ($GLOBALS['app.var.user.permissions']['Locations']['delete'] == 1 ? '<span style="margin-left:15px;">' . funcForm::submit('btnDelete' . $l->Idlocation, 'Delete', "btnDelete form-control btn btn-danger", null, 'style="width:45%;"') . '</span>' : null) . '</td>';
    }
    if ($GLOBALS['app.user']->IduserTitle == 5) {
      $entityName = Entity::getName($l->Identity);
      $content .= '<td>' . $entityName . '</td>';
    }
    $content .= '</td>
                        <td>
                          <span id="txtOnlyName' . $l->Idlocation . '">' . $l->LocationName . '</span>
                          <span id="txtBoxName' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateName[' . $l->Idlocation . ']', $l->LocationName, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyBuilding' . $l->Idlocation . '">' . $l->Building . '</span>
                          <span id="txtBoxBuilding' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateBuilding[' . $l->Idlocation . ']', $l->Building, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyRegion' . $l->Idlocation . '">' . $l->Region . '</span>
                          <span id="txtBoxRegion' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateRegion[' . $l->Idlocation . ']', $l->Region, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyCountry' . $l->Idlocation . '">' . $l->Country . '</span>
                          <span id="txtBoxCountry' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateCountry[' . $l->Idlocation . ']', $l->Country, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyBarcode' . $l->Idlocation . '">' . $l->Barcode . '</span>
                          <span id="txtBoxBarcode' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateBarcode[' . $l->Idlocation . ']', $l->Barcode, "form-control", null, null) . '</span>
                        </td>
                        <td>
                          <span id="txtOnlyLongitude' . $l->Idlocation . '">' . $l->Longitude . '/' . $l->Latitude . '</span>
                          <span id="txtBoxLongitude' . $l->Idlocation . '" style="display:none;">' . funcForm::text('txtUpdateLongitude[' . $l->Idlocation . ']', $l->Longitude, "form-control", null, null, 'Placeholder=Enter&nbsp;Longitude') . '<br>' . funcForm::text('txtUpdateLatitude[' . $l->Idlocation . ']', $l->Latitude, "form-control", null, null, 'Placeholder=Enter&nbsp;Latitude') . '</span>
                        </td>
                    </tr>';
  }
}
else {

  $content .= '<table id="location_table" class="table table-striped table-bordered table-hover table-responsive" style="width:100%;margin-top:20px;">
                    <tr>
                        <th  style="color: #FFFFFF;background-color: #15147b" width="20%">Action</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Name</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Building</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Region</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Country</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Barcode</th>
                        <th  style="color: #FFFFFF;background-color: #15147b">Longitude/Latitude</th>
                    </tr>';

}
$content .= '</table></div>' . funcForm::closeForm();
?>