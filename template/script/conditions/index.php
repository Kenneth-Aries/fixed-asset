<?php
funcCore::requireClasses('condition');
$conditions = funcArray::get($_POST, 'txtUpdate');
$newCondition = funcArray::get($_POST, 'txtAddCondition');
$deleteId = funcArray::get($_POST, 'deleteId');

if (!empty($newCondition)) {
  $sql = "select `idcondition`,`condition_name` from `condition` where `condition_name` = '{$newCondition}'";
  $exisdtingCondition = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($exisdtingCondition)) {
    $sql = "INSERT INTO `condition` SET `condition_name` = '{$newCondition}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $condition = new Condition();
  }
  else {
    funcCore::redirect('home.php?module=conditions', 'Error: This condition already exists', $GLOBALS['app.alert.success']);
  }
}

if (!empty($conditions)) {
  foreach ($conditions as $key => $condition) {
    $sql = "UPDATE `condition` SET `condition_name` = '{$condition}' WHERE `idcondition` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}

if (!empty($deleteId)) {
  $sql = "DELETE FROM `condition` WHERE `idcondition` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=conditions', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=conditions', 'Saved successfully', $GLOBALS['app.alert.success']);

?>