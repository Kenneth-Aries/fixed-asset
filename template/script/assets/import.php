<?php
/**
 * Created by PhpStorm.
 * User: ken13
 * Date: 11/5/2017
 * Time: 3:25 PM
 */
funcCore::requireClasses('asset,costcentre,condition,status');
$entityId = funcArray::get($_POST, 'ddEntity', $GLOBALS['app.user']->Identity);
$locationId = funcArray::get($_POST, 'ddLocation');
$files = $_FILES["Filebrowse"]["tmp_name"];
$fileNames = $_FILES["Filebrowse"]["name"];
$invalidFiles = array();
if (!empty($fileNames)) {
  $extensionsArray = array();
  foreach ($fileNames as $fileName) {
    $tmpFileName = explode('.', $fileName);
    $extension = end($tmpFileName);
    if ($extension != 'xls' && $extension != 'xlsx') {
      $invalidFiles[] = $fileName;
    }
  }
}
if (!empty($invalidFiles)) {
  $invalidFiles = implode("<br />", $invalidFiles);
  funcCore::redirect('home.php?module=assets&action=import', 'The following files have invalid file extensions.<br />' . $invalidFiles, $GLOBALS['app.alert.error']);
}
if (!empty($files)) {
  include $GLOBALS['app.folder.include'] . 'PHPExcel.php';
  include $GLOBALS['app.folder.include'] . 'PHPExcel/Writer/Excel5.php';

  foreach ($files as $key => $inputFileName) {
    $mandatoryColumnCount = 16;
    try {
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
      die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
    }
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = funcData::num2alpha($mandatoryColumnCount - 1);
    for ($row = 1; $row <= $highestRow; $row++) {
      $sheetArray = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
      $sheetArray = funcArray::getFirstItem($sheetArray);
      if (strtolower($sheetArray[0]) == 'description' && strtolower($sheetArray[1]) == 'make' && strtolower($sheetArray[2]) == 'model') {
        /** Do not import header row  **/
      }
      else {
        $costCentre = $sheetArray[9];
        $costCentreId = CostCentre::getIdFromName($costCentre);
        if (empty($costCentreId)) {
          $costCentre = 'default';
          $costCentreId = CostCentre::getIdFromName($costCentre);
        }
        $condition = $sheetArray[10];
        $conditionId = Condition::getIdFromName($condition);
        if (empty($conditionId)) {
          $condition = 'Unknown';
          $conditionId = Condition::getIdFromName($condition);
        }
        $status = $sheetArray[11];
        $statusId = Status::getIdFromName($status);
        if (empty($statusId)) {
          $status = 'Unknown';
          $statusId = Condition::getIdFromName($status);
        }
        $asset = new Asset();
        $asset->Idlocation = $locationId;
        $asset->Iduser = $GLOBALS['app.user']->Iduser;
        $asset->UserName = $GLOBALS['app.user']->UserName;
        $asset->AssetDescription = $sheetArray[0];
        $asset->Make = $sheetArray[1];
        $asset->Model = $sheetArray[2];
        $asset->AssetBarcode = $sheetArray[3];
        $asset->AssetNumber = $sheetArray[4];
        $asset->SerialNumber = $sheetArray[5];
        $asset->SerialNumber = $sheetArray[6];
        $asset->Longitude = $sheetArray[7];
        $asset->Latitude = $sheetArray[8];
        $asset->CategoryName = $sheetArray[9];
        $asset->Cost = $sheetArray[10];
        $asset->Supplier = $sheetArray[11];
        $asset->Invoice = $sheetArray[12];
        $asset->IdcostCentre = $costCentreId;
        $asset->Cost_Centre = $costCentre;
        $asset->Idcondition = $conditionId;
        $asset->Condition = $condition;
        $asset->Idstatus = $statusId;
        $asset->Status = $status;
        $asset->save();
      }
    }
  }
  funcCore::redirect('home.php?module=assets&action=import', 'Data Successfully Imported.', $GLOBALS['app.alert.success']);
}
funcCore::redirect('home.php?module=assets&action=import', 'Error: No files imported.', $GLOBALS['app.alert.error']);
?>