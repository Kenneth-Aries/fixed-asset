<?php
/**
 * Created by PhpStorm.
 * User: ken13
 * Date: 11/7/2017
 * Time: 6:40 PM
 */
funcCore::requireClasses('usertitle');
$roles = funcArray::get($_POST, 'txtUpdate');
$newRole = funcArray::get($_POST, 'txtAddRole');
$deleteId = funcArray::get($_POST, 'deleteId');

if (!empty($newRole)) {
  $sql = "select `iduser_title`,`title_name` from `user_title` where `title_name` = '{$newRole}'";
  $exisdtingRole = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($exisdtingRole)) {
    $sql = "INSERT INTO `user_title` SET `title_name` = '{$newRole}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $role = new UserTitle();
  }
  else {
    funcCore::redirect('home.php?module=roles', 'Error: This role already exists', $GLOBALS['app.alert.success']);
  }
}

if (!empty($roles)) {
  foreach ($roles as $key => $role) {
    $sql = "UPDATE `user_title` SET `title_name` = '{$role}' WHERE `iduser_title` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}

if (!empty($deleteId)) {
  $sql = "DELETE FROM `user_title` WHERE `iduser_title` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=roles', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=roles', 'Saved successfully', $GLOBALS['app.alert.success']);
