<?php
funcCore::requireClasses('costcentre');
$newCostCentre = funcArray::get($_POST, 'txtAddCostCentre');
$costCentres = funcArray::get($_POST, 'txtUpdate');
$deleteId = funcArray::get($_POST, 'deleteId');
if (!empty($newCostCentre)) {
  $sql = "select `idcost_centre`,`cost_centre` from `cost_centre` where `cost_centre` = '{$newCostCentre}'";
  $existingCostCentre = $GLOBALS['app.db']->executeQuery($sql, true);
  if (empty($existingCostCentre)) {
    $sql = "INSERT INTO `cost_centre` SET `cost_centre` = '{$newCostCentre}'";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
  else {
    funcCore::redirect('home.php?module=cost-centre', 'Error: This cost centre already exists', $GLOBALS['app.alert.success']);
  }

}

if (!empty($costCentres)) {
  foreach ($costCentres as $key => $costCentre) {
    $sql = "UPDATE `cost_centre` SET `cost_centre` = '{$costCentre}' WHERE `idcost_centre` = {$key}";
    $result = $GLOBALS['app.db']->executeSQL($sql);
  }
}
if (!empty($deleteId)) {
  $sql = "DELETE FROM `cost_centre` WHERE `idcost_centre` = {$deleteId}";
  $result = $GLOBALS['app.db']->executeSQL($sql);
  funcCore::redirect('home.php?module=cost-centre', 'Item successfully removed', $GLOBALS['app.alert.success']);
}

funcCore::redirect('home.php?module=cost-centre', 'Saved successfully', $GLOBALS['app.alert.success']);

?>