<?php
require_once('include/setup.app.php');
require_once($GLOBALS['app.folder.include'].'class.classgen.php');

$tables = $GLOBALS['app.db']->getTables($GLOBALS['app.db.db']);

if (isset($_REQUEST['action']) && ($_REQUEST['action']=='update' || $_REQUEST['action']=='create') && isset($_REQUEST['table']) && in_array($_REQUEST['table'], $tables)) {
  $cg = new ClassGen($_REQUEST['table']);
  $cg->GenerateClass();
  if ($cg->WriteFile(true)) {
    header('location: classgen.php');
  }
  else {
    $content .= 'There was a problem with method: ' . $_REQUEST['action'] . ' on table ' . $_REQUEST['table'];
  }
}

if (!is_array($tables) || empty($tables)) {
  $content .= 'No tables in database: ' . $GLOBALS['app.db.db'];
}
else {
  $content .= '<table border="1" style="border-collapse:collapse;border-color:#CCC;">
    <tr><th style="text-align:left">Table/Class</th>
        <th>Status</th>
    </tr>';
  foreach ($tables as $t) {
    $cg = new ClassGen($t);
    if (!empty($cg->error)) {
        $content .= $cg->error;
    }
    else {
      if ($cg->generateClass()) {
        $file = $GLOBALS['app.folder.include.generic'] . 'class.' . $cg->fileName . '.php';
        if (is_file($file)) {
          $classContent = funcFS::readFile($file);
          if ($classContent==$cg->output) {
            $finalOutput = 'Latest';
          }
          else {
            $finalOutput = ' Not latest. <a href="?action=update&amp;table='.$t.'">Update</a>';
          }
        }
        else {
          $finalOutput = ' Does not exist. <a href="?action=create&amp;table='.$t.'">Create</a>?';
        }
        $content .= '<tr><td>' . $cg->className . '</td><td>' . $finalOutput . '</td></tr>';
      }
      else {
        $content .= $cg->error;
      }
    }
  }
  $content .= '</table>';
}

funcUI::renderOutput($GLOBALS['app.ui.theme'], 'html.php');
?>