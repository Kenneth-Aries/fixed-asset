<?php

class funcData {

  static function nz($value, $acceptsNull = false, $str = false, $default = null) {
    if ((is_null($value) || $value == '') && (is_null($default) || $default == '')) {
      if ($acceptsNull) {
        return 'NULL';
      }
      else {
        return ($str ? "''" : '0');
      }
    }
    $result = (is_null($value) ? $default : $value);
    return ($str ? "'$result'" : $result);
  }

  /**
   * Validate an email address.
   * Provide email address (raw input)
   * Returns true if the email address has the email
   * address format and the domain exists.
   */
  static function validateEmail($email, $doDNScheck = false) {
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
      $isValid = false;
    }
    else {
      $domain = substr($email, $atIndex + 1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64) {
        // local part length exceeded
        $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255) {
        // domain part length exceeded
        $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
        // local part starts or ends with '.'
        $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local)) {
        // local part has two consecutive dots
        $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
        // character not valid in domain part
        $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain)) {
        // domain part has two consecutive dots
        $isValid = false;
      }
      else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
        // character not valid in local part unless 
        // local part is quoted
        if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
          $isValid = false;
        }
      }
      if ($doDNScheck && $isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
        // domain not found in DNS
        $isValid = false;
      }
    }
    return $isValid;
  }

  static function exportSqlToCsv($sql, $headers = true) {
    $result = mysqli_query($GLOBALS['app.db']->getConnection(), $sql);
    $fields = mysqli_num_fields($result);
    $header = '';
    $data = '';

    if ($headers) {
      for ($i = 0; $i < $fields; $i++) {
        if ($i < $fields - 1) {
          $header .= $GLOBALS['app.db']->mysqli_field_name($export, $i) . ",";
        }
        else {
          $header .= $GLOBALS['app.db']->mysqli_field_name($export, $i) . ",\n";
        }
      }
    }

    while ($row = mysqli_fetch_row($result)) {
      $line = '';
      foreach ($row as $value) {
        if (!isset($value) || $value == '') {
          $value = ",";
        }
        else {
          $value = str_replace('"', '""', $value);
          $value = '"' . $value . '"' . ",";
        }
        $line .= $value;
      }
      $data .= trim($line) . "\n";
    }
    $data = str_replace("\r", '', $data);
    return $header . $data;
  }

  static function csvField($field) {
    if (empty($field) || is_array($field)) {
      return null;
    }
    //check for comma or quotes
    if (strpos($field, ',') !== false || strpos($field, '"') !== false || strpos($field, "\n") !== false || strpos($field, "\r") !== false) {
      //found comma or quotes in field - double-up the quotes within and put quotes around
      $field = '"' . str_replace('"', '""', $field) . '"';
    }
    return $field;
  }

  static function arrayToCsvLine($array) {
    $line = null;
    foreach ($array as $value) {
      if (!isset($value) || $value == '') {
        $value = ",";
      }
      else {
        $value = str_replace('"', '""', $value);
        $value = '"' . $value . '"' . ",";
      }
      $line .= $value;
    }
    $line = trim(rtrim($line, ','));
    return $line;
  }

  static function html2xls($htmltable, &$xls, $index = 0, $rowHeight = null) {
    //PHPExcel must be passed into $xls by reference
    set_time_limit(30 * 60);
    $xls->setActiveSheetIndex($index);
    libxml_use_internal_errors(true);
    $dom = new DOMDocument;
    $result = $dom->loadHTML($htmltable);
    libxml_use_internal_errors(false);
    $rows = $dom->getElementsByTagName('tr');

    foreach ($rows as $rowNum => $row) {
      $columns = $row->getElementsByTagName('th');
      if (!empty($rowHeight) && $rowNum > 0) {
        $currentRow = $rowNum + 1;
        $xls->getActiveSheet()->getRowDimension($currentRow)->setRowHeight($rowHeight);
      }

      if ($columns->length == 0) {
        $columns = $row->getElementsByTagName('td');
      }
      if ($columns->length > 0) {
        /** Construct column number array **/
        $columnNum = array();
        $columnNum[$xls->getActiveSheet()->getTitle()] = 0;
        $cellAfterMerge = null;
        foreach ($columns as $fieldNum => $field) {
          /*Below is a list of variables that will be used for styling the XLS documnet cell by cell*/
          $autoWidth = null;
          $autoWidthId = null;
          $border = null;
          $borderId = null;
          $cellBgColor = null;
          $cellBgColorId = null;
          $fontSize = null;
          $fontSizeId = null;
          $fontColor = null;
          $fontColorId = null;
          $cellMerge = null;
          //$mergeStartNum = null;
          $mergeStart = null;
          $mergeUntil = null;
          $cellBold = null;
          $cellBoldId = null;
          $cellDataType = null;
          $cellDataTypeId = null;
          $cellTotal = null;
          $cellAlign = null; //option LEFT, CENTER, RIGHT
          $cellAlignId = null;
          $cellAsNum = null;

          /*Fist thing is first, we need to see if there are any attributes to begin with*/
          if ($columns->item($fieldNum)->hasAttributes()) {
            /*Now we loop throught the DOM::Attributes of the cell/object/item so that we can find the styles that is needed*/
            $n = $fieldNum + $cellAfterMerge;
            $cell = funcData::num2alpha($n) . ($rowNum + 1);
            foreach ($columns->item($fieldNum)->attributes as $attr) {

              /*The attribute that we will be looking for is a unique and other wise useless attribute, However this is what holds our STYLE 8D*/
              if ($attr->nodeName == 'excelformat') {
                /*this attribute hold a list of CSV so we want to explode it into an array*/
                $excelFormat = explode(',', $attr->nodeValue);
                //echo funcArray::display($excelFormat);
                /*loop thriough the array to get the values*/
                foreach ($excelFormat as $ef) {
                  /*NOTE: there are two kinds of values
                   *A)Single Value{b,i,perc,tot}
                   *B)Multi Value{bg=[value],fs=[value],fc=[value],cs=[value]}*/
                  if (strpos($ef, '=')) { // ------> MULTI VALUE SEPERATION
                    $efarray = explode('=', $ef);
                    /***COLUMN MERGE / COLSPAN***/
                    if (strpos($efarray[0], 'cs') !== false) {
                      $cellMerge .= $efarray[1] - 1;
                      //echo 'CELL MERGE INDICATOR: '.$cellMerge.'<br>';
                      $thisCellLoc = $fieldNum + $cellAfterMerge;
                      //echo 'COLUMN NUMBER ACCORDING TO LOOP: '.$fieldNum.'<br>';
                      //echo 'NUMBER OF CELLS MERGED IN PREVIOUS LOOP: '.$cellAfterMerge.'<br>';
                      //echo 'CELL\'S NEW LOCATION: '.$thisCellLoc.'<br>';
                      //$mergeStartNum .= $thisCellLoc;
                      $mergeStart .= funcData::num2alpha($thisCellLoc) . ($rowNum + 1);
                      //echo 'CELL\'S NEW LOCATION IN XLS: '.$mergeStart.'<br>';
                      $mergeUntil .= funcData::num2alpha($thisCellLoc + $cellMerge) . ($rowNum + 1);
                      //echo 'STOP MERGE AT THE FOLLOWING CELL NUMBER: '.($thisCellLoc + $cellMerge).'<br>';
                      //echo 'STOP MERGE AT THE FOLLOWING CELL NUMBER IN XLS: '.$mergeUntil.'<hr>';
                    }
                    /***BG COLOR***/
                    if (strpos($efarray[0], 'bg') !== false) {
                      $cellBgColor .= str_replace('#', '', $efarray[1]);
                      $cellBgColorId .= funcData::num2alpha($fieldNum) . ($rowNum + 1);
                    }
                    /***FONT SIZE***/
                    if (strpos($efarray[0], 'fs') !== false) {
                      $fontSize .= $efarray[1];
                      $fontSizeId .= funcData::num2alpha($fieldNum) . ($rowNum + 1);
                    }

                    /*** BORDER **/
                    if (strpos($efarray[0], 'frame') !== false) {
                      $border .= $efarray[1];
                      $borderId .= funcData::num2alpha($fieldNum) . ($rowNum + 1);
                    }

                    /***FONT COLOR***/
                    if (strpos($efarray[0], 'fc') !== false) {
                      $fontColor .= str_replace('#', '', $efarray[1]);
                      $fontColorId .= funcData::num2alpha($fieldNum) . ($rowNum + 1);
                    }

                    if (strpos($efarray[0], 'align') !== false) {
                      $cellAlignId .= funcData::num2alpha($fieldNum) . ($rowNum + 1);
                      $cellAlign .= strtoupper($efarray[1]);
                    }
                  }
                  /***BOLD***/
                  if (strpos($ef, 'b') !== false) {
                    $cellBold .= true;
                    $cellBoldId .= $cell;
                  }
                  /** AUTO WIDTH */
                  if (strpos($ef, 'aw') !== false) {
                    $autoWidth .= true;
                    $autoWidthId .= funcData::num2alpha($fieldNum + $cellAfterMerge);
                  }

                  /***INTIGER, if empty then default to string***/
                  if (strpos($ef, 'int') !== false) {
                    $cellDataType .= 'number';
                  }
                  /***PERCENTAGE FORMAT***/
                  if (strpos($ef, 'perc') !== false) {
                    $cellDataType .= '%';
                  }
                  /***TOTAL CELL FORMAT***/
                  if (strpos($ef, 'tot') !== false) {
                    $cellTotal .= true;
                  }
                  /***FORMAT CELL AS NUMBER***/
                  if (strpos($ef, 'num') !== false) {
                    $cellAsNum .= true;
                  }
                }
              }
            }
          }

          $n = $fieldNum + $cellAfterMerge;
          $cell = funcData::num2alpha($n) . ($rowNum + 1);
          $xls->getActiveSheet()->setCellValueExplicit($cell, $field->nodeValue, (!empty($cellDataType) || $cellDataType == 'number' ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING));
          if (!empty($cellMerge)) {
            $xls->getActiveSheet()->mergeCells($mergeStart . ':' . $mergeUntil);
            $cellAfterMerge += $cellMerge;
            //echo $mergeStart.':'.$mergeUntil.'<hr>';
          }

          $borders = array(
            'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
          );

          if (!empty($border)) {
            $borderArrays = explode(';', $border);
            $borders = array();
            if (!empty($borderArrays)) {
              foreach ($borderArrays as $borderArray) {
                $borders[$borderArray] = array('style' => PHPExcel_Style_Border::BORDER_THIN);
              }
            }
          }

          if (!empty($mergeStart)) {
            $xls->getActiveSheet()->getStyle($mergeStart . ':' . $mergeUntil)->applyFromArray(array('borders' => $borders));
          }
          else {
            $xls->getActiveSheet()->getStyle($cell)->applyFromArray(array('borders' => $borders));
          }

          if ($cellTotal) {
            $xls->getActiveSheet()->getStyle($cell)->applyFromArray(array(
              'borders' => array(
                'top' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE)
              )
            ));
          }

          if ($cellDataType == '%') {
            $xls->getActiveSheet()->getStyle($cell)->getNumberFormat()->applyFromArray(array(
              'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
            ));
          }

          if ($autoWidth) {
            $xls->getActiveSheet()->getColumnDimension($autoWidthId)->setAutoSize(true);
          }

          if (!empty($cellBgColor)) {
            if (!empty($cellMerge)) {
              $cellBgColorId = $mergeStart;
            }
            $xls->getActiveSheet()->getStyle($cellBgColorId)->applyFromArray(array(
              'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => $cellBgColor)
              )
            ));
          }
          if (!empty($fontColor)) {
            if (!empty($cellMerge)) {
              $fontColorId = $mergeStart;
            }
            $xls->getActiveSheet()->getStyle($fontColorId)->getFont()->getColor()->setRGB($fontColor);
          }
          if (!empty($fontSize)) {
            if (!empty($cellMerge)) {
              $fontSizeId = $mergeStart;
            }
            $xls->getActiveSheet()->getStyle($fontSizeId)->getFont()->setSize($fontSize);
          }
          if (!empty($cellBold)) {
            if (!empty($cellMerge)) {
              $cellBoldId = $mergeStart;
            }
            $xls->getActiveSheet()->getStyle($cellBoldId)->getFont()->setBold($cellBold);
          }

          if (!empty($cellAlign)) {
            if (!empty($cellMerge)) {
              $cellAlignId = $mergeStart;
            }
            if ($cellAlign == 'LEFT') {
              $xls->getActiveSheet()->getStyle($cellAlignId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }
            if ($cellAlign == 'CENTER') {
              $xls->getActiveSheet()->getStyle($cellAlignId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            if ($cellAlign == 'RIGHT') {
              $xls->getActiveSheet()->getStyle($cellAlignId)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            }
          }
          if (!empty($cellAsNum)) {
            $xls->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
          }
          $columnNum[$xls->getActiveSheet()->getTitle()] += 1;

        }
      }
    }
  }

  static function num2alpha($n) {
    for ($r = ""; $n >= 0; $n = intval($n / 26) - 1) $r = chr($n % 26 + 0x41) . $r;
    return $r;
  }

  static function searchArray($needle, $haystack, $key) {
    foreach ($haystack as $hItem => $hValue) {
      foreach ($hValue as $subHItem => $details) {
        if (is_array($details) && isset($details[$key]) && $details[$key] === $needle) {
          return $hItem;
        }
      }
    }
  }

  static function is_digit($digit) {
    if (is_int($digit)) {
      return true;
    }
    elseif (is_string($digit)) {
      return ctype_digit($digit);
    }
    else {
      // booleans, floats and others
      return false;
    }
  }

  static function str_getcsv2($input, $delimiter = ',', $enclosure = '"') {
    if (empty($enclosure)) {
      return str_getcsv($input, $delimiter);
    }

    if (!preg_match("/[$enclosure]/", $input)) {
      return (array)preg_replace(array(
        "/^\\s*/",
        "/\\s*$/"
      ), '', explode($delimiter, $input));
    }

    $token = "##";
    $token2 = "::";
    //alternate tokens "\034\034", "\035\035", "%%";
    $t1 = preg_replace(array(
      "/\\\[$enclosure]/",
      "/$enclosure{2}/",
      "/[$enclosure]\\s*[$delimiter]\\s*[$enclosure]\\s*/",
      "/\\s*[$enclosure]\\s*/"
    ), array(
      $token2,
      $token2,
      $token,
      $token
    ), trim(trim(trim($input), $enclosure)));

    $a = explode($token, $t1);


    foreach ($a as $k => &$v) {
      if (preg_match("/^{$delimiter}/", $v) || preg_match("/{$delimiter}$/", $v)) {
        //$a[$k] = trim($v, $delimiter);
        if (substr($a[$k], 0, 1) == $delimiter) {
          $a[$k] = substr($a[$k], 1);
        }
        if (substr($a[$k], -1) == $delimiter) {
          $a[$k] = substr($a[$k], 0, strlen($a[$k]) - 1);
        }
        $a[$k] = preg_replace("/$delimiter/", "$token", $a[$k]);
      }
    }
    $a = explode($token, implode($token, $a));
    return (array)preg_replace(array(
      "/^\\s/",
      "/\\s$/",
      "/$token2/"
    ), array(
      '',
      '',
      $enclosure
    ), $a);
  }

  static function jsonpp($json, $istr = '  ') {
    $result = '';
    for ($p = $q = $i = 0; isset($json[$p]); $p++) {
      $json[$p] == '"' && ($p > 0 ? $json[$p - 1] : '') != '\\' && $q = !$q;
      if (!$q && strchr(" \t\n", $json[$p])) {
        continue;
      }
      if (strchr('}]', $json[$p]) && !$q && $i--) {
        strchr('{[', $json[$p - 1]) || $result .= "\n" . str_repeat($istr, $i);
      }
      $result .= $json[$p];
      if (strchr(',{[', $json[$p]) && !$q) {
        $i += strchr('{[', $json[$p]) === FALSE ? 0 : 1;
        strchr('}]', $json[$p + 1]) || $result .= "\n" . str_repeat($istr, $i);
      }
    }
    return $result;
  }

  static function splitName($fullName, $surnameFirst = false) {
    funcCore::requireClasses('collectiontext');
    $split = array();
    $splitName['title'] = null;
    $splitName['initials'] = null;
    $splitName['firstName'] = null;
    $splitName['surname'] = null;
    $splitName['isCompany'] = false;

    $company = Collectiontext::getCollectionByCriteria('COMPANYNAMES', $fullName);
    if (!empty($company)) {
      $splitName['surname'] = funcData::formatSurnameProperCase($fullName);
      $splitName['isCompany'] = true;
    }
    else {
      $splitName['surname'] = funcData::splitNameFindSurname($fullName, $surnameFirst);
      if ($surnameFirst) {
        $fullName = trim(str_replace(strtolower($splitName['surname']), '', $fullName), ' ');
      }
      $splitName['title'] = funcData::splitNameFindTitle($fullName);
      $names = trim(str_replace('"', '', str_replace(',', '', strtoupper(trim(str_replace(strtolower($splitName['surname']), '', str_replace(strtolower($splitName['title']), '', strtolower($fullName))), ' ')))), ' ');
      if ($names != '') {
        $splitName['initials'] = funcData::splitNameFindInitials($names);
        $names = explode(' ', funcData::formatSurnameProperCase($names));
        $firstName = false;
        foreach ($names as $name) {
          $len = strlen($name);
          if ($len > 2) {
            $firstName = true;
          }
        }
        if ($firstName) {
          $splitName['firstName'] = implode(' ', $names);
        }
        else {
          $splitName['initials'] = strtoupper(implode(' ', $names));
        }
      }
    }
    return $splitName;
  }

  static function splitNameFindInitials($names) {
    $names = explode(' ', $names);
    $initials = array();
    foreach ($names as $name) {
      $initials[] = substr($name, 0, 1);
    }
    $initials = implode(' ', $initials);
    return $initials;
  }

  static function splitNameFindSurname($fullName, $surnameFirst = false) {
    $fullName = str_replace('.', ' ', $fullName);
    $surname = '';
    if (strtoupper($fullName == 'NO SURNAME')) {
      return strtoupper($fullName);
    }
    $length = strlen($fullName);
    if ($surnameFirst) {
      if (substr(trim(strtolower($fullName), ' '), 0, 10) == "janse van ") {
        $substring = trim(str_replace("janse van ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 11;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 11) == "jansen van ") {
        $substring = trim(str_replace("jansen van ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 12;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "vd ") {
        $substring = trim(str_replace("vd ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 4;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "v d ") {
        $substring = trim(str_replace("v d ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 5;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 8) == "van der ") {
        $substring = trim(str_replace("van der", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 9;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 8) == "van den ") {
        $substring = trim(str_replace("van den ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 9;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 7) == "van de ") {
        $substring = trim(str_replace("van de ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 8;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "van ") {
        $substring = trim(str_replace("van ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 5;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 6) == "de la ") {
        $substring = trim(str_replace("de la ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 7;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "de ") {
        $substring = trim(str_replace("de ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 4;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "du ") {
        $substring = trim(str_replace("du ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 4;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "le ") {
        $substring = trim(str_replace("le ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 4;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "da ") {
        $substring = trim(str_replace("da ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 4;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "von ") {
        $substring = trim(str_replace("von ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 5;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "mc ") {
        $substring = trim(str_replace("mc ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 4;
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mac ") {
        $substring = trim(str_replace("mac ", '', $fullName), ' ');
        $lastPosition = strpos($substring, " ") + 5;
      }
      else {
        $lastPosition = strpos($fullName, ' ');
      }
      if ($lastPosition == 0) {
        $surname = $fullName;
      }
      else {
        $surname = substr($fullName, 0, $lastPosition);
      }
    }
    elseif (substr($fullName, 0, 1) == '"' && substr($fullName, $length - 1, 1) == '"' && strpos($fullName, ',')) {
      //Using just surname
      $fullName = str_replace('"', '', $fullName);
      $surname = ' ' . substr($fullName, 0, strpos($fullName, ','));
      if (substr(trim(strtolower($surname), ' '), 0, 10) == "janse van ") {
        $surname = str_replace("Janse Van", "Janse van", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 11) == "jansen van ") {
        $surname = str_replace("Jansen Van", "Jansen van", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "vd ") {
        $surname = str_replace("vd", "van der", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "v d ") {
        $surname = str_replace("v d", "van der", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 8) == "van der ") {
        $surname = str_replace("Van Der", "van der", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 8) == "van den ") {
        $surname = str_replace("Van Den", "van den", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 7) == "van de ") {
        $surname = str_replace("Van De", "van de", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "van ") {
        $surname = str_replace("Van", "van", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 6) == "de la ") {
        $surname = str_replace("De La", "de la", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "de ") {
        $surname = str_replace("De", "de", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "du ") {
        $surname = str_replace("Du", "du", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "le ") {
        $surname = str_replace("Le", "le", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "da ") {
        $surname = str_replace("Da", "da", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "von ") {
        $surname = str_replace("Von", "von", $surname);
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "mc ") {
        $length = strlen($surname);
        $surname = 'Mc' . funcString::toProper(substr($surname, 3, ($length - 3)));
      }
      elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "mac ") {
        $length = strlen($surname);
        $surname = 'Mac' . funcString::toProper(substr($surname, 4, ($length - 4)));
      }
      else {
        $length = strlen($surname);
        $key = strrpos($surname, " ");
        $surname = substr($surname, $key + 1, $length - $key);
      }
    }
    else {
      $length = strlen($fullName);
      $fullName = funcString::toProper($fullName);
      //CHECK FIRST FOR FULL NAMES CONTAINING SURNAME ONLY, THEN FOR SURNAME AS PART OF A FULL NAME
      if (substr(trim(strtolower($fullName), ' '), 0, 10) == "janse van ") {
        $surname = str_replace("Janse Van", "Janse van", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 11) == "jansen van ") {
        $surname = str_replace("Jansen Van", "Jansen van", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "vd ") {
        $surname = str_replace("vd", "van der", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "v d ") {
        $surname = str_replace("v d", "van der", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 8) == "van der ") {
        $surname = str_replace("Van Der", "van der", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 8) == "van den ") {
        $surname = str_replace("Van Den", "van den", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 7) == "van de ") {
        $surname = str_replace("Van De", "van de", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "van ") {
        $surname = str_replace("Van", "van", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 6) == "de la ") {
        $surname = str_replace("De La", "de la", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "de ") {
        $surname = str_replace("De", "de", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "du ") {
        $surname = str_replace("Du", "du", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "le ") {
        $surname = str_replace("Le", "le", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "da ") {
        $surname = str_replace("Da", "da", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "von ") {
        $surname = str_replace("Von", "von", $fullName);
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "mc ") {
        $length = strlen($fullName);
        $surname = 'Mc' . funcString::toProper(substr($fullName, 3, ($length - 3)));
      }
      elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mac ") {
        $length = strlen($fullName);
        $surname = 'Mac' . funcString::toProper(substr($fullName, 4, ($length - 4)));
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " janse van ")) {
        $key = strpos(strtolower($fullName), " janse van ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " jansen van ")) {
        $key = strpos(strtolower($fullName), " jansen van ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " vd ")) {
        $key = strpos(strtolower($fullName), " vd ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " v d ")) {
        $key = strpos(strtolower($fullName), " v d ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " van der ")) {
        $key = strpos(strtolower($fullName), " van der ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " van den ")) {
        $key = strpos(strtolower($fullName), " van den ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " van de ")) {
        $key = strpos(strtolower($fullName), " van de ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " van ")) {
        $key = strpos(strtolower($fullName), " van ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " de la ")) {
        $key = strpos(strtolower($fullName), " de la ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " de ")) {
        $key = strpos(strtolower($fullName), " de ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " du ")) {
        $key = strpos(strtolower($fullName), " du ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " le ")) {
        $key = strpos(strtolower($fullName), " le ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " da ")) {
        $key = strpos(strtolower($fullName), " da ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " von ")) {
        $key = strpos(strtolower($fullName), " von ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " mc ")) {
        $key = strpos(strtolower($fullName), " mc ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), " mac ")) {
        $key = strpos(strtolower($fullName), " mc ");
        $surname = substr($fullName, $key + 1, $length - $key);
      }
      elseif (strpos(trim(strtolower($fullName), ' '), ",")) {
        $key = strpos(strtolower($fullName), ",");
        $surname = substr($fullName, 0, $key + 1);
      }
      else {
        if (empty($surname)) {
          $key = strrpos(strtolower($fullName), " ");
          if (!empty($key)) {
            $surname = substr($fullName, $key + 1, $length - $key);
          }
          else {
            $surname = $fullName;
          }
        }
      }
    }
    return funcData::formatSurnameProperCase($surname);
  }

  static function splitNameFindTitle($fullName) {
    $fullName = str_replace('.', ' ', $fullName);
    $title = '';
    if (substr($fullName, 0, 1) == '"' && substr($fullName, $length - 1, 1) == '"' && strpos($fullName, ',')) {
      $length = strlen($fullName);
      $fullName = str_replace('"', '', $fullName);
      $fullName = substr($fullName, strpos($fullName, ','), $length - strpos($fullName, ','));
    }
    if (substr(trim(strtolower($fullName), ' '), 0, 4) == "mrs ") {
      $title = 'Mrs';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "mr ") {
      $title = 'Mr';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mnr ") {
      $title = 'Mnr';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mev ") {
      $title = 'Mev';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mej ") {
      $title = 'Mej';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "ms ") {
      $title = 'Ms';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 5) == "miss ") {
      $title = 'Miss';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "dr ") {
      $title = 'Dr';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 10) == "professor ") {
      $title = 'Professor';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 5) == "prof ") {
      $title = 'Prof';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "rev ") {
      $title = 'Rev';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 6) == "judge ") {
      $title = 'Judge';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 10) == "inspector ") {
      $title = 'Inspector';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "gen ") {
      $title = 'Gen';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 8) == "captain ") {
      $title = 'Capt';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 5) == "capt ") {
      $title = 'Capt';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "cpt ") {
      $title = 'Capt';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 5) == "brig ") {
      $title = 'Brig';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "adv ") {
      $title = 'Adv';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 9) == "advocate ") {
      $title = 'Adv';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "col ") {
      $title = 'Col';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 10) == "brigadier ") {
      $title = 'Brigadier';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 5) == "mlle ") {
      $title = 'Mademoiselle';
    }
    elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mme ") {
      $title = 'Madame';
    }
    $title = trim($title, ' ');
    if ($title == '') {
      if (strpos($fullName, ',')) {
        //comma in the name - chances are that it is back-to-front!
        $length = strlen($fullName);
        if (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "mrs") {
          $title = 'Mrs';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 2, 2) == "mr") {
          $title = 'Mr';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "mnr") {
          $title = 'Mnr';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "mev") {
          $title = 'Mev';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "mej") {
          $title = 'Mej';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 2, 2) == "ms") {
          $title = 'Ms';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 4, 4) == "miss") {
          $title = 'Miss';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 2, 2) == "dr") {
          $title = 'Dr';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 9, 9) == "professor") {
          $title = 'Professor';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 4, 4) == "prof") {
          $title = 'Prof';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "rev") {
          $title = 'Rev';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 5, 5) == "judge") {
          $title = 'Judge';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 9, 9) == "inspector") {
          $title = 'Inspector';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "gen") {
          $title = 'Gen';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 7, 7) == "captain") {
          $title = 'Capt';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 4, 4) == "capt") {
          $title = 'Capt';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "cpt") {
          $title = 'Capt';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 4, 4) == "brig") {
          $title = 'Brig';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "adv") {
          $title = 'Adv';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 8, 8) == "advocate") {
          $title = 'Adv';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 3, 3) == "col") {
          $title = 'Col';
        }
        elseif (substr(trim(strtolower($fullName), ' '), $length - 9, 9) == "brigadier") {
          $title = 'Brigadier';
        }
        elseif (substr(trim(strtolower($fullName), ' '), 0, 4) == "mlle") {
          $title = 'Mademoiselle';
        }
        elseif (substr(trim(strtolower($fullName), ' '), 0, 3) == "mme") {
          $title = 'Madame';
        }
      }
    }
    return $title;
  }

  static function formatSurnameProperCase($surname) {
    $surname = funcString::toProper(trim(str_replace('.', '', $surname), ' '));
    if (substr(trim(strtolower($surname), ' '), 0, 10) == "janse van ") {
      $surname = str_replace("Janse Van", "Janse van", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 11) == "jansen van ") {
      $surname = str_replace("Jansen Van", "Jansen van", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "vd ") {
      $surname = str_replace("vd", "van der", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "v d ") {
      $surname = str_replace("v d", "van der", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 8) == "van der ") {
      $surname = str_replace("Van Der", "van der", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 8) == "van den ") {
      $surname = str_replace("Van Den", "van den", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 7) == "van de ") {
      $surname = str_replace("Van De", "van de", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "van ") {
      $surname = str_replace("Van", "van", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 6) == "de la ") {
      $surname = str_replace("De La", "de la", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "de ") {
      $surname = str_replace("De", "de", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "du ") {
      $surname = str_replace("Du", "du", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "le ") {
      $surname = str_replace("Le", "le", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "da ") {
      $surname = str_replace("Da", "da", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 4) == "von ") {
      $surname = str_replace("Von", "von", $surname);
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "mc ") {
      $length = strlen($surname);
      $surname = 'Mc' . funcString::toProper(substr($surname, 2, ($length - 2)));
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 2) == "mc") {
      $length = strlen($surname);
      $surname = 'Mc' . funcString::toProper(substr($surname, 2, ($length - 2)));
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "mac ") {
      $length = strlen($surname);
      $surname = 'Mac' . funcString::toProper(substr($surname, 3, ($length - 3)));
    }
    elseif (substr(trim(strtolower($surname), ' '), 0, 3) == "mac") {
      $length = strlen($surname);
      $surname = 'Mac' . funcString::toProper(substr($surname, 3, ($length - 3)));
    }
    return $surname;
  }
}

?>