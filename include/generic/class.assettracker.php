<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.assettracker.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class AssetTracker extends _AssetTracker {
  }
}

class _AssetTracker {
  public $IdassetTracker;
  public $Idasset;
  public $Idlocation;
  public $DateTime;
  public $AssetNumber;
  public $CategoryName;
  public $AssetDescription;
  public $Make;
  public $Model;
  public $SerialNumber;
  public $Iduser;
  public $IdcostCentre;
  public $Idcondition;
  public $Idstatus;
  public $Longitude;
  public $Latitude;

  public function __construct($IdassetTracker = null) {
    if (!is_null($IdassetTracker)) {
      $this->lookup($IdassetTracker);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'IdassetTracker';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idasset_tracker';
    }
    return null;
  }

  public function exists($IdassetTracker = null) {
    $IdassetTracker = (!empty($IdassetTracker)) ? $GLOBALS['app.db']->realEscapeString($IdassetTracker) : $this->IdassetTracker;
    if (empty($IdassetTracker)) {
      return false;
    }

    $sql = "SELECT `idasset_tracker` FROM asset_tracker WHERE 
`idasset_tracker` = '" . $GLOBALS['app.db']->realEscapeString($IdassetTracker) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->IdassetTracker;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idasset_tracker';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM asset_tracker WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`asset_tracker`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `asset_tracker`.`idasset_tracker`) AS `Total` FROM asset_tracker $join $where";
    }
    else {
      $sql = "SELECT $select FROM asset_tracker $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $a = new AssetTracker();
        $a->populate($row);
        $result[$a->IdassetTracker] = $a;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->IdassetTracker = isset($row['idasset_tracker']) ? funcString::latinToUtf8($row['idasset_tracker']) : null;
    $this->Idasset = isset($row['idasset']) ? funcString::latinToUtf8($row['idasset']) : null;
    $this->Idlocation = isset($row['idlocation']) ? funcString::latinToUtf8($row['idlocation']) : null;
    $this->DateTime = isset($row['date_time']) ? funcString::latinToUtf8($row['date_time']) : null;
    $this->AssetNumber = isset($row['asset_number']) ? funcString::latinToUtf8($row['asset_number']) : null;
    $this->CategoryName = isset($row['category_name']) ? funcString::latinToUtf8($row['category_name']) : null;
    $this->AssetDescription = isset($row['asset_description']) ? funcString::latinToUtf8($row['asset_description']) : null;
    $this->Make = isset($row['make']) ? funcString::latinToUtf8($row['make']) : null;
    $this->Model = isset($row['model']) ? funcString::latinToUtf8($row['model']) : null;
    $this->SerialNumber = isset($row['serial_number']) ? funcString::latinToUtf8($row['serial_number']) : null;
    $this->Iduser = isset($row['iduser']) ? funcString::latinToUtf8($row['iduser']) : null;
    $this->IdcostCentre = isset($row['idcost_centre']) ? funcString::latinToUtf8($row['idcost_centre']) : null;
    $this->Idcondition = isset($row['idcondition']) ? funcString::latinToUtf8($row['idcondition']) : null;
    $this->Idstatus = isset($row['idstatus']) ? funcString::latinToUtf8($row['idstatus']) : null;
    $this->Longitude = isset($row['longitude']) ? funcString::latinToUtf8($row['longitude']) : null;
    $this->Latitude = isset($row['latitude']) ? funcString::latinToUtf8($row['latitude']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE asset_tracker SET
`idasset` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idasset), true, false) . ",
`idlocation` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idlocation), true, false) . ",
`date_time` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->DateTime)), true, true) . ",
`asset_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetNumber)), true, true) . ",
`category_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CategoryName)), true, true) . ",
`asset_description` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetDescription)), true, true) . ",
`make` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Make)), true, true) . ",
`model` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Model)), true, true) . ",
`serial_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->SerialNumber)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . ",
`idcost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IdcostCentre), true, false) . ",
`idcondition` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idcondition), true, false) . ",
`idstatus` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idstatus), true, false) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . "
WHERE
`idasset_tracker` = '" . $GLOBALS['app.db']->realEscapeString($this->IdassetTracker) . "'";
    }
    else {
      $sql = "INSERT INTO asset_tracker SET
`idasset` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idasset), true, false) . ",
`idlocation` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idlocation), true, false) . ",
`date_time` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->DateTime)), true, true) . ",
`asset_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetNumber)), true, true) . ",
`category_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CategoryName)), true, true) . ",
`asset_description` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetDescription)), true, true) . ",
`make` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Make)), true, true) . ",
`model` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Model)), true, true) . ",
`serial_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->SerialNumber)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . ",
`idcost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IdcostCentre), true, false) . ",
`idcondition` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idcondition), true, false) . ",
`idstatus` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idstatus), true, false) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->IdassetTracker = (empty($this->IdassetTracker) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->IdassetTracker;
    return $result;
  }

  public function delete($IdassetTracker = null) {
    $IdassetTracker = (!empty($IdassetTracker)) ? $GLOBALS['app.db']->realEscapeString($IdassetTracker) : $this->IdassetTracker;
    if (empty($IdassetTracker)) {
      return false;
    }
    $sql = "DELETE FROM asset_tracker WHERE `idasset_tracker` = '$IdassetTracker' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>