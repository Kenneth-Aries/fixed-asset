<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.status.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Status extends _Status {
  }
}

class _Status {
  public $Idstatus;
  public $StatusName;

  public function __construct($Idstatus = null) {
    if (!is_null($Idstatus)) {
      $this->lookup($Idstatus);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Idstatus';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idstatus';
    }
    return null;
  }

  public function exists($Idstatus = null) {
    $Idstatus = (!empty($Idstatus)) ? $GLOBALS['app.db']->realEscapeString($Idstatus) : $this->Idstatus;
    if (empty($Idstatus)) {
      return false;
    }

    $sql = "SELECT `idstatus` FROM status WHERE 
`idstatus` = '" . $GLOBALS['app.db']->realEscapeString($Idstatus) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Idstatus;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idstatus';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM status WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`status`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `status`.`idstatus`) AS `Total` FROM status $join $where";
    }
    else {
      $sql = "SELECT $select FROM status $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $s = new Status();
        $s->populate($row);
        $result[$s->Idstatus] = $s;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Idstatus = isset($row['idstatus']) ? funcString::latinToUtf8($row['idstatus']) : null;
    $this->StatusName = isset($row['status_name']) ? funcString::latinToUtf8($row['status_name']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE status SET
`status_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->StatusName)), false, true) . "
WHERE
`idstatus` = '" . $GLOBALS['app.db']->realEscapeString($this->Idstatus) . "'";
    }
    else {
      $sql = "INSERT INTO status SET
`status_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->StatusName)), false, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Idstatus = (empty($this->Idstatus) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Idstatus;
    return $result;
  }

  public function delete($Idstatus = null) {
    $Idstatus = (!empty($Idstatus)) ? $GLOBALS['app.db']->realEscapeString($Idstatus) : $this->Idstatus;
    if (empty($Idstatus)) {
      return false;
    }
    $sql = "DELETE FROM status WHERE `idstatus` = '$Idstatus' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>