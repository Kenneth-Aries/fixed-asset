<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.usertitle.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class UserTitle extends _UserTitle {
  }
}

class _UserTitle {
  public $IduserTitle;
  public $TitleName;

  public function __construct($IduserTitle = null) {
    if (!is_null($IduserTitle)) {
      $this->lookup($IduserTitle);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'IduserTitle';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'iduser_title';
    }
    return null;
  }

  public function exists($IduserTitle = null, $TitleName = null) {
    $IduserTitle = (!empty($IduserTitle)) ? $GLOBALS['app.db']->realEscapeString($IduserTitle) : $this->IduserTitle;
    $TitleName = (!empty($TitleName)) ? $GLOBALS['app.db']->realEscapeString($TitleName) : $this->TitleName;
    if (empty($IduserTitle)) {
      return false;
    }
    if (empty($TitleName)) {
      return false;
    }
    $sql = "SELECT `iduser_title` FROM user_title WHERE 
`iduser_title` = '" . $GLOBALS['app.db']->realEscapeString($this->IduserTitle) . "' AND
`title_name` = '" . $GLOBALS['app.db']->realEscapeString($this->TitleName) . "'";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->IduserTitle;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'iduser_title';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM user_title WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`user_title`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `user_title`.`iduser_title`) AS `Total` FROM user_title $join $where";
    }
    else {
      $sql = "SELECT $select FROM user_title $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $u = new UserTitle();
        $u->populate($row);
        $result[] = $u;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->IduserTitle = isset($row['iduser_title']) ? funcString::latinToUtf8($row['iduser_title']) : null;
    $this->TitleName = isset($row['title_name']) ? funcString::latinToUtf8($row['title_name']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE user_title SET
`iduser_title` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IduserTitle), false, false) . ",
`title_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->TitleName)), false, true) . "
WHERE
`iduser_title` = '" . $GLOBALS['app.db']->realEscapeString($this->IduserTitle) . "' AND
`title_name` = '" . $GLOBALS['app.db']->realEscapeString($this->TitleName) . "'";
    }
    else {
      $sql = "INSERT INTO user_title SET
`iduser_title` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IduserTitle), false, false) . ",
`title_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->TitleName)), false, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->IduserTitle = (empty($this->IduserTitle) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->IduserTitle;
    return $result;
  }

  public function delete($IduserTitle = null) {
    $IduserTitle = (!empty($IduserTitle)) ? $GLOBALS['app.db']->realEscapeString($IduserTitle) : $this->IduserTitle;
    if (empty($IduserTitle)) {
      return false;
    }
    $sql = "DELETE FROM user_title WHERE `iduser_title` = '$IduserTitle' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>