<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.transaction.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Transaction extends _Transaction {
  }
}

class _Transaction {
  public $IdassetTrack;
  public $Idasset;
  public $Created;
  public $LastScanned;
  public $LastMoved;
  public $WrittenOff;
  public $Retired;
  public $StatusName;
  public $ConditionName;
  public $Idlocation;
  public $Longitude;
  public $Latitude;
  public $Iduser;

  public function __construct($IdassetTrack = null) {
    if (!is_null($IdassetTrack)) {
      $this->lookup($IdassetTrack);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'IdassetTrack';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idasset_track';
    }
    return null;
  }

  public function exists($IdassetTrack = null) {
    $IdassetTrack = (!empty($IdassetTrack)) ? $GLOBALS['app.db']->realEscapeString($IdassetTrack) : $this->IdassetTrack;
    if (empty($IdassetTrack)) {
      return false;
    }

    $sql = "SELECT `idasset_track` FROM transaction WHERE 
`idasset_track` = '" . $GLOBALS['app.db']->realEscapeString($IdassetTrack) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->IdassetTrack;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idasset_track';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM transaction WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`transaction`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `transaction`.`idasset_track`) AS `Total` FROM transaction $join $where";
    }
    else {
      $sql = "SELECT $select FROM transaction $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $t = new Transaction();
        $t->populate($row);
        $result[$t->IdassetTrack] = $t;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->IdassetTrack = isset($row['idasset_track']) ? funcString::latinToUtf8($row['idasset_track']) : null;
    $this->Idasset = isset($row['idasset']) ? funcString::latinToUtf8($row['idasset']) : null;
    $this->Created = isset($row['created']) ? funcString::latinToUtf8($row['created']) : null;
    $this->LastScanned = isset($row['last_scanned']) ? funcString::latinToUtf8($row['last_scanned']) : null;
    $this->LastMoved = isset($row['last_moved']) ? funcString::latinToUtf8($row['last_moved']) : null;
    $this->WrittenOff = isset($row['written_off']) ? funcString::latinToUtf8($row['written_off']) : null;
    $this->Retired = isset($row['retired']) ? funcString::latinToUtf8($row['retired']) : null;
    $this->StatusName = isset($row['status_name']) ? funcString::latinToUtf8($row['status_name']) : null;
    $this->ConditionName = isset($row['condition_name']) ? funcString::latinToUtf8($row['condition_name']) : null;
    $this->Idlocation = isset($row['idlocation']) ? funcString::latinToUtf8($row['idlocation']) : null;
    $this->Longitude = isset($row['longitude']) ? funcString::latinToUtf8($row['longitude']) : null;
    $this->Latitude = isset($row['latitude']) ? funcString::latinToUtf8($row['latitude']) : null;
    $this->Iduser = isset($row['iduser']) ? funcString::latinToUtf8($row['iduser']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE transaction SET
`idasset` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idasset), true, false) . ",
`created` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Created)), true, true) . ",
`last_scanned` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->LastScanned)), true, true) . ",
`last_moved` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->LastMoved)), true, true) . ",
`written_off` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->WrittenOff)), true, true) . ",
`retired` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Retired)), true, true) . ",
`status_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->StatusName)), true, true) . ",
`condition_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ConditionName)), true, true) . ",
`idlocation` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idlocation), true, false) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . "
WHERE
`idasset_track` = '" . $GLOBALS['app.db']->realEscapeString($this->IdassetTrack) . "'";
    }
    else {
      $sql = "INSERT INTO transaction SET
`idasset` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idasset), true, false) . ",
`created` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Created)), true, true) . ",
`last_scanned` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->LastScanned)), true, true) . ",
`last_moved` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->LastMoved)), true, true) . ",
`written_off` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->WrittenOff)), true, true) . ",
`retired` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Retired)), true, true) . ",
`status_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->StatusName)), true, true) . ",
`condition_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ConditionName)), true, true) . ",
`idlocation` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idlocation), true, false) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->IdassetTrack = (empty($this->IdassetTrack) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->IdassetTrack;
    return $result;
  }

  public function delete($IdassetTrack = null) {
    $IdassetTrack = (!empty($IdassetTrack)) ? $GLOBALS['app.db']->realEscapeString($IdassetTrack) : $this->IdassetTrack;
    if (empty($IdassetTrack)) {
      return false;
    }
    $sql = "DELETE FROM transaction WHERE `idasset_track` = '$IdassetTrack' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>