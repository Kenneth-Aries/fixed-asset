<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.location.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Location extends _Location {
  }
}

class _Location {
  public $Idlocation;
  public $Barcode;
  public $LocationName;
  public $Building;
  public $Region;
  public $Country;
  public $Longitude;
  public $Latitude;
  public $Iduser;
  public $Identity;

  public function __construct($Idlocation = null) {
    if (!is_null($Idlocation)) {
      $this->lookup($Idlocation);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Idlocation';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idlocation';
    }
    return null;
  }

  public function exists($Idlocation = null) {
    $Idlocation = (!empty($Idlocation)) ? $GLOBALS['app.db']->realEscapeString($Idlocation) : $this->Idlocation;
    if (empty($Idlocation)) {
      return false;
    }

    $sql = "SELECT `idlocation` FROM location WHERE 
`idlocation` = '" . $GLOBALS['app.db']->realEscapeString($Idlocation) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Idlocation;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idlocation';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM location WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`location`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `location`.`idlocation`) AS `Total` FROM location $join $where";
    }
    else {
      $sql = "SELECT $select FROM location $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $l = new Location();
        $l->populate($row);
        $result[$l->Idlocation] = $l;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Idlocation = isset($row['idlocation']) ? funcString::latinToUtf8($row['idlocation']) : null;
    $this->Barcode = isset($row['barcode']) ? funcString::latinToUtf8($row['barcode']) : null;
    $this->LocationName = isset($row['location_name']) ? funcString::latinToUtf8($row['location_name']) : null;
    $this->Building = isset($row['building']) ? funcString::latinToUtf8($row['building']) : null;
    $this->Region = isset($row['region']) ? funcString::latinToUtf8($row['region']) : null;
    $this->Country = isset($row['country']) ? funcString::latinToUtf8($row['country']) : null;
    $this->Longitude = isset($row['longitude']) ? funcString::latinToUtf8($row['longitude']) : null;
    $this->Latitude = isset($row['latitude']) ? funcString::latinToUtf8($row['latitude']) : null;
    $this->Iduser = isset($row['iduser']) ? funcString::latinToUtf8($row['iduser']) : null;
    $this->Identity = isset($row['identity']) ? funcString::latinToUtf8($row['identity']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE location SET
`barcode` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Barcode)), false, true) . ",
`location_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->LocationName)), true, true) . ",
`building` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Building)), true, true) . ",
`region` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Region)), true, true) . ",
`country` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Country)), true, true) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . ",
`identity` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Identity), true, false) . "
WHERE
`idlocation` = '" . $GLOBALS['app.db']->realEscapeString($this->Idlocation) . "'";
    }
    else {
      $sql = "INSERT INTO location SET
`barcode` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Barcode)), false, true) . ",
`location_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->LocationName)), true, true) . ",
`building` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Building)), true, true) . ",
`region` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Region)), true, true) . ",
`country` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Country)), true, true) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . ",
`identity` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Identity), true, false) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Idlocation = (empty($this->Idlocation) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Idlocation;
    return $result;
  }

  public function delete($Idlocation = null) {
    $Idlocation = (!empty($Idlocation)) ? $GLOBALS['app.db']->realEscapeString($Idlocation) : $this->Idlocation;
    if (empty($Idlocation)) {
      return false;
    }
    $sql = "DELETE FROM location WHERE `idlocation` = '$Idlocation' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>