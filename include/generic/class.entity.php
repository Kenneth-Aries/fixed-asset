<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.entity.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Entity extends _Entity {
  }
}

class _Entity {
  public $Identity;
  public $EntityName;

  public function __construct($Identity = null) {
    if (!is_null($Identity)) {
      $this->lookup($Identity);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Identity';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'identity';
    }
    return null;
  }

  public function exists($Identity = null) {
    $Identity = (!empty($Identity)) ? $GLOBALS['app.db']->realEscapeString($Identity) : $this->Identity;
    if (empty($Identity)) {
      return false;
    }

    $sql = "SELECT `identity` FROM entity WHERE 
`identity` = '" . $GLOBALS['app.db']->realEscapeString($Identity) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Identity;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'identity';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM entity WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`entity`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `entity`.`identity`) AS `Total` FROM entity $join $where";
    }
    else {
      $sql = "SELECT $select FROM entity $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $e = new Entity();
        $e->populate($row);
        $result[$e->Identity] = $e;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Identity = isset($row['identity']) ? funcString::latinToUtf8($row['identity']) : null;
    $this->EntityName = isset($row['entity_name']) ? funcString::latinToUtf8($row['entity_name']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE entity SET
`entity_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->EntityName)), false, true) . "
WHERE
`identity` = '" . $GLOBALS['app.db']->realEscapeString($this->Identity) . "'";
    }
    else {
      $sql = "INSERT INTO entity SET
`entity_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->EntityName)), false, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Identity = (empty($this->Identity) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Identity;
    return $result;
  }

  public function delete($Identity = null) {
    $Identity = (!empty($Identity)) ? $GLOBALS['app.db']->realEscapeString($Identity) : $this->Identity;
    if (empty($Identity)) {
      return false;
    }
    $sql = "DELETE FROM entity WHERE `identity` = '$Identity' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>