<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.asset.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Asset extends _Asset {
  }
}

class _Asset {
  public $Idasset;
  public $Idlocation;
  public $AssetBarcode;
  public $AssetNumber;
  public $CategoryName;
  public $AssetDescription;
  public $Make;
  public $Model;
  public $SerialNumber;
  public $Iduser;
  public $IdcostCentre;
  public $Idcondition;
  public $Idstatus;
  public $Longitude;
  public $Latitude;
  public $Value;
  public $Cost;
  public $Supplier;
  public $Invoice;
  public $EmployeeId;
  public $Identity;

  public function __construct($Idasset = null) {
    if (!is_null($Idasset)) {
      $this->lookup($Idasset);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Idasset';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idasset';
    }
    return null;
  }

  public function exists($Idasset = null) {
    $Idasset = (!empty($Idasset)) ? $GLOBALS['app.db']->realEscapeString($Idasset) : $this->Idasset;
    if (empty($Idasset)) {
      return false;
    }

    $sql = "SELECT `idasset` FROM asset WHERE 
`idasset` = '" . $GLOBALS['app.db']->realEscapeString($Idasset) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Idasset;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idasset';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM asset WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`asset`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `asset`.`idasset`) AS `Total` FROM asset $join $where";
    }
    else {
      $sql = "SELECT $select FROM asset $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $a = new Asset();
        $a->populate($row);
        $result[$a->Idasset] = $a;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Idasset = isset($row['idasset']) ? funcString::latinToUtf8($row['idasset']) : null;
    $this->Idlocation = isset($row['idlocation']) ? funcString::latinToUtf8($row['idlocation']) : null;
    $this->AssetBarcode = isset($row['asset_barcode']) ? funcString::latinToUtf8($row['asset_barcode']) : null;
    $this->AssetNumber = isset($row['asset_number']) ? funcString::latinToUtf8($row['asset_number']) : null;
    $this->CategoryName = isset($row['category_name']) ? funcString::latinToUtf8($row['category_name']) : null;
    $this->AssetDescription = isset($row['asset_description']) ? funcString::latinToUtf8($row['asset_description']) : null;
    $this->Make = isset($row['make']) ? funcString::latinToUtf8($row['make']) : null;
    $this->Model = isset($row['model']) ? funcString::latinToUtf8($row['model']) : null;
    $this->SerialNumber = isset($row['serial_number']) ? funcString::latinToUtf8($row['serial_number']) : null;
    $this->Iduser = isset($row['iduser']) ? funcString::latinToUtf8($row['iduser']) : null;
    $this->IdcostCentre = isset($row['idcost_centre']) ? funcString::latinToUtf8($row['idcost_centre']) : null;
    $this->Idcondition = isset($row['idcondition']) ? funcString::latinToUtf8($row['idcondition']) : null;
    $this->Idstatus = isset($row['idstatus']) ? funcString::latinToUtf8($row['idstatus']) : null;
    $this->Longitude = isset($row['longitude']) ? funcString::latinToUtf8($row['longitude']) : null;
    $this->Latitude = isset($row['latitude']) ? funcString::latinToUtf8($row['latitude']) : null;
    $this->Value = isset($row['value']) ? funcString::latinToUtf8($row['value']) : null;
    $this->Cost = isset($row['cost']) ? funcString::latinToUtf8($row['cost']) : null;
    $this->Supplier = isset($row['supplier']) ? funcString::latinToUtf8($row['supplier']) : null;
    $this->Invoice = isset($row['invoice']) ? funcString::latinToUtf8($row['invoice']) : null;
    $this->EmployeeId = isset($row['employee_id']) ? funcString::latinToUtf8($row['employee_id']) : null;
    $this->Identity = isset($row['identity']) ? funcString::latinToUtf8($row['identity']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE asset SET
`idlocation` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idlocation), false, false) . ",
`asset_barcode` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetBarcode)), true, true) . ",
`asset_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetNumber)), true, true) . ",
`category_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CategoryName)), true, true) . ",
`asset_description` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetDescription)), true, true) . ",
`make` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Make)), true, true) . ",
`model` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Model)), true, true) . ",
`serial_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->SerialNumber)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . ",
`idcost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IdcostCentre), true, false) . ",
`idcondition` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idcondition), true, false) . ",
`idstatus` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idstatus), true, false) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . ",
`value` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Value), true, false) . ",
`cost` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Cost), true, false) . ",
`supplier` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Supplier)), true, true) . ",
`invoice` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Invoice)), true, true) . ",
`employee_id` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->EmployeeId), true, false) . ",
`identity` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Identity), false, false) . "
WHERE
`idasset` = '" . $GLOBALS['app.db']->realEscapeString($this->Idasset) . "'";
    }
    else {
      $sql = "INSERT INTO asset SET
`idlocation` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idlocation), false, false) . ",
`asset_barcode` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetBarcode)), true, true) . ",
`asset_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetNumber)), true, true) . ",
`category_name` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->CategoryName)), true, true) . ",
`asset_description` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetDescription)), true, true) . ",
`make` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Make)), true, true) . ",
`model` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Model)), true, true) . ",
`serial_number` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->SerialNumber)), true, true) . ",
`iduser` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Iduser), true, false) . ",
`idcost_centre` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->IdcostCentre), true, false) . ",
`idcondition` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idcondition), true, false) . ",
`idstatus` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idstatus), true, false) . ",
`longitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Longitude)), true, true) . ",
`latitude` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Latitude)), true, true) . ",
`value` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Value), true, false) . ",
`cost` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Cost), true, false) . ",
`supplier` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Supplier)), true, true) . ",
`invoice` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Invoice)), true, true) . ",
`employee_id` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->EmployeeId), true, false) . ",
`identity` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Identity), false, false) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Idasset = (empty($this->Idasset) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Idasset;
    return $result;
  }

  public function delete($Idasset = null) {
    $Idasset = (!empty($Idasset)) ? $GLOBALS['app.db']->realEscapeString($Idasset) : $this->Idasset;
    if (empty($Idasset)) {
      return false;
    }
    $sql = "DELETE FROM asset WHERE `idasset` = '$Idasset' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>