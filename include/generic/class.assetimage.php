<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.assetimage.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class AssetImage extends _AssetImage {
  }
}

class _AssetImage {
  public $IdassetImages;
  public $Idasset;
  public $AssetUrl;
  public $ImagePriority;
  public $ImageDate;

  public function __construct($IdassetImages = null) {
    if (!is_null($IdassetImages)) {
      $this->lookup($IdassetImages);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'IdassetImages';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'idasset_images';
    }
    return null;
  }

  public function exists($IdassetImages = null) {
    $IdassetImages = (!empty($IdassetImages)) ? $GLOBALS['app.db']->realEscapeString($IdassetImages) : $this->IdassetImages;
    if (empty($IdassetImages)) {
      return false;
    }

    $sql = "SELECT `idasset_images` FROM asset_image WHERE 
`idasset_images` = '" . $GLOBALS['app.db']->realEscapeString($IdassetImages) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->IdassetImages;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'idasset_images';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM asset_image WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`asset_image`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `asset_image`.`idasset_images`) AS `Total` FROM asset_image $join $where";
    }
    else {
      $sql = "SELECT $select FROM asset_image $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $a = new AssetImage();
        $a->populate($row);
        $result[$a->IdassetImages] = $a;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->IdassetImages = isset($row['idasset_images']) ? funcString::latinToUtf8($row['idasset_images']) : null;
    $this->Idasset = isset($row['idasset']) ? funcString::latinToUtf8($row['idasset']) : null;
    $this->AssetUrl = isset($row['asset_url']) ? funcString::latinToUtf8($row['asset_url']) : null;
    $this->ImagePriority = isset($row['image_priority']) ? funcString::latinToUtf8($row['image_priority']) : null;
    $this->ImageDate = isset($row['image_date']) ? funcString::latinToUtf8($row['image_date']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE asset_image SET
`idasset` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idasset), true, false) . ",
`asset_url` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetUrl)), true, true) . ",
`image_priority` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->ImagePriority), true, false) . ",
`image_date` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ImageDate)), true, true) . "
WHERE
`idasset_images` = '" . $GLOBALS['app.db']->realEscapeString($this->IdassetImages) . "'";
    }
    else {
      $sql = "INSERT INTO asset_image SET
`idasset` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->Idasset), true, false) . ",
`asset_url` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->AssetUrl)), true, true) . ",
`image_priority` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->ImagePriority), true, false) . ",
`image_date` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ImageDate)), true, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->IdassetImages = (empty($this->IdassetImages) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->IdassetImages;
    return $result;
  }

  public function delete($IdassetImages = null) {
    $IdassetImages = (!empty($IdassetImages)) ? $GLOBALS['app.db']->realEscapeString($IdassetImages) : $this->IdassetImages;
    if (empty($IdassetImages)) {
      return false;
    }
    $sql = "DELETE FROM asset_image WHERE `idasset_images` = '$IdassetImages' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>