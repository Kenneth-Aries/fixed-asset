<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.roles.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Roles extends _Roles {
  }
}

class _Roles {
  public $Id;
  public $RoleId;
  public $Module;
  public $ViewRole;
  public $AddRole;
  public $EditRole;
  public $DeleteRole;

  public function __construct($Id = null) {
    if (!is_null($Id)) {
      $this->lookup($Id);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Id';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'Id';
    }
    return null;
  }

  public function exists($Id = null) {
    $Id = (!empty($Id)) ? $GLOBALS['app.db']->realEscapeString($Id) : $this->Id;
    if (empty($Id)) {
      return false;
    }

    $sql = "SELECT `Id` FROM roles WHERE 
`Id` = '" . $GLOBALS['app.db']->realEscapeString($Id) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Id;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'Id';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM roles WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  public static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`roles`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `roles`.`Id`) AS `Total` FROM roles $join $where";
    }
    else {
      $sql = "SELECT $select FROM roles $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $r = new Roles();
        $r->populate($row);
        $result[$r->Id] = $r;
      }
      return $result;
    }
    return null;
  }

  public static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  public static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Id = isset($row['Id']) ? funcString::latinToUtf8($row['Id']) : null;
    $this->RoleId = isset($row['RoleId']) ? funcString::latinToUtf8($row['RoleId']) : null;
    $this->Module = isset($row['Module']) ? funcString::latinToUtf8($row['Module']) : null;
    $this->ViewRole = isset($row['ViewRole']) ? funcString::latinToUtf8($row['ViewRole']) : null;
    $this->AddRole = isset($row['AddRole']) ? funcString::latinToUtf8($row['AddRole']) : null;
    $this->EditRole = isset($row['EditRole']) ? funcString::latinToUtf8($row['EditRole']) : null;
    $this->DeleteRole = isset($row['DeleteRole']) ? funcString::latinToUtf8($row['DeleteRole']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE roles SET
`RoleId` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->RoleId), false, false) . ",
`Module` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Module)), false, true) . ",
`ViewRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->ViewRole), false, false) . ",
`AddRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->AddRole), false, false) . ",
`EditRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->EditRole), false, false) . ",
`DeleteRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->DeleteRole), false, false) . "
WHERE
`Id` = '" . $GLOBALS['app.db']->realEscapeString($this->Id) . "'";
    }
    else {
      $sql = "INSERT INTO roles SET
`RoleId` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->RoleId), false, false) . ",
`Module` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Module)), false, true) . ",
`ViewRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->ViewRole), false, false) . ",
`AddRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->AddRole), false, false) . ",
`EditRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->EditRole), false, false) . ",
`DeleteRole` = " . funcData::nz($GLOBALS['app.db']->realEscapeString($this->DeleteRole), false, false) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Id = (empty($this->Id) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Id;
    return $result;
  }

  public function delete($Id = null) {
    $Id = (!empty($Id)) ? $GLOBALS['app.db']->realEscapeString($Id) : $this->Id;
    if (empty($Id)) {
      return false;
    }
    $sql = "DELETE FROM roles WHERE `Id` = '$Id' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>