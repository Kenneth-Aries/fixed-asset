<?php
class Entity extends _Entity {

  static function getName($entityId) {
    if (!empty($entityId)) {
      $sql = "SELECT `entity_name` FROM entity WHERE `identity` = {$entityId}";
      $result = $GLOBALS['app.db']->executeQuery($sql, true);
      if(!empty($result)) {
        return $result[0]['entity_name'];
      }
    }
    return false;
  }

}
?>