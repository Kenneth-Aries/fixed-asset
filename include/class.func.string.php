<?php
class funcString {

  public static function br2nl($html) {
    return preg_replace('#<br\s*/?>#i', "\n", $html);
  }

  public static function getFirstLineWithMatch($haystack, $needle) {
    $lines = explode("\n", $haystack);
    if ($lines) {
      foreach ($lines as $line) {
        if (strpos($line, $needle) !== false) {
          return $line;
        }
      }
    }
    return null;
  }

  public static function shorten($str, $len) {
    if (strlen($str) > $len) {
      $str = substr($str, 0, $len) . '...';
    }
    return $str;
  }

  public static function toProper($str) {
    return ucwords(strtolower($str));
  }

  public static function toSentence($str) {
    return ucfirst(strtolower($str));
  }

  public static function filter($str, $filter_type) {
    if (empty($str) || empty($filter_type)) {
      return $str;
    }
    if ($filter_type == 'ALPHANUMERIC') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }
    elseif ($filter_type == 'ALPHABETIC') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    elseif ($filter_type == 'DIGIT') {
      $allowedChars = '0123456789';
    }
    elseif ($filter_type == 'DECIMAL') {
      $allowedChars = '0123456789.';
    }
    elseif ($filter_type == 'FILENAME') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._- ()';
    }
    elseif ($filter_type == 'GFK') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 `-=[];,.\/~@#$%^&*+{}|":?><()_';
    }
    elseif ($filter_type == 'PERSONNAME') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.- ';
    }
    elseif ($filter_type == 'KEYBOARD') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=\][;,./ ~!@#$%^&*()_+{}|:"<>?\'';
    }
    elseif ($filter_type == 'MULTILANGUAGE') {
      $str = preg_replace('/[\x00-\x08\x0B-\x1F]/', '', $str);
      if (preg_match('/^[a-zA-Z0-9!"\'@#\$%\^\&*)\]\[(+=._-].*$/', $str)) {
        return $str;
      }
      return self::filter($str, 'KEYBOARD');
    }
    else {
      return $str;
    }

    $output = null;
    $i = 0;
    $max = strlen($str);
    for ($i = 0; $i < $max; $i++) {
      $letter = substr($str, $i, 1);
      if (strpos($allowedChars, $letter) !== false) {
        $output .= $letter;
      }
    }
    return $output;
  }

  public static function firstWord($text) {
    return substr($text, 0, strpos($text, ' '));
  }

  public static function smsParts($text) {
    if (strlen($text) <= 160) {
      return 1;
    }
    else {
      return ceil(strlen($text) / 152);
    }
  }

  public static function levenshtein2($word1, $word2, $caseSensitive = true) {
    $score = 0;
    if (!$caseSensitive) {
      $word1 = strtolower($word1);
      $word2 = strtolower($word2);
    }

    $remainder = preg_replace("/[" . preg_replace("/[^A-Za-z0-9\']/", ' ', $word1) . "]/i", '', $word2);
    $remainder .= preg_replace("/[" . preg_replace("/[^A-Za-z0-9\']/", ' ', $word2) . "]/i", '', $word1);
    $score = strlen($remainder) * 2;

    $w1_len = strlen($word1);
    $w2_len = strlen($word2);
    $score += $w1_len > $w2_len ? $w1_len - $w2_len : $w2_len - $w1_len;

    $w1 = $w1_len > $w2_len ? $word1 : $word2;
    $w2 = $w1_len > $w2_len ? $word2 : $word1;

    for ($i = 0; $i < strlen($w1); $i++) {
      if (!isset($w2[$i]) || $w1[$i] != $w2[$i]) {
        $score++;
      }
    }

    return $score;
  }

  private static function valid_1byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0x80) == 0x00;
  }

  private static function valid_2byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xE0) == 0xC0;
  }

  private static function valid_3byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xF0) == 0xE0;
  }

  private static function valid_4byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xF8) == 0xF0;
  }

  private static function valid_nextbyte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xC0) == 0x80;
  }

  public static function valid_utf8($string) {
    $len = strlen($string);
    $i = 0;
    while ($i < $len) {
      $char = ord(substr($string, $i++, 1));
      if (self::valid_1byte($char)) {    // continue
        continue;
      }
      else if (self::valid_2byte($char)) { // check 1 byte
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
      }
      else if (self::valid_3byte($char)) { // check 2 bytes
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
      }
      else if (self::valid_4byte($char)) { // check 3 bytes
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
      } // goto next char
    }
    return true; // done
  }

  public static function utf8ToLatin($value) {
    return iconv("UTF-8", "Windows-1252//IGNORE", $value);

/*
    //to be looked at
    //not complete. Need to get chars into correct format. This logs to the root folder error log for debugging
    if (!empty($value)) {
      $order      = mb_detect_order();
      $encoding   = mb_detect_encoding($value, $order, true);
      if ($encoding === false) {
        $encoding = 'UTF-8';
      }
      try {
        $test = iconv($encoding, "Windows-1252//IGNORE", $value);
        if ($test !== $value) {
          error_log('ICONV: ' . $value);
        }
        return $test;
      }
      catch (Exception $e) {
        error_log($value);
        error_log($e);
        //exit;
      }
    }
*/
  }

  public static function latinToUtf8($value) {
    if (strlen($value) == 1 && $value == 0) {
      return $value;
    }
    elseif (!empty($value)) {
      return iconv("Windows-1252", "UTF-8//IGNORE", $value);
    }
  }

  public static function serializeLenFix($data) {
    if (!empty($data)) {
      return unserialize(preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $data));
    }
    return false;
  }

  public static function isRepeatedChar($string, $min_char) {
    $string_length = strlen($string);
    if (empty($string) || empty($min_char) || $string_length < $min_char) {
      return false;
    }
    $string = strtoupper($string);
    //get first char
    $char = substr($string, 0, 1);
    $char_count = 0;
    //split string into arrray
    $string_array = str_split($string);
    foreach ($string_array as $c) {
      //check if multiple chars
      if ($c != $char) {
        return false;
      }
      $char_count++;
      //check char count matches or greater than min count
      if ($char_count >= $min_char) {
        return true;
      }
    }
    return false;
  }
}

?>