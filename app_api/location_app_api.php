<?php
require_once 'config.php';
//session_start(); // Starting Session
$error=''; // Variable To Store Error Message


if(!empty($_POST['function'])){

   
    
    if($_POST['function'] === "getLocationByEntity"){

        
            if(!empty($_POST['entity_id'])){
                
             $result = getLocationsByEntity($_POST['entity_id']);

                    if($result->num_rows > 0) {
                     $locationsArray = array();
                            while($row = $result->fetch_array(MYSQL_ASSOC)) {
                                //echo "in while";
                                $locationsArray[] = $row;
                            }

                        $resultsArray = array('result'=>'success','results'=>$locationsArray);

                        echo json_encode($resultsArray);

                    } else {
                        $result = array(
                                    "result" => "empty"
                                );

                        echo json_encode($result);
                    }

            }else{
                
                   $result = array(
                                    "result" => "No entity found."
                                ); 
                echo json_encode($result);
                
            }
          
        
        }elseif($_POST['function'] === "insertLocation"){

        
            if(!empty($_POST['location_barcode'])){
               
                $result = getLocationByName($_POST['location_name']);

                if($result->num_rows > 0) {
                
                   $resultsArray = array('result'=>'Location name exists.');

                   echo json_encode($resultsArray);

                } else {

                    $result = insertLocation($_POST['location_barcode'],$_POST['location_name'],$_POST['location_building'],$_POST['location_region'],$_POST['location_country'],$_POST['id_entity'],$_POST['longitude'],$_POST['latitude']);

                    if($result === true) {
                    
                            $result = array(
                                        "result" => "success"
                                    );

                            echo json_encode($result);
                    }else{
                        
                        $result = array(
                                        "result" => "fail"
                                    );

                            echo json_encode($result);
                    }
                }
            }
            }elseif($_POST['function'] === "getLocationByBarcode"){

        
                if(!empty($_POST['location_barcode'])){


                 $result = getLocationByBarcode($_POST['location_barcode']);

                     if($result->num_rows > 0) {
                     $locationsArray = array();
                            while($row = $result->fetch_array(MYSQL_ASSOC)) {
                                //echo "in while";
                                $locationsArray[] = $row;
                            }

                        $resultsArray = array('result'=>'success','results'=>$locationsArray);

                        echo json_encode($resultsArray);

                    } else {
                        $result = array(
                                    "result" => "empty"
                                );

                        echo json_encode($result);
                    }

                }

        
        }else{
         $result = array(
                        "result" => "No function found."
                                );     
            echo json_encode($result);
    }
}

function insertLocation($barcode, $locationName, $locationBuilding,$locationRegion,$locationCountry,$identity,$longitude,$latitude){
    
    $mysqli = getmysqli();

    $date = date("Y-m-d");
    
    $sql =  "INSERT INTO `facets_db`.`location`
                (`barcode`,
                `location_name`,
                `building`,
                `region`,
                `country`,
                `longitude`,
                `latitude`,
                `iduser`,
                `identity`)
                VALUES
                ('$barcode',
                '$locationName',
                '$locationBuilding',
                '$locationRegion',
                '$locationCountry',
                '$longitude',
                '$latitude',
                '1',
                '$identity');";
    
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]");
    return $result;
        //echo "New record created successfully";
    
    $mysqli->close();
}

function getLocations() {

    $mysqli = getmysqli();

    $sql = "select * from location";
    
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]");
    return $result;
        //echo "New record created successfully";
    
    $mysqli->close();
}
               

function getLocationsByEntity($entity_id) {

    $mysqli = getmysqli();

    $sql = "select * from location where identity='$entity_id'";
    
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]");
    return $result;
      
    $mysqli->close();
}

function getLocationByBarcode($location_barcode) {

    $mysqli = getmysqli();

    $sql = "select * from location where barcode='$location_barcode' or location_name ='$location_barcode'";
    
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]");
    return $result;
      
    $mysqli->close();
}
       
function getLocationByName($location_name) {

    $mysqli = getmysqli();

    $sql = "select * from location where location_name ='$location_name'";
    
    $result = $mysqli->query($sql) or trigger_error($mysqli->error." [$sql]");
    return $result;
      
    $mysqli->close();
}


?>